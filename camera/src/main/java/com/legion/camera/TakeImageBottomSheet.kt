package com.legion.camera

import android.Manifest
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.legion.camera.core.CameraType
import com.legion.camera.core.TakeImageCameraResultListener
import com.legion.camera.databinding.FragmentCameraBinding
import com.legion.core.bottomsheet.AppBottomSheetFragment
import com.legion.utils.BitmapBuilder
import com.legion.utils.DATE_PATTERN_WITH_ALL_TIME
import com.legion.utils.extensions.setVisibility
import com.legion.utils.getCurrentTime
import com.legion.utils.getImageFile
import java.util.concurrent.Executors

class TakeImageBottomSheet(
    private val listener: TakeImageCameraResultListener,
    private val cameraType: CameraType = CameraType.MAIN
) : BottomSheetDialogFragment() {

    private var _binding: FragmentCameraBinding? = null
    val binding
        get() = _binding!!

    private val cameraExecutor by lazy {
        Executors.newSingleThreadExecutor()
    }

    private val cameraSelector by lazy {
        when(cameraType) {
            CameraType.MAIN -> CameraSelector.DEFAULT_BACK_CAMERA
            CameraType.FRONT -> CameraSelector.DEFAULT_FRONT_CAMERA
        }
    }

    private val requestPermissionLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
        if (it) startCamera()
        else dismiss()
    }

    private val cameraProviderFuture by lazy {
        ProcessCameraProvider.getInstance(requireContext())
    }

    private val imageCapture by lazy {
        ImageCapture.Builder()
            .setTargetResolution(Size(720, 720))
            .build()
    }

    private val cameraProviderListener = Runnable {
        val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

        val preview = Preview.Builder()
            .build()
            .apply {
                setSurfaceProvider(binding.preview.surfaceProvider)
            }

        try {
            cameraProvider.unbindAll()
            cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture)

        } catch(exc: Exception) {
            listener.onError(exc)
            dismiss()
        }
    }

    private val takeImageButtonClickHandler = View.OnClickListener {
        binding.progress.setVisibility(true)
        takeImage()
    }
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCameraBinding.inflate(layoutInflater)
        return binding.root
    }
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    
        binding.root.layoutParams.height = resources.displayMetrics.heightPixels
    
        val dialogFragment = dialog as BottomSheetDialog
        
        dialogFragment.apply {
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
            behavior.peekHeight = BottomSheetBehavior.PEEK_HEIGHT_AUTO
            behavior.isDraggable = false
        }
    
        binding.takeImageButton.setOnClickListener(takeImageButtonClickHandler)
        binding.closeButton.setOnClickListener { dismiss() }
    
        if (!isCameraPermissionGranted()) {
            requestPermissionLauncher.launch(Manifest.permission.CAMERA)
            return
        }
        
        startCamera()
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
        _binding = null
    }

    private fun takeImage() {
        val fileName = "${getCurrentTime(DATE_PATTERN_WITH_ALL_TIME)}.jpg"
        val photoFile = getImageFile(requireContext(), fileName)

        val outputOptions = ImageCapture
            .OutputFileOptions
            .Builder(photoFile)
            .build()

        imageCapture.takePicture(outputOptions, ContextCompat.getMainExecutor(requireContext()),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    listener.onError(exc)
                    dismiss()
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    val savedUri = Uri.fromFile(photoFile)

                    val rotate = when(cameraType) {
                        CameraType.MAIN -> 90f
                        CameraType.FRONT -> -90f
                    }

                    val bitmapBuilder = BitmapBuilder(requireContext())
                    val bitmap = bitmapBuilder.buildFromUri(savedUri, rotate)

                    listener.onSuccess(savedUri, bitmap)
                    dismiss()
                }
            })
    }

    private fun startCamera() {
        cameraProviderFuture.addListener(cameraProviderListener, ContextCompat.getMainExecutor(requireContext()))
    }

    private fun isCameraPermissionGranted(): Boolean {
        val res = ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
        return res == PackageManager.PERMISSION_GRANTED
    }

    fun takeImage(fragmentManager: FragmentManager) {
        show(fragmentManager, toString())
    }
}