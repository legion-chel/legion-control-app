package com.legion.camera.core

interface TakeImageCamera {
    fun takeImage(listener: TakeImageCameraResultListener)
}