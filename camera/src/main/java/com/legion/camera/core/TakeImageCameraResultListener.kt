package com.legion.camera.core

import android.graphics.Bitmap
import android.net.Uri

interface TakeImageCameraResultListener {
    fun onSuccess(imageUri: Uri, bitmap: Bitmap?)
    fun onError(throwable: Throwable?)
}