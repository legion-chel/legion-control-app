package com.legion.camera.core

enum class CameraType {
    MAIN,
    FRONT
}