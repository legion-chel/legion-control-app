package com.legion.camera

import android.content.Context
import android.net.Uri
import android.os.Environment
import androidx.activity.result.ActivityResultLauncher
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.FileProvider
import com.legion.utils.DATE_PATTERN_WITH_ALL_TIME
import com.legion.utils.getCurrentTime
import com.legion.utils.getImageFile
import com.legion.utils.getImageFolder
import java.io.File

fun takeImage(context: Context, launcher: ActivityResultLauncher<Uri>): Uri? {
    val folder = getImageFolder(context)
    folder.mkdirs()

    val file = getImageFile(context, "${getCurrentTime(DATE_PATTERN_WITH_ALL_TIME)}.jpg")

    file.createNewFile()

    val imageUri = FileProvider.getUriForFile(
        context,
        BuildConfig.LIBRARY_PACKAGE_NAME,
        file
    )

    launcher.launch(imageUri)

    return imageUri
}