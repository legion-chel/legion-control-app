package com.legion.utils.log

import timber.log.Timber

object Logger {
    fun d(e: Throwable, message: String) = Timber.d(e, message)
    fun d(message: String) = Timber.d(message)

    fun w(e: Throwable, message: String) = Timber.w(e, message)
    fun w(message: String) = Timber.w(message)

    fun e(e: Throwable, message: String) = Timber.d(e, message)
    fun e(message: String) = Timber.e(message)
}

