package com.legion.utils.log

import timber.log.Timber
import javax.inject.Inject

class LogConfigurator @Inject constructor(
    private val releaseTree: ReleaseTree
) {

    fun configure(isDebug: Boolean) {
        val tree = if (isDebug) Timber.DebugTree()
        else releaseTree

        Timber.plant(tree)
    }
}