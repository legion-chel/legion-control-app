package com.legion.utils.log;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class LogConfigurator_Factory implements Factory<LogConfigurator> {
  private final Provider<ReleaseTree> releaseTreeProvider;

  public LogConfigurator_Factory(Provider<ReleaseTree> releaseTreeProvider) {
    this.releaseTreeProvider = releaseTreeProvider;
  }

  @Override
  public LogConfigurator get() {
    return newInstance(releaseTreeProvider.get());
  }

  public static LogConfigurator_Factory create(Provider<ReleaseTree> releaseTreeProvider) {
    return new LogConfigurator_Factory(releaseTreeProvider);
  }

  public static LogConfigurator newInstance(ReleaseTree releaseTree) {
    return new LogConfigurator(releaseTree);
  }
}
