package com.legion.utils.log

import android.util.Log
import timber.log.Timber
import javax.inject.Inject

class ReleaseTree @Inject constructor() : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG) {
            return
        }
    }
}