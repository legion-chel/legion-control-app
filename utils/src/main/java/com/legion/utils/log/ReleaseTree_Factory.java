package com.legion.utils.log;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ReleaseTree_Factory implements Factory<ReleaseTree> {
  @Override
  public ReleaseTree get() {
    return newInstance();
  }

  public static ReleaseTree_Factory create() {
    return InstanceHolder.INSTANCE;
  }

  public static ReleaseTree newInstance() {
    return new ReleaseTree();
  }

  private static final class InstanceHolder {
    private static final ReleaseTree_Factory INSTANCE = new ReleaseTree_Factory();
  }
}
