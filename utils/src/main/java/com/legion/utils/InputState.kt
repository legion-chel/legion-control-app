package com.legion.utils

sealed class InputState {
    object Correct : InputState()

    data class Incorrect(val error: String? = null) : InputState()
}

val InputState?.isCorrect get() = if (this == null) false else this is InputState.Correct