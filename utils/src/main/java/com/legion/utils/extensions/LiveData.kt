package com.legion.utils.extensions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

fun <T> MutableLiveData<T>.asImmutable() = this as LiveData<T>

@Suppress("RedundantSuspendModifier")
suspend fun <T> MutableLiveData<T>.observeDelegate(data: T) {
    postValue(data)
}