package com.legion.utils.extensions

val Any.tag: String
    get() = this::class.java.simpleName