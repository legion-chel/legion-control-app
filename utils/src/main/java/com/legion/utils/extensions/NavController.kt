package com.legion.utils.extensions

import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import com.legion.utils.R

fun NavController.navigateSafe(
    destination: NavDirections
) {
    currentDestination
        ?.getAction(destination.actionId)
        ?: return

    navigate(destination)
}