package com.legion.utils.extensions

import android.content.SharedPreferences

fun SharedPreferences.getString(key: String) = getString(key, String.empty) ?: String.empty