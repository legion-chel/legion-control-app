package com.legion.utils.extensions

import android.app.Activity
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar


fun Fragment.showMessage(message: String) = Snackbar
    .make(requireView(), message, Snackbar.LENGTH_SHORT)
    .show()

fun Fragment.showMessageWithAction(
    message: String,
    actionString: String,
    onAction: () -> Unit
) = Snackbar
    .make(requireView(), message, Snackbar.LENGTH_SHORT)
    .setAction(actionString) { onAction() }
    .show()

fun Fragment.showToast(message: String) = Toast
    .makeText(requireContext(), message, Toast.LENGTH_SHORT)
    .show()

fun Fragment.navigate(navDirection: NavDirections) {
    findNavController().navigateSafe(navDirection)
}

fun Fragment.hideKeyboard() {
    val imm: InputMethodManager =
        requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(requireView().windowToken, 0)
}
