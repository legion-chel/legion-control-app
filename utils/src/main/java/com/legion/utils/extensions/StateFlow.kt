package com.legion.utils.extensions

import kotlinx.coroutines.flow.MutableStateFlow

suspend fun<T> MutableStateFlow<T>.emitOrReplay(value: T) {
    val currentValue = this.value
    if (currentValue == value) replayCache
    else emit(value)
}