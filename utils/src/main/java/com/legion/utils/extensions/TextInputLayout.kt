package com.legion.utils.extensions

import com.google.android.material.textfield.TextInputLayout
import com.legion.utils.InputState

fun TextInputLayout.setInputState(inputState: InputState?) {
    when(inputState) {
        is InputState.Correct -> isErrorEnabled = false
        is InputState.Incorrect -> {
            isErrorEnabled = true
            error = inputState.error
        }
    }
}