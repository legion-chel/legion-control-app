package com.legion.utils

import android.content.Context
import androidx.core.content.ContextCompat
import javax.inject.Inject

class ResourceProvider @Inject constructor(
    private val context: Context
) {
    fun getString(stringId: Int) = context.getString(stringId)
    fun getString(stringId: Int, string: String) = context.getString(stringId, string)
    fun getStringArray(arrayId: Int) = context.resources.getStringArray(arrayId)

    fun getDimen(dimenId: Int) = context.resources.getDimension(dimenId).toInt()

    fun getDrawable(drawableId: Int) = ContextCompat.getDrawable(context, drawableId)
}