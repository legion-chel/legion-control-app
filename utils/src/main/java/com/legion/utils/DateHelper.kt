package com.legion.utils

import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private val DEFAULT_LOCALE = Locale("ru")

const val DEFAULT_DATE_PATTERN = "dd MMMM yyyy"
const val SEPARATED_DATE_PATTERN = "dd.MM.yyyy"
const val DATE_PATTERN_WITH_TIME = "dd MMMM yyyy HH:mm"
const val DATE_PATTERN_WITH_ALL_TIME = "dd_mm_yyyy_HH:mm:ss"

fun timeUnixToDate(time: Long, pattern: String = DEFAULT_DATE_PATTERN): String {
    val milliseconds = TimeUnit.MILLISECONDS.convert(time, TimeUnit.SECONDS)
    val date = Date(milliseconds)

    return date.toString(pattern)
}

fun getCurrentTime() = System.currentTimeMillis()

fun getCurrentTimeUnix() = TimeUnit.MILLISECONDS.toSeconds(getCurrentTime())

fun getCurrentTime(pattern: String): String {
    val currentTime = System.currentTimeMillis()
    val currentDate = Date(currentTime)

    return currentDate.toString(pattern)
}

fun Date.toString(pattern: String = DEFAULT_DATE_PATTERN): String =
    SimpleDateFormat(pattern, DEFAULT_LOCALE).format(this)
 