package com.legion.utils

import android.content.Context
import android.os.Environment
import java.io.File

fun getImageFolder(context: Context) =
    File("${context.getExternalFilesDir(Environment.DIRECTORY_DCIM)}").also {
        it.mkdir()
    }

fun getImageFile(context: Context, name: String) = File(getImageFolder(context), name)