package com.legion.utils

import android.text.Editable

typealias InputDelegate = (Editable?) -> Unit