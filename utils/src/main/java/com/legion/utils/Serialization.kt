package com.legion.utils

import com.google.gson.Gson
import com.google.gson.JsonParser
import com.legion.utils.extensions.empty

inline fun <reified T> fromJson(jsonString: String): T? = try {
    Gson().fromJson(jsonString, T::class.java)
} catch (e: Exception) {
    null
}

inline fun <reified T> listFromJson(jsonString: String): List<T>? = try {
    val gson = Gson()
    JsonParser
        .parseString(jsonString)
        .asJsonArray
        .map { gson.fromJson(it, T::class.java) }
} catch (e: Exception) {
    null
}


fun toJson(any: Any): String = try {
    Gson().toJson(any)
} catch (e: Exception) {
    String.empty
}