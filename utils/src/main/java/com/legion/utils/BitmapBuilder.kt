package com.legion.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import javax.inject.Inject

class BitmapBuilder @Inject constructor(
    private val context: Context
) {

    fun buildFromUri(uri: Uri, rotate: Float = 0f): Bitmap? {
        val contentResolver = context.applicationContext.contentResolver

        val parcelFileDescriptor = contentResolver
            .openFileDescriptor(uri, OPEN_FILE_MODE)
            ?: return null

        val fileDescriptor = parcelFileDescriptor.fileDescriptor
        val bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor)
        parcelFileDescriptor.close()

        val matrix = Matrix().apply {
            postRotate(rotate)
        }

        return Bitmap.createBitmap(
            bitmap,
            0,
            0,
            bitmap.width,
            bitmap.height,
            matrix,
            true
        )
    }

    companion object {
        private const val OPEN_FILE_MODE = "r"
    }
}