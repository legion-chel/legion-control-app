package com.legion.core.state

import com.legion.utils.extensions.empty

sealed class UIState<out T> {
    object Loading : UIState<Nothing>() {
        override fun toString() = "Loading"
    }
    
    class Success<T>(val data: T) : UIState<T>() {
        override fun toString() = "Success ${data.toString()}"
    }
    
    class Error(val error: String = String.empty) : UIState<Nothing>() {
        override fun toString() = "Error $error"
    }

    fun handleState(
        loadingDelegate: (() -> Unit)? = null,
        successDelegate: ((T) -> Unit)? = null,
        errorDelegate: ((String) -> Unit)? = null
    ) {
        when(this) {
            is Loading -> loadingDelegate?.invoke()
            is Success -> successDelegate?.invoke(data)
            is Error -> errorDelegate?.invoke(error)
        }
    }
}