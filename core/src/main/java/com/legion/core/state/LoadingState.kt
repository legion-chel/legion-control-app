package com.legion.core.state

import com.legion.utils.extensions.empty

sealed class LoadingState<out T> {
    object Processing : LoadingState<Nothing>()
    class Success<T>(val data: T) : LoadingState<T>()
    class Error(val error: String = String.empty) : LoadingState<Nothing>()

    val isSuccess
        get() = this is Success || this is Success
}