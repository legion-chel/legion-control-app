package com.legion.core.session.auth

import android.content.Context
import androidx.core.content.edit
import com.legion.utils.extensions.getString
import com.legion.utils.fromJson
import com.legion.utils.toJson

internal class AuthClient(
    context: Context
) {
    private val storage = context.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE)

    fun getAuthInfo(): AuthInfo? {
        val json = storage.getString(AUTH_INFO_KEY)
        return fromJson(json)
    }

    fun saveAuthInfo(authInfo: AuthInfo) {
        val json = toJson(authInfo)

        storage.edit {
            putString(AUTH_INFO_KEY, json)
        }
    }

    fun clear() {
        storage.edit {
            clear()
        }
    }

    companion object {
        private const val STORAGE_NAME = "auth_client"
        private const val AUTH_INFO_KEY = "auth_info"
    }
}