package com.legion.core.session.user

import android.content.Context
import androidx.core.content.edit
import com.legion.utils.extensions.getString
import com.legion.utils.fromJson
import com.legion.utils.toJson

internal class UserInfoSaver(
    context: Context
) {
    private val storage = context.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE)

    fun saveInfo(user: User) {
        val json = toJson(user)
        storage.edit {
            putString(INFO_KEY, json)
        }
    }

    fun getInfo(): User? {
        val json = storage.getString(INFO_KEY)
        return fromJson(json)
    }

    fun clear() {
        storage.edit {
            clear()
        }
    }

    companion object {
        private const val STORAGE_NAME = "user_info_storage"
        private const val INFO_KEY = "info_key"
    }
}