package com.legion.core.session.user

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("id")
    val id: Int,
    
    @SerializedName("username")
    val name: String,

    @SerializedName("email")
    val email: String,

    @SerializedName("firstName")
    val firstName: String,

    @SerializedName("lastName")
    val lastName: String,

    @SerializedName("licenseNumber")
    val licenseNumber: String,

    @SerializedName("birthDate")
    val birthDate: String,

    @SerializedName("created_at")
    val registrationDate: String,

    @SerializedName("role")
    val role: UserRole,

    @SerializedName("avatar")
    val avatar: String,
    
    @SerializedName("isRegistrationEnded")
    val isRegistrationEnded: Boolean
)