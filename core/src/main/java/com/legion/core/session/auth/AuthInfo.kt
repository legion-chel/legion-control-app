package com.legion.core.session.auth

import com.google.gson.annotations.SerializedName

data class AuthInfo(
    @SerializedName("msg")
    val message: String,

    @SerializedName("token")
    val token: String,

    @SerializedName("expires_in")
    val expireIn: Long
)