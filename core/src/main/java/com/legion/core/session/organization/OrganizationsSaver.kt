package com.legion.core.session.organization

import android.content.Context
import androidx.core.content.edit
import com.legion.utils.extensions.getString
import com.legion.utils.fromJson
import com.legion.utils.listFromJson
import com.legion.utils.toJson

internal class OrganizationsSaver(
    context: Context
) {
    private val pref = context.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE)

    fun save(organizations: List<Organization>) {
        val json = toJson(organizations)
        pref.edit {
            putString(DATA_KEY, json)
        }
    }

    fun get(): List<Organization> {
        val json = pref.getString(DATA_KEY)
        val defaultResult = emptyList<Organization>()

        return if (json.isEmpty()) defaultResult
        else listFromJson(json) ?: defaultResult
    }

    fun clear() {
        pref.edit {
            clear()
        }
    }

    companion object {
        private const val STORAGE_NAME = "organizations_info_storage"
        private const val DATA_KEY = "organizations_key"
    }
}