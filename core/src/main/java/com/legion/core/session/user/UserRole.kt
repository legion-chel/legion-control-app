package com.legion.core.session.user

import com.google.gson.annotations.SerializedName

enum class UserRole {
    @SerializedName("owner")
    OWNER,

    @SerializedName("client")
    CLIENT,

    @SerializedName("clientUser")
    CLIENT_USER,

    @SerializedName("guard")
    GUARD,

    @SerializedName("admin")
    ADMIN,

    @SerializedName("superAdmin")
    SUPER_ADMIN
}