package com.legion.core.session

import android.content.Context
import com.legion.core.session.auth.AuthClient
import com.legion.core.session.auth.AuthInfo
import com.legion.core.session.organization.Organization
import com.legion.core.session.organization.OrganizationsSaver
import com.legion.core.session.user.User
import com.legion.core.session.user.UserInfoSaver
import com.legion.utils.extensions.empty
import javax.inject.Inject

class UserSessionFacade @Inject constructor(
    context: Context,
) {
    private val authClient = AuthClient(context)
    private val userInfoSaver = UserInfoSaver(context)
    private val organizationsSaver = OrganizationsSaver(context)

    val token: String
        get() = authClient.getAuthInfo()?.token ?: String.empty

    val authInfo
        get() = authClient.getAuthInfo()

    val user
        get() = userInfoSaver.getInfo()

    val organizations
        get() = organizationsSaver.get()

    fun saveAuth(authInfo: AuthInfo) = authClient.saveAuthInfo(authInfo)

    fun saveUser(user: User) = userInfoSaver.saveInfo(user)

    fun saveOrganizations(organizations: List<Organization>) = organizationsSaver.save(organizations)

    fun clear() {
        authClient.clear()
        userInfoSaver.clear()
        organizationsSaver.clear()
    }
}