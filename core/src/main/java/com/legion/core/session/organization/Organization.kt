package com.legion.core.session.organization

import com.google.gson.annotations.SerializedName

data class Organization(
    @SerializedName("id")
    val id: Int,

    @SerializedName("name")
    val name: String,

    @SerializedName("description")
    val description: String,

    @SerializedName("address")
    val address: String
)