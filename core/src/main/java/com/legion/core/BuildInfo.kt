package com.legion.core

import javax.inject.Inject

class BuildInfo @Inject constructor() {
    val isDebug
        get() = BuildConfig.DEBUG
}