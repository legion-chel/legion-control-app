package com.legion.core.bottomsheet

import android.app.Dialog
import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


abstract class AppBottomSheetFragment : BottomSheetDialogFragment() {
    protected open val isFullScreen: Boolean
        get() = false

    protected open val isDraggable: Boolean
        get() = true

    protected val displayHeight: Int by lazy {
        Resources.getSystem().displayMetrics.heightPixels
    }

    protected open val maxHeight: Int
        get() = displayHeight

    protected abstract fun onCreateView(layoutInflater: LayoutInflater): View
    protected abstract fun onCreateDialog(dialogFragment: BottomSheetDialog)
    protected abstract fun getMinHeight(displayHeight: Int): Int

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        val view = onCreateView(layoutInflater).apply {
            minimumHeight = getMinHeight(displayHeight)
        }

        dialog.setContentView(view)

        if (isFullScreen) {
            view.layoutParams.height = maxHeight
            dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        dialog.behavior.isDraggable = isDraggable

        onCreateDialog(dialog)

        return dialog
    }
}