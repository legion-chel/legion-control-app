package com.legion.api

const val UNAUTHORIZED = 401
const val INVALID_DATA_CODE = 422
const val BAD_REQUEST_CODE = 400
const val SERVER_ERROR_CODE = 500