package com.legion.api

import com.legion.api.call.ResultAdapterFactory
import com.legion.api.call.ResultCallAdapter
import com.legion.api.factory.HttpClientFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class ApiServiceBuilder @Inject constructor(
    private val clientFactory: HttpClientFactory
) {
    fun <T> build(apiUrl: String, serviceClass: Class<T>): T {
        val httpClient = clientFactory.create()
        val converterFactory = GsonConverterFactory.create()

        return Retrofit.Builder()
            .baseUrl(apiUrl)
            .client(httpClient)
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(ResultAdapterFactory())
            .build()
            .create(serviceClass)
    }
}