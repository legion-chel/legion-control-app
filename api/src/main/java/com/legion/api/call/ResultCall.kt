package com.legion.api.call

import com.legion.api.http.ApiResult
import com.legion.api.http.HttpException
import com.legion.utils.log.Logger
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.concurrent.TimeUnit

internal class ResultCall<T>(proxy: Call<T>) : CallDelegate<T, ApiResult<T>>(proxy) {

    override fun enqueueImpl(callback: Callback<ApiResult<T>>) = proxy
        .enqueue(ResultCallback(this, callback))

    override fun cloneImpl(): ResultCall<T> = ResultCall(proxy.clone())

    private class ResultCallback<T>(
        private val proxy: ResultCall<T>,
        private val callback: Callback<ApiResult<T>>
    ) : Callback<T> {

        override fun onResponse(call: Call<T>, response: Response<T>) {
            val result: ApiResult<T> = if (response.isSuccessful) createSuccessResponse(response)
            else {
                val error = HttpException(
                    statusCode = response.code(),
                    statusMessage = response.message()
                )
                ApiResult.Failure.HttpError(error)
            }

            Logger.d("HTTP RESULT: $result")
            callback.onResponse(proxy, Response.success(result))
        }

        override fun onFailure(call: Call<T>, error: Throwable) {
            val result = when (error) {
                is retrofit2.HttpException -> ApiResult.Failure.HttpError(
                    HttpException(error.code(), error.message(), cause = error)
                )
                is IOException -> ApiResult.Failure.Error(error)
                else -> ApiResult.Failure.Error(error)
            }

            Logger.d("FAILURE HTTP RESULT: $result")
            callback.onResponse(proxy, Response.success(result))
        }

        @Suppress("UNCHECKED_CAST")
        private fun createSuccessResponse(response: Response<T>): ApiResult<T> {
            val body = response.body()
            return if(body == null) ApiResult.Success.Empty
            else {
                ApiResult.Success.HttpResponse(
                    value = response.body() as T,
                    statusCode = response.code(),
                    statusMessage = response.message()
                )
            }
        }
    }

    override fun timeout(): Timeout = Timeout().timeout(30, TimeUnit.SECONDS)
}