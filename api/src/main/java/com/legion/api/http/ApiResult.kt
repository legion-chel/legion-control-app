package com.legion.api.http

sealed class ApiResult<out T> {

    suspend fun handleResult(
        successDelegate: (suspend (T?) -> Unit)? = null,
        httpErrorDelegate: (suspend (Failure.HttpError) -> Unit)? = null,
        errorDelegate: (suspend (Throwable) -> Unit)? = null
    ) {
        when(this) {
            is Success.Empty -> successDelegate?.invoke(null)
            is Success -> successDelegate?.invoke(value)
            is Failure.HttpError -> httpErrorDelegate?.invoke(this)
            is Failure.Error -> errorDelegate?.invoke(error)
            else -> return
        }
    }

    sealed class Success<T> : ApiResult<T>() {
        abstract val value: T

        override fun toString() = value?.toString() ?: ""

        data class HttpResponse<T>(
            override val value: T,
            override val statusCode: Int,
            override val statusMessage: String?
        ) : Success<T>(), ApiResponse

        object Empty : Success<Nothing>() {
            override val value: Nothing get() = error("Value is empty")

            override fun toString() = "success"
        }
    }

    sealed class Failure<out E: Throwable>(open val error: E) : ApiResult<Nothing>() {
        override fun toString() = "failure($error)"

        class Error(override val error: Throwable) : Failure<Throwable>(error) {
            override fun toString() = "error $error"
        }

        class HttpError(override val error: HttpException) : Failure<HttpException>(error),
            ApiResponse {
            override val statusCode: Int
                get() = error.statusCode

            override val statusMessage: String?
                get() = error.statusMessage

            operator fun component1() = error
            operator fun component2() = statusCode
            operator fun component3() = statusMessage

            override fun toString() = "status code: $statusCode message: $statusMessage error: $error"
        }
    }
}