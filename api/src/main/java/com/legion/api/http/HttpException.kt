package com.legion.api.http

class HttpException(
    val statusCode: Int,
    val statusMessage: String? = null,
    cause: Throwable? = null
) : Exception(null, cause)