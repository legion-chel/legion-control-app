package com.legion.api.http

interface ApiResponse {
    val statusCode: Int
    val statusMessage: String?
}