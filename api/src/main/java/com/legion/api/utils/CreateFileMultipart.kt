package com.legion.api.utils

import android.content.Context
import android.net.Uri
import com.legion.utils.getImageFile
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody

private const val FILE_PART_NAME = "photos"

fun createFileMultipart(context: Context, uri: Uri): MultipartBody.Part {
    val imageName = uri.lastPathSegment
    checkNotNull(imageName) { "required image name" }
    val file = getImageFile(context, imageName)
    val requestFile = file.asRequestBody()
    return MultipartBody.Part.createFormData(FILE_PART_NAME, file.name, requestFile)
}

fun createFileMultipart(context: Context, name: String, uri: Uri): MultipartBody.Part {
    val imageName = uri.lastPathSegment
    
    checkNotNull(imageName) { "required image name" }
    
    val file = getImageFile(context, imageName)
    val requestFile = file.asRequestBody()
    return MultipartBody.Part.createFormData(name, file.name, requestFile)
}