package com.legion.api.utils

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody

private const val TYPE_MEDIA_TYPE = "multipart/form-data"
private const val TYPE_APPLICATION_JSON = "application/json"

fun createRequestBodyWithMediaType(value: String): RequestBody {
    val part = TYPE_MEDIA_TYPE.toMediaTypeOrNull()
    return value.toRequestBody(part)
}

fun createRequestBody(value: String): RequestBody {
    val part = TYPE_APPLICATION_JSON.toMediaTypeOrNull()
    return value.toRequestBody(part)
}