package com.legion.api.factory

import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class HttpClientFactory @Inject constructor(
    private val interceptorsFactory: InterceptorsFactory
) {
    fun create(): OkHttpClient {
        val currentTimeUnit = TimeUnit.SECONDS

        val builder = OkHttpClient.Builder()
            .readTimeout(ClientParams.REQUEST_TIMEOUT, currentTimeUnit)
            .writeTimeout(ClientParams.REQUEST_TIMEOUT, currentTimeUnit)

        builder.interceptors().clear()

        interceptorsFactory.create().forEach { interceptor ->
            builder.addInterceptor(interceptor)
        }

        return builder.build()
    }

    private object ClientParams {
        const val REQUEST_TIMEOUT = 30L
    }
}