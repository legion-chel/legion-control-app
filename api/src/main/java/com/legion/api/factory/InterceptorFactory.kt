package com.legion.api.factory

import okhttp3.Interceptor

fun interface InterceptorsFactory {
    fun create(): List<Interceptor>
}