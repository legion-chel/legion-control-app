package com.legion.qr

import androidx.fragment.app.Fragment
import com.google.zxing.integration.android.IntentIntegrator
import javax.inject.Inject

class QrScanner @Inject constructor() {

    fun scanQr(fragment: Fragment) {
        val integrator = IntentIntegrator.forSupportFragment(fragment)
        integrator.initiateScan()
    }

    fun onResult(
        resultData: ScanResult,
        listener: QrScanListener
    ) {
        val (requestCode, resultCode, data) = resultData

        val result = IntentIntegrator
            .parseActivityResult(requestCode, resultCode, data)
            ?.contents
            ?: return

        listener.onResult(result)
    }
}