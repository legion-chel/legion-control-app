package com.legion.qr

fun interface QrScanListener {
    fun onResult(data: String)
}