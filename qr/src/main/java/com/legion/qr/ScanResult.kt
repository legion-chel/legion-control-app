package com.legion.qr

import android.content.Intent

data class ScanResult(
    val requestCode: Int,
    val resultCode: Int,
    val data: Intent?
)