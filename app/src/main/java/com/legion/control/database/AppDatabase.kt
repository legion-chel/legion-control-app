package com.legion.control.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.legion.control.models.schedules.data.Schedule
import com.legion.control.models.schedules.db.ScheduleDao

@Database(
    entities = [
        Schedule::class
    ],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getScheduleDao(): ScheduleDao
}