package com.legion.control.database.impl

import android.content.Context
import androidx.room.Room
import com.legion.control.database.AppDatabase
import com.legion.control.database.AppDatabaseBuilder
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppDatabaseBuilderImpl @Inject constructor(
    context: Context
) : AppDatabaseBuilder {

    private val databaseInstance: AppDatabase = Room.databaseBuilder(
        context, AppDatabase::class.java, DATABASE_NAME
    )
        .build()

    override fun getDatabase() = databaseInstance

    companion object {
        private const val DATABASE_NAME = "app_database"
    }
}