package com.legion.control.database

interface AppDatabaseBuilder {
    fun getDatabase(): AppDatabase
}