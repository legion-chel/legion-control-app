package com.legion.control.components.guard.qr.goods

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.legion.camera.TakeImageBottomSheet
import com.legion.camera.core.TakeImageCamera
import com.legion.camera.core.TakeImageCameraResultListener
import com.legion.control.components.guard.qr.adapter.QrActionsAdapter
import com.legion.control.components.guard.qr.adapter.listeners.OnActionPhotoClickListener
import com.legion.control.components.guard.qr.adapter.listeners.OnActionTextClickListener
import com.legion.control.components.guard.qr.goods.data.GoodsQrInfoUIWrapper
import com.legion.control.databinding.FragmentQrInfoBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.control.models.qr.info.data.QrContent
import com.legion.core.state.UIState
import com.legion.utils.extensions.setVisibility
import com.legion.utils.extensions.showToast

class GoodsPassScreen(
    private val factory: ViewModelFactory,
    private val actionsAdapter: QrActionsAdapter,
    private val qrContent: QrContent
) : BottomSheetDialogFragment(), TakeImageCamera {

    private var _binding: FragmentQrInfoBinding? = null
    private val binding: FragmentQrInfoBinding get() = _binding!!

    private val viewModel: GoodsQrInfoViewModel by viewModels { factory }

    private val uiStateObserver = Observer<UIState<GoodsQrInfoUIWrapper>> {
        it?.handleState(
            loadingDelegate = this::onLoadingQRInfo,
            successDelegate = this::onSuccessLoadingData,
            errorDelegate = this::onErrorLoadingQrInfo
        )
    }

    private val sendQrResultUIStateObserver = Observer<UIState<String>> {
        it.handleState(
            loadingDelegate = this::onLoadingSendQrResult,
            successDelegate = this::onSuccessSendQrResult,
            errorDelegate = this::onErrorSendQrResult
        )
    }

    private val buttonCloseClickHandler = View.OnClickListener {
        dismiss()
    }

    private val buttonSendResultClickHandler = View.OnClickListener {
        val qrId = actionsAdapter.qrId
        val qrType = actionsAdapter.actionType ?: return@OnClickListener

        viewModel.sendActionsResult(qrId, qrType)
    }

    private val photoActionClickListener = OnActionPhotoClickListener { action, imageUri, _ ->
        viewModel.addActionPhoto(action, imageUri)
    }

    private val textActionClickListener = OnActionTextClickListener { action, text ->
        viewModel.addActionText(action, text)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentQrInfoBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        actionsAdapter.apply {
            photoActionClickListener = this@GoodsPassScreen.photoActionClickListener
            textActionClickListener = this@GoodsPassScreen.textActionClickListener
            takeImageCamera = this@GoodsPassScreen
        }

        binding.actionsList.apply {
            adapter = actionsAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        binding.apply {
            closeButton.setOnClickListener(buttonCloseClickHandler)
            sendResultButton.setOnClickListener(buttonSendResultClickHandler)
        }

        with(viewModel) {
            qrInfoUIState.observe(viewLifecycleOwner, uiStateObserver)
            sendQrResultUIState.observe(viewLifecycleOwner, sendQrResultUIStateObserver)
            loadQrInfo(qrContent)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun takeImage(listener: TakeImageCameraResultListener) {
        val takeImageBottomSheet = TakeImageBottomSheet(listener)
        takeImageBottomSheet.takeImage(parentFragmentManager)
    }

    private fun setQrLoadingState(isLoading: Boolean, isError: Boolean = false) {
        binding.apply {
            content.setVisibility(!isLoading && !isError)
            loadingErrorMessage.setVisibility(isError)
            circularProgress.setVisibility(isLoading)
        }
    }

    private fun onLoadingQRInfo() {
        setQrLoadingState(true)
    }

    private fun onSuccessLoadingData(wrapper: GoodsQrInfoUIWrapper) {
        setQrLoadingState(false)

        with(binding) {
            name.text = wrapper.description
            count.text = wrapper.count.toString()
            direction.text = wrapper.direction
        }

        actionsAdapter.updateItems(wrapper.qrId, wrapper.actionType, wrapper.actions)
    }

    private fun onErrorLoadingQrInfo(message: String) {
        setQrLoadingState(isLoading = false, isError = true)
        binding.loadingErrorMessage.text = message
    }

    private fun setLoadingSendQrResult(isLoading: Boolean) {
        binding.progress.setVisibility(isLoading)
        binding.sendResultButton.isEnabled = !isLoading
    }

    private fun onLoadingSendQrResult() {
        setLoadingSendQrResult(true)
    }

    private fun onSuccessSendQrResult(message: String) {
        setLoadingSendQrResult(false)
        showToast(message)
        dismiss()
    }

    private fun onErrorSendQrResult(errorMessage: String) {
        setLoadingSendQrResult(false)
        showToast(errorMessage)
    }
}