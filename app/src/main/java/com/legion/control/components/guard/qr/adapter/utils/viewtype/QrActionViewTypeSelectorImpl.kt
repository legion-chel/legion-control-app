package com.legion.control.components.guard.qr.adapter.utils.viewtype

import com.legion.control.components.guard.qr.adapter.utils.ActionViewType
import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.actions.data.ActionType
import javax.inject.Inject

class QrActionViewTypeSelectorImpl @Inject constructor() : QrActionViewTypeSelector {
    override fun getViewTypeByActionType(action: Action): ActionViewType {
        return when(action.type) {
            ActionType.PHOTO -> ActionViewType.PHOTO
            ActionType.TEXT -> ActionViewType.TEXT
        }
    }
}