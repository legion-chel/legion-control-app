package com.legion.control.components.client.pass.adapter

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.legion.control.databinding.ItemCreatedPhotoBinding

class AppendedPhotosAdapter : RecyclerView.Adapter<AppendedPhotosAdapter.CreatedPhotosHolder>() {
    private val createdPhotos = mutableListOf<Bitmap>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CreatedPhotosHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemCreatedPhotoBinding.inflate(layoutInflater, parent, false)

        return CreatedPhotosHolder(binding)
    }

    override fun onBindViewHolder(holder: CreatedPhotosHolder, position: Int) {
        val bitmap = createdPhotos[position]
        holder.bind(bitmap)
    }

    override fun getItemCount() = createdPhotos.size

    fun updatePhotos(createdPhotos: List<Bitmap>) {
        this.createdPhotos.apply {
            clear()
            addAll(createdPhotos)
        }

        notifyDataSetChanged()
    }

    class CreatedPhotosHolder(
        private val binding: ItemCreatedPhotoBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(bitmap: Bitmap) {
            binding.photo.setImageBitmap(bitmap)
        }
    }
}