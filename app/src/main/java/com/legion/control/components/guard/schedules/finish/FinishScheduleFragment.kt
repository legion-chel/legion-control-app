package com.legion.control.components.guard.schedules.finish

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.viewModels
import com.google.android.material.button.MaterialButton
import com.google.android.material.progressindicator.CircularProgressIndicator
import com.google.android.material.textview.MaterialTextView
import com.legion.control.components.guard.schedules.core.ActionScheduleFragment
import com.legion.control.components.guard.schedules.core.ActionShiftViewModel
import com.legion.control.databinding.FragmentFinishScheduleBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory

class FinishScheduleFragment(
    private val factory: ViewModelFactory
) : ActionScheduleFragment() {

    private lateinit var binding: FragmentFinishScheduleBinding

    private val viewModel: FinishShiftViewModel by viewModels { factory }

    override val actionViewModel: ActionShiftViewModel
        get() = viewModel

    override val text: MaterialTextView
        get() = binding.text

    override val photo: AppCompatImageView
        get() = binding.photo

    override val button: MaterialButton
        get() = binding.buttonAction

    override val buttonClose: ImageButton
        get() = binding.buttonClose

    override val progress: CircularProgressIndicator
        get() = binding.progressIndicator

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFinishScheduleBinding.inflate(layoutInflater)
        return binding.root
    }
}