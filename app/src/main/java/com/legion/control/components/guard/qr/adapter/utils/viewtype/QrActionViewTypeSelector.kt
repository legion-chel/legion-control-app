package com.legion.control.components.guard.qr.adapter.utils.viewtype

import com.legion.control.components.guard.qr.adapter.utils.ActionViewType
import com.legion.control.models.qr.actions.data.Action

interface QrActionViewTypeSelector {
    fun getViewTypeByActionType(action: Action): ActionViewType
}