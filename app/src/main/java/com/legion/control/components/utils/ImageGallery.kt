package com.legion.control.components.utils

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.legion.control.databinding.ItemImageGalleryBinding

class ImageGallery(
    private val clickListener: ((bitmap: Bitmap) -> Unit)? = null
) : RecyclerView.Adapter<ImageGallery.ImageGalleryHolder>() {
    
    private val images = mutableListOf<Bitmap>()
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageGalleryHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemImageGalleryBinding.inflate(layoutInflater, parent, false)
        
        return ImageGalleryHolder(binding)
    }
    
    override fun onBindViewHolder(holder: ImageGalleryHolder, position: Int) {
        val image = images[position]
        holder.bind(image)
    }
    
    override fun getItemCount() = images.size
    
    fun add(bitmap: Bitmap) {
        images.add(bitmap)
        notifyDataSetChanged()
    }
    
    fun update(bitmaps: List<Bitmap>) {
        images.apply {
            clear()
            addAll(bitmaps)
        }
        
        notifyDataSetChanged()
    }
    
    fun remove(bitmap: Bitmap) {
    
    }
    
    class ImageGalleryHolder(
        private val binding: ItemImageGalleryBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        
        fun bind(bitmap: Bitmap) {
            binding.image.setImageBitmap(bitmap)
        }
    }
}