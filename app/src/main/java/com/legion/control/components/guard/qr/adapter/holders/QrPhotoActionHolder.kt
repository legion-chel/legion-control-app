package com.legion.control.components.guard.qr.adapter.holders

import android.graphics.Bitmap
import android.net.Uri
import com.legion.camera.core.TakeImageCamera
import com.legion.camera.core.TakeImageCameraResultListener
import com.legion.control.components.guard.qr.adapter.data.ActionUIWrapper
import com.legion.control.components.guard.qr.adapter.listeners.OnActionPhotoClickListener
import com.legion.control.databinding.ItemQrPhotoActionBinding

class QrPhotoActionHolder(
    private val binding: ItemQrPhotoActionBinding
) : QrActionHolder(binding.root) {

    fun bind(
        wrapper: ActionUIWrapper,
        takeImageCamera: TakeImageCamera?,
        listener: OnActionPhotoClickListener?
    ) {
        val button = binding.takeImageButton

        button.text = wrapper.actionText
        button.isEnabled = wrapper.buttonEnabled
        button.setOnClickListener {
            val resultListener = object : TakeImageCameraResultListener {
                override fun onSuccess(imageUri: Uri, bitmap: Bitmap?) {
                    listener?.onClick(wrapper.action, imageUri, bitmap)
                }

                override fun onError(throwable: Throwable?) {

                }
            }

            takeImageCamera?.takeImage(resultListener)
        }
    }
}