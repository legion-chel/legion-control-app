package com.legion.control.components.organizations.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.legion.control.components.organizations.OrganizationsModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

class OrganizationsViewModel @Inject constructor(
    private val model: OrganizationsModel
) : ViewModel() {

    val organizations = model
        .organization
        .asLiveData(IO)

    fun load() {
        viewModelScope.launch {
            model.loadOrganizations()
        }
    }
}