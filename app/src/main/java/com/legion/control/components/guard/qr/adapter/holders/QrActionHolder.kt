package com.legion.control.components.guard.qr.adapter.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class QrActionHolder(view: View) : RecyclerView.ViewHolder(view)