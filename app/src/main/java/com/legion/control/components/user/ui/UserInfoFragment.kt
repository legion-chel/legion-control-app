package com.legion.control.components.user.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.legion.control.databinding.FragmentUserInfoBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.control.models.ui.adapter.MapAdapter
import com.legion.control.models.ui.adapter.MapItemDecoration
import javax.inject.Inject

class UserInfoFragment @Inject constructor(
    private val factory: ViewModelFactory,
    private val mapAdapter: MapAdapter,
    private val userInfoItemDecorator: MapItemDecoration
) : Fragment() {
    private lateinit var binding: FragmentUserInfoBinding

    private val viewModel: UserInfoViewModel by viewModels { factory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserInfoBinding.inflate(inflater, container, false)

        binding.userInfo.apply {
            adapter = mapAdapter
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(userInfoItemDecorator)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.load()

        viewModel.userInfo.observe(viewLifecycleOwner) {
            mapAdapter.update(it ?: return@observe)
        }
    }
}