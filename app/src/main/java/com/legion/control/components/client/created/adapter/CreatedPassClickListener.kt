package com.legion.control.components.client.created.adapter

import com.legion.control.models.client.data.CreatedPass

fun interface CreatedPassClickListener {
    fun onClick(pass: CreatedPass)
}