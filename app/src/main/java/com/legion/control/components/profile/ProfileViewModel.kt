package com.legion.control.components.profile

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.legion.control.models.auth.model.AuthModel
import com.legion.control.models.upload.cases.UploadImageUseCase
import com.legion.control.models.upload.data.UploadImageResult
import com.legion.control.models.upload.data.UploadImageState
import com.legion.control.models.upload.data.UploadImageType
import com.legion.core.session.UserSessionFacade
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
    private val userSessionFacade: UserSessionFacade,
    private val authModel: AuthModel,
    private val uploadImageUseCase: UploadImageUseCase
) : ViewModel() {
    
    private val _avatarUrl = MutableLiveData<String>(userSessionFacade.user?.avatar)
    val avatarUri: LiveData<String> get() = _avatarUrl
    
    init {
        viewModelScope.launch {
            uploadImageUseCase.uploadImageState
                .collect {
                    handleUploadImageResult(it)
                }
        }
    }

    fun getAuthToken() = "Bearer ${userSessionFacade.token}"
    
    fun uploadAvatar(avatarUri: Uri) {
        viewModelScope.launch {
            uploadImageUseCase.uploadImage(avatarUri, UploadImageType.AVATAR)
        }
    }

    fun getFullName(): String? {
        val user = userSessionFacade.user ?: return null

        return "${user.firstName} ${user.lastName}"
    }

    fun logout() {
        viewModelScope.launch {
            authModel.logout()
        }
    }
    
    private suspend fun handleUploadImageResult(state: UploadImageState?) = withContext(Dispatchers.Default) {
        if (state is UploadImageState.Success) {
            val imageUrl = state.result.url
            _avatarUrl.postValue(imageUrl)
        }
    }
}