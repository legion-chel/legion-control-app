package com.legion.control.components.guard.rounds

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.legion.control.models.qr.actions.cases.SendQrActionsResultUseCase
import com.legion.control.models.qr.actions.cases.SetCompleteActionUseCase
import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.info.data.ActionType
import com.legion.control.models.rounds.entities.RoundUIWrapper
import com.legion.control.models.rounds.usecases.GetRoundsUseCases
import com.legion.control.models.utils.TransformLoadingStateToUIStateUseCase
import com.legion.core.state.UIState
import com.legion.utils.extensions.asImmutable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.launch
import javax.inject.Inject

class RoundsViewModel @Inject constructor(
    private val getRoundUseCases: GetRoundsUseCases,
    private val sendQrActionsResultUseCase: SendQrActionsResultUseCase,
    private val setCompleteActionUseCase: SetCompleteActionUseCase,
    private val transformLoadingStateToUIStateUseCase: TransformLoadingStateToUIStateUseCase
) : ViewModel() {

    private val photoActions = mutableMapOf<Action, Uri>()
    private val textActions = mutableMapOf<Action, String>()

    private val _roundInfo = MutableLiveData<UIState<RoundUIWrapper>>(UIState.Loading)
    val roundInfo get() = _roundInfo.asImmutable()

    val sendActionsUIState = sendQrActionsResultUseCase
        .sendLoadingState
        .transform {
            val state = transformLoadingStateToUIStateUseCase(it) ?: return@transform
            emit(state)
        }
        .asLiveData(Dispatchers.Default)

    fun loadRoundsInfo() {
        viewModelScope.launch {
            getRoundUseCases()
                .collect {
                    val uiState = transformLoadingStateToUIStateUseCase(it) ?: return@collect
                    _roundInfo.postValue(uiState)
                }
        }
    }

    fun addActionPhoto(action: Action, imageUri: Uri) {
        photoActions[action] = imageUri
        viewModelScope.launch(Dispatchers.IO) {
            setCompleteActionUseCase.setComplete(action)
        }
    }

    fun addActionText(action: Action, text: String) {
        textActions[action] = text
        viewModelScope.launch(Dispatchers.IO) {
            setCompleteActionUseCase.setComplete(action)
        }
    }

    fun sendActionsResult(qrId: Int, actionType: ActionType) {
        viewModelScope.launch(Dispatchers.IO) {
            sendQrActionsResultUseCase.send(qrId, actionType, photoActions, textActions)
        }
    }
}