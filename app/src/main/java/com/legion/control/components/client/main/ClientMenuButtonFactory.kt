package com.legion.control.components.client.main

import com.legion.control.R
import com.legion.control.models.ui.adapter.AppAdapter
import com.legion.control.models.ui.menu.ButtonType
import com.legion.control.models.ui.menu.MenuButton
import com.legion.utils.ResourceProvider
import javax.inject.Inject

class ClientMenuButtonFactory @Inject constructor(
    private val resourceProvider: ResourceProvider
) : AppAdapter.FactoryItems<MenuButton> {

    override fun create(): List<MenuButton> = listOf(
        createButton(ButtonType.CREATE_GOODS_PASS, R.string.create_goods_pass)
    )

    private fun createButton(type: ButtonType, stringId: Int): MenuButton {
        val title = resourceProvider.getString(stringId)
        return MenuButton(type, title)
    }
}