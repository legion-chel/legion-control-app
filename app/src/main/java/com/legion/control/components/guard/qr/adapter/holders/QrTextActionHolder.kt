package com.legion.control.components.guard.qr.adapter.holders

import androidx.core.widget.doAfterTextChanged
import com.legion.control.components.guard.qr.adapter.listeners.OnActionTextClickListener
import com.legion.control.databinding.ItemQrTextHolderBinding
import com.legion.control.models.qr.actions.data.Action
import com.legion.utils.extensions.empty

class QrTextActionHolder(
    private val binding: ItemQrTextHolderBinding
) : QrActionHolder(binding.root) {
    
    fun bind(action: Action, listener: OnActionTextClickListener?) {
        binding.inputLayout.hint = action.description
        binding.input.doAfterTextChanged {
            val text = it?.toString() ?: String.empty
            listener?.onClick(action, text)
        }
    }
}