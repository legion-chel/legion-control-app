package com.legion.control.components.client.created.adapter.holders

import com.legion.control.components.client.created.adapter.CreatedPassClickListener
import com.legion.control.components.client.created.adapter.CreatedPassViewHolder
import com.legion.control.databinding.ItemCreatedPassBinding
import com.legion.control.models.client.data.CreatedPass

class EmployeeCreatedPassViewHolder(
    binding: ItemCreatedPassBinding
) : CreatedPassViewHolder(binding) {

    override fun bind(pass: CreatedPass, clickListener: CreatedPassClickListener?) {
        super.bind(pass, clickListener)
        val employeePass = pass as? CreatedPass.Employee ?: return
        binding.name.text = getNameString(employeePass)
    }

    private fun getNameString(goodsPass: CreatedPass.Employee): String {
        val (name, secondName, patronymic) = goodsPass
        return "$secondName $name $patronymic"
    }
}