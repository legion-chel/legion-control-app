package com.legion.control.components.client.pass.employee

import android.graphics.Bitmap
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.legion.camera.TakeImageBottomSheet
import com.legion.camera.core.TakeImageCameraResultListener
import com.legion.control.components.client.pass.adapter.AppendedPhotosAdapter
import com.legion.control.databinding.FragmentCreateEmployeePassBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.core.bottomsheet.AppBottomSheetFragment
import com.legion.core.state.UIState
import com.legion.utils.extensions.setVisibility
import com.legion.utils.extensions.showToast
import javax.inject.Inject

class CreateEmployeePassFragment @Inject constructor(
    private val factory: ViewModelFactory
) : AppBottomSheetFragment() {

    private var _binding: FragmentCreateEmployeePassBinding? = null
    val binding
        get() = _binding!!

    override val isDraggable: Boolean
        get() = false

    private val appendedPhotosAdapter = AppendedPhotosAdapter()

    private val viewModel: CreateEmployeePassViewModel by viewModels { factory }

    private val inputSecondNameActionListener = TextView.OnEditorActionListener { _, _, _ ->
        binding.firstNameInput.requestFocus()
        true
    }

    private val inputFirstNameActionListener = TextView.OnEditorActionListener { _, _, _ ->
        binding.patronymicInput.requestFocus()
        true
    }

    private val inputPatronymicActionListener = TextView.OnEditorActionListener { _, _, _ ->
        viewModel.create()
        true
    }

    private val buttonCreateClickHandler = View.OnClickListener {
        viewModel.create()
    }

    private val takeImageCameraResultListener = object : TakeImageCameraResultListener {
        override fun onSuccess(imageUri: Uri, bitmap: Bitmap?) {
            viewModel.appendPhoto(imageUri, bitmap)
        }

        override fun onError(throwable: Throwable?) {

        }
    }

    private val buttonAppendPhotoClickHandler = View.OnClickListener {
        val takeImageBottomSheet = TakeImageBottomSheet(takeImageCameraResultListener)
        takeImageBottomSheet.takeImage(parentFragmentManager)
    }

    private val buttonCloseClickHandler = View.OnClickListener {
        dismiss()
    }

    private val uiStateObserver = Observer<UIState<String>> {
        when(it) {
            is UIState.Loading -> onLoading()
            is UIState.Success -> onSuccess(it.data)
            is UIState.Error -> onError(it.error)
        }
    }

    private val appendedPhotosObserver = Observer<List<Bitmap>> {
        appendedPhotosAdapter.updatePhotos(it ?: return@Observer)
    }

    override fun onCreateView(layoutInflater: LayoutInflater): View {
        _binding = FragmentCreateEmployeePassBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onCreateDialog(dialogFragment: BottomSheetDialog) {
        dialogFragment.apply {
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
            behavior.peekHeight = BottomSheetBehavior.PEEK_HEIGHT_AUTO
        }
    
    
        with(binding.secondNameInput) {
            doAfterTextChanged(viewModel.inputSecondNameDelegate)
            setOnEditorActionListener(inputSecondNameActionListener)
        }

        with(binding.firstNameInput) {
            doAfterTextChanged(viewModel.inputFirstNameDelegate)
            setOnEditorActionListener(inputFirstNameActionListener)
        }

        with(binding.patronymicInput) {
            doAfterTextChanged(viewModel.inputPatronymicDelegate)
            setOnEditorActionListener(inputPatronymicActionListener)
        }

        with(binding) {
            buttonClose.setOnClickListener(buttonCloseClickHandler)
            buttonCreate.setOnClickListener(buttonCreateClickHandler)
        }

        with(binding.appendedPhotos) {
            adapter = appendedPhotosAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        }

        binding.buttonAppendPhoto.setOnClickListener(buttonAppendPhotoClickHandler)

        val viewLifecycleOwner = this
        with(viewModel) {
            uiState.observe(viewLifecycleOwner, uiStateObserver)
            appendedPhotos.observe(viewLifecycleOwner, appendedPhotosObserver)
        }
    }

    override fun getMinHeight(displayHeight: Int) = displayHeight

    private fun onLoading() {
        binding.progress.setVisibility(true)
    }

    private fun onSuccess(message: String) {
        binding.progress.setVisibility(false)
        showToast(message)
        dismiss()
    }

    private fun onError(error: String) {
        binding.progress.setVisibility(false)
        showToast(error)
    }
}