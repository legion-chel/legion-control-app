package com.legion.control.components.guard.schedules.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.material.button.MaterialButton
import com.legion.control.databinding.FragmentShiftBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.control.models.schedules.data.ScheduleActState
import com.legion.control.models.schedules.data.ScheduleActionDelegate
import com.legion.control.models.schedules.data.ui.ScheduleUIWrapper
import javax.inject.Inject

class ShiftFragment @Inject constructor(
    private val factory: ViewModelFactory
) : Fragment() {
    private lateinit var binding: FragmentShiftBinding

    private val title: TextView by lazy {
        binding.title
    }

    private val description: TextView by lazy {
        binding.description
    }

    private val dateStart: TextView by lazy {
        binding.dateStart
    }

    private val dateEnd: TextView by lazy {
        binding.dateEnd
    }

    private val actionButton: MaterialButton by lazy {
        binding.shiftActionButton
    }

    private val viewModel: ShiftViewModel by viewModels { factory }

    private val scheduleLoadingStateObserver = Observer<ScheduleActState?> {
        it ?: return@Observer
    }

    private val currentScheduleObserver = Observer<ScheduleUIWrapper> {
        it ?: return@Observer
        title.text = it.title
        description.text = it.description
        dateStart.text = it.timeStart
        dateEnd.text = it.timeEnd
        setButton(it.actionDelegate ?: return@Observer)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentShiftBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startObservingData()
    }

    private fun startObservingData() {
        with(viewModel) {
            scheduleLoadingState.observe(viewLifecycleOwner, scheduleLoadingStateObserver)
            currentSchedule.observe(viewLifecycleOwner, currentScheduleObserver)
        }
    }

    private fun setButton(shiftAction: ScheduleActionDelegate) {
        with(actionButton) {
            text = shiftAction.title
            setOnClickListener(createButtonClickHandler(shiftAction.action))
        }
    }

    private fun createButtonClickHandler(action: (FragmentManager) -> Unit) = View.OnClickListener {
        action.invoke(parentFragmentManager)
    }
}