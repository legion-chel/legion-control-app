package com.legion.control.components.guard.schedules.core

import android.content.res.Resources
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.progressindicator.CircularProgressIndicator
import com.google.android.material.textview.MaterialTextView
import com.legion.camera.TakeImageBottomSheet
import com.legion.camera.core.CameraType
import com.legion.camera.core.TakeImageCameraResultListener
import com.legion.control.models.schedules.data.ScheduleActState
import com.legion.utils.extensions.setVisibility
import com.legion.utils.extensions.showToast


abstract class ActionScheduleFragment : BottomSheetDialogFragment() {
    protected abstract val actionViewModel: ActionShiftViewModel

    protected abstract val text: MaterialTextView
    protected abstract val photo: AppCompatImageView
    protected abstract val button: MaterialButton
    protected abstract val buttonClose: ImageButton
    protected abstract val progress: CircularProgressIndicator

    private val takeImageCameraResultListener = object : TakeImageCameraResultListener {
        override fun onSuccess(imageUri: Uri, bitmap: Bitmap?) {
            actionViewModel.setImage(imageUri)
        }

        override fun onError(throwable: Throwable?) {
            showToast("Ошибка при получении изображения")
        }

    }

    private val buttonEnabledObserver = Observer<Boolean> {
        button.isEnabled = it ?: return@Observer
    }

    private val buttonTextObserver = Observer<String> {
        button.text = it ?: return@Observer
    }

    private val textIsVisibleObserver = Observer<Boolean> {
        text.setVisibility(it ?: return@Observer)
    }

    private val photoObserver = Observer<Bitmap?> {
        if (it == null) button.setOnClickListener(onNotImageClick)
        else {
            photo.setImageBitmap(it)
            button.setOnClickListener(onWithImageClick)
        }
    }

    private val scheduleActState = Observer<ScheduleActState?> {
        when(it) {
            is ScheduleActState.Success.WithMessage -> {
                showToast(it.message)
                dismiss()
            }

            is ScheduleActState.Error.WithMessage -> showToast(it.message)
            else -> return@Observer
        }

        if (it is ScheduleActState.Success) dismiss()
    }

    private val onNotImageClick = View.OnClickListener {
        val takeImageBottomSheet = TakeImageBottomSheet(takeImageCameraResultListener, CameraType.FRONT)
        takeImageBottomSheet.takeImage(parentFragmentManager)
    }

    private val onWithImageClick = View.OnClickListener {
        actionViewModel.executeShiftAction()
    }

    private val onButtonCloseClick = View.OnClickListener {
        dismiss()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bottomSheet = dialog as? BottomSheetDialog
        bottomSheet?.behavior?.state = BottomSheetBehavior.STATE_EXPANDED

        view.layoutParams.height = Resources.getSystem().displayMetrics.heightPixels

        buttonClose.setOnClickListener(onButtonCloseClick)
        startObservingData()
    }

    private fun startObservingData() {
        val lifecycleOwner = this

        with(actionViewModel) {
            textIsVisible.observe(lifecycleOwner, textIsVisibleObserver)
            buttonIsEnabled.observe(lifecycleOwner, buttonEnabledObserver)
            buttonText.observe(lifecycleOwner, buttonTextObserver)
            photo.observe(lifecycleOwner, photoObserver)
            actState.observe(lifecycleOwner, scheduleActState)
        }
    }
}