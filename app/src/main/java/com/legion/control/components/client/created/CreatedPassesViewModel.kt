package com.legion.control.components.client.created

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.legion.control.models.client.scenario.GetCreatedPassesScenario
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class CreatedPassesViewModel @Inject constructor(
    private val getCreatedPassesScenario: GetCreatedPassesScenario
) : ViewModel() {

    val uiState = getCreatedPassesScenario
        .uiState
        .asLiveData(Dispatchers.Default)

    fun loadCreatedPasses() {
        viewModelScope.launch {
            getCreatedPassesScenario.getCreatedPasses()
        }
    }
}