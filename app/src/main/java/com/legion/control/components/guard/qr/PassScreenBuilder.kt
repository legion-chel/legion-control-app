package com.legion.control.components.guard.qr

import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.legion.control.components.guard.qr.adapter.QrActionsAdapter
import com.legion.control.components.guard.qr.adapter.utils.viewholder.QrActionViewHolderBuilder
import com.legion.control.components.guard.qr.adapter.utils.viewtype.QrActionViewTypeSelector
import com.legion.control.components.guard.qr.employee.EmployeePassScreen
import com.legion.control.components.guard.qr.goods.GoodsPassScreen
import com.legion.control.components.guard.qr.visitor.VisitorPassScreen
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.control.models.qr.info.data.QrContent
import com.legion.control.models.qr.info.data.ActionType
import javax.inject.Inject

class PassScreenBuilder @Inject constructor(
    private val factory: ViewModelFactory,
    private val selector: QrActionViewTypeSelector,
    private val builder: QrActionViewHolderBuilder
) {
    fun build(qrContent: QrContent): BottomSheetDialogFragment? {
        val adapter = QrActionsAdapter(selector, builder)

        return when (qrContent.type) {
            ActionType.GOODS -> GoodsPassScreen(factory, adapter, qrContent)
            ActionType.EMPLOYEE_PASS -> EmployeePassScreen(factory, adapter, qrContent)
            ActionType.VISITOR_PASS -> VisitorPassScreen(factory, adapter, qrContent)
            else -> return null
        }
    }
}