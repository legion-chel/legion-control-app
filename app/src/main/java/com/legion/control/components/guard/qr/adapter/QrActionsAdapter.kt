package com.legion.control.components.guard.qr.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.legion.camera.core.TakeImageCamera
import com.legion.control.components.guard.qr.adapter.data.ActionUIWrapper
import com.legion.control.components.guard.qr.adapter.holders.QrActionHolder
import com.legion.control.components.guard.qr.adapter.holders.QrPhotoActionHolder
import com.legion.control.components.guard.qr.adapter.holders.QrTextActionHolder
import com.legion.control.components.guard.qr.adapter.listeners.OnActionPhotoClickListener
import com.legion.control.components.guard.qr.adapter.listeners.OnActionTextClickListener
import com.legion.control.components.guard.qr.adapter.utils.ActionViewType
import com.legion.control.components.guard.qr.adapter.utils.viewholder.QrActionViewHolderBuilder
import com.legion.control.components.guard.qr.adapter.utils.viewtype.QrActionViewTypeSelector
import com.legion.control.models.qr.info.data.ActionType

class QrActionsAdapter(
    private val selector: QrActionViewTypeSelector,
    private val builder: QrActionViewHolderBuilder
) : RecyclerView.Adapter<QrActionHolder>() {

    var qrId = 0
    var actionType: ActionType? = null

    var photoActionClickListener: OnActionPhotoClickListener? = null
    var textActionClickListener: OnActionTextClickListener? = null
    var takeImageCamera: TakeImageCamera? = null

    private val wrappers = mutableListOf<ActionUIWrapper>()

    override fun getItemViewType(position: Int): Int {
        val action = wrappers[position].action
        return selector
            .getViewTypeByActionType(action)
            .viewType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QrActionHolder {
        val inflater = LayoutInflater.from(parent.context)
        val type = ActionViewType
            .values()
            .find { it.viewType == viewType }

        checkNotNull(type) {
            "incorrect view type value"
        }

        return builder.build(type, inflater, parent)
    }

    override fun onBindViewHolder(holder: QrActionHolder, position: Int) {
        val wrapper = wrappers[position]
        when(holder) {
            is QrPhotoActionHolder -> holder.bind(wrapper, takeImageCamera, photoActionClickListener)
            is QrTextActionHolder -> holder.bind(wrapper.action, textActionClickListener)
        }
    }

    override fun getItemCount() = wrappers.size

    fun updateItems(qrId: Int, actionType: ActionType, items: List<ActionUIWrapper>) {
        this.qrId = qrId
        this.actionType = actionType

        wrappers.apply {
            clear()
            addAll(items)
        }

        notifyDataSetChanged()
    }
}