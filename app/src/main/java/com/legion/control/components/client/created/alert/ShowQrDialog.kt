package com.legion.control.components.client.created.alert

import android.view.LayoutInflater
import android.view.View
import coil.imageLoader
import coil.request.ImageRequest
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.legion.control.databinding.DialogShowQrBinding
import com.legion.core.bottomsheet.AppBottomSheetFragment

class ShowQrDialog(private val qrUrl: String) : AppBottomSheetFragment() {
    private var _binding: DialogShowQrBinding? = null
    val binding
        get() = _binding!!

    override val isFullScreen: Boolean
        get() = true

    override val maxHeight: Int
        get() = (super.maxHeight / 3) * 2

    override fun onCreateView(layoutInflater: LayoutInflater): View {
        _binding = DialogShowQrBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun getMinHeight(displayHeight: Int) = displayHeight

    override fun onCreateDialog(dialogFragment: BottomSheetDialog) {
        val request = ImageRequest.Builder(requireContext())
            .data(qrUrl)
            .target { drawable ->
                binding.qr.setImageDrawable(drawable)
            }
            .build()
        requireContext().imageLoader.enqueue(request)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}