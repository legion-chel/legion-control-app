package com.legion.control.components.utils

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class ImageGalleryItemDecorator(
    private val verticalMargin: Int = 0,
    private val horizontalMargin: Int = 0
) : RecyclerView.ItemDecoration() {
    
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildLayoutPosition(view)
        
        val isFirst = position == 0
        
        setHorizontalMargin(outRect, isFirst)
    }
    
    
    private fun setHorizontalMargin(outRect: Rect, isFirst: Boolean) {
        outRect.apply {
            if (isFirst) left = horizontalMargin
            right = horizontalMargin
        }
    }
}