package com.legion.control.components.home.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class HomePageAdapter(
    fragmentActivity: FragmentActivity,
    private val pages: List<Fragment>
) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount() = pages.size

    override fun createFragment(position: Int) = pages[position]
}