package com.legion.control.components.splash

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.legion.control.models.auth.model.AuthModel
import com.legion.control.models.sync.DataSynchronizationModule
import com.legion.control.models.sync.data.SyncState
import com.legion.control.models.sync.scenario.HandleSyncStateScenario
import com.legion.control.models.utils.ConfigureCrashlyticsUseCase
import com.legion.core.session.UserSessionFacade
import com.legion.utils.extensions.asImmutable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val authModel: AuthModel,
    private val userSessionFacade: UserSessionFacade,
    private val dataSynchronizationModule: DataSynchronizationModule,
    private val configureCrashlyticsUseCase: ConfigureCrashlyticsUseCase,
    private val handleSyncStateScenario: HandleSyncStateScenario
) : ViewModel() {
    
    private val _loadingState = MutableLiveData<Boolean>()
    val loadingState get() = _loadingState.asImmutable()
    
    fun syncData() {
        viewModelScope.launch {
            if (authModel.isAuthorized) startSync()
            else onSyncState(SyncState.NEED_AUTH)
        }
    }

    private suspend fun startSync() {
        withContext(IO) {
            if (authModel.isNeedRefresh) authModel.refresh()
            
            dataSynchronizationModule.startSync()
        
            val isRegistrationEnded = userSessionFacade
                .user
                ?.isRegistrationEnded
                ?: true
        
            val state = if (isRegistrationEnded) SyncState.DONE else SyncState.NEED_ENTER_INFO
            onSyncState(state)
        
            configureCrashlyticsUseCase()
        }
    }

    private suspend fun onSyncState(state: SyncState) {
        val isLoading = handleSyncStateScenario.getLoadingBySyncState(state)
        _loadingState.postValue(isLoading)
        
        withContext(Dispatchers.Main) {
            handleSyncStateScenario.navigateBySyncState(state)
        }
    }
}