package com.legion.control.components.guard.schedules.core

import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.legion.control.models.schedules.cases.ActScheduleUseCase
import com.legion.control.models.schedules.scenario.ActScheduleUIScenario
import com.legion.control.models.upload.data.UploadImageState
import com.legion.control.models.upload.data.UploadImageType
import com.legion.control.models.upload.scenario.UploadImageScenario
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

abstract class ActionShiftViewModel(
    private val actScheduleUseCase: ActScheduleUseCase,
    private val uploadImageScenario: UploadImageScenario,
    private val actUIScenario: ActScheduleUIScenario,
) : ViewModel() {

    val uploadImageError = uploadImageScenario
        .uploadImageState
        .filter { it is UploadImageState.Error }
        .map { it as UploadImageState.Error }
        .asLiveData(Default)

    val actState = actScheduleUseCase
        .actState
        .asLiveData(Default)

    val photo = uploadImageScenario
        .bitmap
        .onEach {
            actUIScenario.getButtonTitle(it)
            actUIScenario.getTextVisibility(it)
        }
        .asLiveData(Default)

    val buttonIsEnabled = actUIScenario
        .buttonIsEnabled
        .asLiveData(Default)

    val buttonText = actUIScenario
        .buttonTitle
        .asLiveData(Default)

    val textIsVisible = actUIScenario
        .textIsVisible
        .asLiveData(Default)

    fun executeShiftAction() {
        viewModelScope.launch(IO) {
            uploadImageScenario.uploadImage(UploadImageType.SELFIE)

            uploadImageScenario
                .uploadImageState
                .collect(this@ActionShiftViewModel::onUploadImageState)
        }
    }

    private suspend fun onUploadImageState(uploadImageState: UploadImageState?) {
        uploadImageState ?: return
        if (uploadImageState is UploadImageState.Success) {
            val imageId = uploadImageState.result.imageId
            actScheduleUseCase.executeAct(imageId)
        }
    }

    fun setImage(imageUri: Uri) {
        viewModelScope.launch(IO) {
            uploadImageScenario.saveImage(imageUri)
        }
    }
}