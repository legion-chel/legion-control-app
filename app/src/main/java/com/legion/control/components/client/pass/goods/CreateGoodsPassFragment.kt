package com.legion.control.components.client.pass.goods

import android.graphics.Bitmap
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.legion.camera.TakeImageBottomSheet
import com.legion.camera.core.TakeImageCameraResultListener
import com.legion.control.components.client.pass.adapter.AppendedPhotosAdapter
import com.legion.control.databinding.FragmentCreateGoodsPassBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.core.bottomsheet.AppBottomSheetFragment
import com.legion.core.state.UIState
import com.legion.utils.extensions.hideKeyboard
import com.legion.utils.extensions.setVisibility
import com.legion.utils.extensions.showToast

class CreateGoodsPassFragment(
    private val factory: ViewModelFactory
) : AppBottomSheetFragment() {

    private var _binging: FragmentCreateGoodsPassBinding? = null
    val binding
        get() = _binging!!

    override val isDraggable: Boolean
        get() = false

    private val imageCameraResultListener = object : TakeImageCameraResultListener {
        override fun onSuccess(imageUri: Uri, bitmap: Bitmap?) {
            viewModel.appendImage(imageUri, bitmap)
        }

        override fun onError(throwable: Throwable?) {
            showToast("Ошибка получения фоторгафии")
        }

    }

    private val viewModel: CreateGoodsPassViewModel by viewModels { factory }

    private val createdPhotosAdapter = AppendedPhotosAdapter()

    private val inputDescriptionActionListener = TextView.OnEditorActionListener { _, _, _ ->
        binding.countInput.requestFocus()
        true
    }

    private val inputCountActionListener = TextView.OnEditorActionListener { _, _, _ ->
        hideKeyboard()
        true
    }

    private val createdPhotosObserver = Observer<List<Bitmap>> {
        createdPhotosAdapter.updatePhotos(it ?: return@Observer)
    }

    private val uiStateObserver = Observer<UIState<String>> {
        when(it) {
            is UIState.Loading -> onLoading()
            is UIState.Success -> onSuccess(it.data)
            is UIState.Error -> onError(it.error)
            else -> return@Observer
        }
    }

    private val buttonCreateClickHandler = View.OnClickListener {
        viewModel.create()
    }

    private val buttonAddPhotoClickHandler = View.OnClickListener {
        val takeImageBottomSheet = TakeImageBottomSheet(imageCameraResultListener)
        takeImageBottomSheet.takeImage(parentFragmentManager)
    }

    private val oItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            viewModel.onSelectDirection(position)
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {}
    }

    override fun onCreateView(layoutInflater: LayoutInflater): View {
        _binging = FragmentCreateGoodsPassBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onCreateDialog(dialogFragment: BottomSheetDialog) {
        dialogFragment.apply {
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
            behavior.peekHeight = BottomSheetBehavior.PEEK_HEIGHT_AUTO
        }

        binding.descriptionInput.apply {
            doAfterTextChanged(viewModel.inputDescriptionDelegate)
            setOnEditorActionListener(inputDescriptionActionListener)
        }

        binding.countInput.apply {
            doAfterTextChanged(viewModel.inputCountDelegate)
            setOnEditorActionListener(inputCountActionListener)
        }

        binding.apply {
            closeButton.setOnClickListener { dismiss() }
            directionInput.onItemSelectedListener = oItemSelectedListener
            buttonCreate.setOnClickListener(buttonCreateClickHandler)
            buttonAddPhoto.setOnClickListener(buttonAddPhotoClickHandler)
        }

        binding.createdPhotos.apply {
            adapter = createdPhotosAdapter
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.HORIZONTAL,
                false
            )
        }

        setDirections()

        with(viewModel) {
            uiState.observe(this@CreateGoodsPassFragment, uiStateObserver)
            createdPhotos.observe(this@CreateGoodsPassFragment, createdPhotosObserver)
        }
    }

    override fun getMinHeight(displayHeight: Int) = displayHeight

    private fun setDirections() {
        val directions = viewModel
            .getGoodsDirections()

        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, directions).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        binding.directionInput.adapter = adapter
    }

    private fun onLoading() {
        binding.progress.setVisibility(true)
    }

    private fun onSuccess(message: String) {
        binding.progress.setVisibility(false)
        showToast(message)
        dismiss()
    }

    private fun onError(error: String) {
        binding.progress.setVisibility(false)
        showToast(error)
    }
}