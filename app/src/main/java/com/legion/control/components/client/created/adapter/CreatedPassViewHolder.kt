package com.legion.control.components.client.created.adapter

import androidx.recyclerview.widget.RecyclerView
import com.legion.control.databinding.ItemCreatedPassBinding
import com.legion.control.models.client.data.CreatedPass
import com.legion.utils.extensions.empty

abstract class CreatedPassViewHolder(
    protected val binding: ItemCreatedPassBinding
) : RecyclerView.ViewHolder(binding.root) {

    open fun bind(pass: CreatedPass, clickListener: CreatedPassClickListener?) {
        binding.name.text = String.empty
        binding.createdDate.text = String.empty

        binding.rootContainer.setOnClickListener {
            clickListener?.onClick(pass)
        }
    }
}