package com.legion.control.components.guard.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.legion.control.R
import com.legion.control.components.guard.reports.CreateReportFragment
import com.legion.control.components.guard.qr.adapter.QrActionsAdapter
import com.legion.control.components.guard.qr.adapter.utils.viewholder.QrActionViewHolderBuilder
import com.legion.control.components.guard.qr.adapter.utils.viewtype.QrActionViewTypeSelector
import com.legion.control.components.guard.rounds.RoundsFragment
import com.legion.control.databinding.FragmentActionsBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.control.models.ui.AppFragment
import com.legion.control.models.ui.adapter.AppAdapter
import com.legion.control.models.ui.menu.ButtonType
import com.legion.control.models.ui.menu.GridMenuAdapter
import com.legion.control.models.ui.menu.MenuButton
import com.legion.qr.QrScanListener
import com.legion.qr.QrScanner
import com.legion.qr.ScanResult
import com.legion.utils.extensions.showMessage
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GuardMainFragment @Inject constructor(
    private val factory: ViewModelFactory,
    private val buttonsFactory: GuardMenuButtonFactory,
    private val qrScanner: QrScanner,
    private val selector: QrActionViewTypeSelector,
    private val builder: QrActionViewHolderBuilder
) : AppFragment() {
    private lateinit var binding: FragmentActionsBinding

    private val viewModel: GuardMainViewModel by viewModels { factory }

    private val gridButtonAdapter: GridMenuAdapter by lazy {
        GridMenuAdapter(buttonsFactory, menuClickListener)
    }

    private val menuClickListener =  AppAdapter.ItemClickListener<MenuButton> {
        when(it.type) {
            ButtonType.SCAN_QR -> scanQR()
            ButtonType.DETOUR -> startDetour()
            ButtonType.CREATE_INCIDENT -> createIncidents()
            else -> return@ItemClickListener
        }
    }

    private val scanResultListener = QrScanListener {
        lifecycleScope.launch {
            viewModel.getShowQrInfoDelegateByQr(it)?.invoke(parentFragmentManager)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentActionsBinding.inflate(inflater, container, false)

        binding.buttons.apply {
            adapter = gridButtonAdapter
            layoutManager = GridLayoutManager(requireContext(), 2)
        }

        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val scanResult = ScanResult(requestCode, resultCode, data)
        qrScanner.onResult(scanResult, scanResultListener)
    }
    
    private fun scanQR() {
        lifecycleScope.launch {
            if (viewModel.canScanQr()) withContext(Main) {
                qrScanner.scanQr(this@GuardMainFragment)
            } else withContext(Main) {
                val errorMessage = getString(R.string.need_start_schedule)
                showMessage(errorMessage)
            }
        }
    }
    
    private fun startDetour() {
        val roundsFragment = RoundsFragment(factory, QrActionsAdapter(selector, builder))
        roundsFragment.show(parentFragmentManager, roundsFragment.tag)
    }

    private fun createIncidents() {
        val fragment = CreateReportFragment(factory, QrActionsAdapter(selector, builder))
        fragment.show(parentFragmentManager, fragment.tag)
    }
}