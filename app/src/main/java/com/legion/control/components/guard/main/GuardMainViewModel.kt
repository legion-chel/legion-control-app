package com.legion.control.components.guard.main

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModel
import com.legion.control.components.guard.qr.PassScreenBuilder
import com.legion.control.models.qr.info.cases.GetCanScanQrUseCase
import com.legion.control.models.qr.info.cases.ParseQrStringUseCase
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GuardMainViewModel @Inject constructor(
    private val qrScreenViewer: PassScreenBuilder,
    private val parseQrStringUseCase: ParseQrStringUseCase,
    private val getCanScanQrUseCase: GetCanScanQrUseCase
) : ViewModel() {
    
    suspend fun canScanQr() = getCanScanQrUseCase()
    
    suspend fun getShowQrInfoDelegateByQr(qrString: String): ((FragmentManager) -> Unit)? {
        val qrContent = parseQrStringUseCase.parse(qrString) ?: return null
        return withContext(Main) {
            val fragment = qrScreenViewer.build(qrContent) ?: return@withContext null
            {
                fragment.show(it, fragment.tag)
            }
        }
    }
}