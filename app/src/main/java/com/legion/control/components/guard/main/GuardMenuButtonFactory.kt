package com.legion.control.components.guard.main

import com.legion.control.R
import com.legion.control.models.ui.adapter.AppAdapter
import com.legion.control.models.ui.menu.ButtonType
import com.legion.control.models.ui.menu.MenuButton
import com.legion.utils.ResourceProvider
import javax.inject.Inject

class GuardMenuButtonFactory @Inject constructor(
    private val resourceProvider: ResourceProvider
) : AppAdapter.FactoryItems<MenuButton> {
    override fun create(): List<MenuButton> = listOf(
        createButton(ButtonType.DETOUR, R.string.main_menu_detour),
        createButton(ButtonType.SCAN_QR, R.string.main_menu_qr),
        createButton(ButtonType.CREATE_INCIDENT, R.string.main_menu_create_incident)
    )

    private fun createButton(type: ButtonType, stringId: Int): MenuButton {
        val title = resourceProvider.getString(stringId)
        return MenuButton(type, title)
    }
}