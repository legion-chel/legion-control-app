package com.legion.control.components.client.created.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.legion.control.components.client.created.adapter.holders.EmployeeCreatedPassViewHolder
import com.legion.control.components.client.created.adapter.holders.GoodsCreatedPassViewHolder
import com.legion.control.components.client.created.adapter.holders.VisitorCreatedPassViewHolder
import com.legion.control.databinding.ItemCreatedPassBinding
import com.legion.control.models.client.data.CreatedPass

class CreatedPassesAdapter : RecyclerView.Adapter<CreatedPassViewHolder>() {
    var clickListener: CreatedPassClickListener? = null

    private val passes = mutableListOf<CreatedPass>()

    override fun getItemViewType(position: Int): Int {
        return when(passes[position]) {
            is CreatedPass.Goods -> 0
            is CreatedPass.Employee -> 1
            is CreatedPass.Visitor -> 2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CreatedPassViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCreatedPassBinding.inflate(inflater, parent, false)

        return when(viewType) {
            0 -> GoodsCreatedPassViewHolder(binding)
            1 -> EmployeeCreatedPassViewHolder(binding)
            else -> VisitorCreatedPassViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: CreatedPassViewHolder, position: Int) {
        val item = passes[position]
        holder.bind(item, clickListener)
    }

    override fun getItemCount() = passes.count()

    fun updatePasses(passes: List<CreatedPass>) {
        this.passes.apply {
            clear()
            addAll(passes)
        }

        notifyDataSetChanged()
    }
}