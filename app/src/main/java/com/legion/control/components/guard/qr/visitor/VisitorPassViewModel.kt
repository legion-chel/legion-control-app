package com.legion.control.components.guard.qr.visitor

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.legion.control.models.qr.actions.cases.SendQrActionsResultUseCase
import com.legion.control.models.qr.actions.cases.SetCompleteActionUseCase
import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.info.cases.visitor.GetVisitorQrInfoUIWrapperUseCase
import com.legion.control.models.qr.info.data.QrContent
import com.legion.control.models.qr.info.data.ActionType
import com.legion.control.models.qr.info.data.visitor.VisitorQrInfoUIWrapper
import com.legion.control.models.schedules.repository.ScheduleRepository
import com.legion.control.models.utils.TransformLoadingStateToUIStateUseCase
import com.legion.core.state.UIState
import com.legion.utils.extensions.asImmutable
import com.legion.utils.extensions.observeDelegate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.launch
import javax.inject.Inject

class VisitorPassViewModel @Inject constructor(
    private val scheduleRepository: ScheduleRepository,
    private val getVisitorQrInfoUseCase: GetVisitorQrInfoUIWrapperUseCase,
    private val sendQrActionsResultUseCase: SendQrActionsResultUseCase,
    private val transformLoadingStateToUIStateUseCase: TransformLoadingStateToUIStateUseCase,
    private val setCompleteActionUseCase: SetCompleteActionUseCase
) : ViewModel() {

    private val photoActions = mutableMapOf<Action, Uri>()
    private val textActions = mutableMapOf<Action, String>()

    private val _qrInfoUIState = MutableLiveData<UIState<VisitorQrInfoUIWrapper>>(UIState.Loading)
    val qrInfoUIState
        get() = _qrInfoUIState.asImmutable()

    val sendQrActionsUIState = sendQrActionsResultUseCase
        .sendLoadingState
        .transform {
            val state = transformLoadingStateToUIStateUseCase(it) ?: return@transform
            emit(state)
        }
        .asLiveData(Dispatchers.Default)

    fun loadQrInfo(qrContent: QrContent) {
        viewModelScope.launch {
            val scheduleId = scheduleRepository.syncGetCurrentSchedule()?.id
            getVisitorQrInfoUseCase(qrContent, scheduleId)
                .transform {
                    val state = transformLoadingStateToUIStateUseCase(it) ?: return@transform
                    emit(state)
                }
                .collect(_qrInfoUIState::observeDelegate)
        }
    }

    fun addActionPhoto(action: Action, imageUri: Uri) {
        photoActions[action] = imageUri
        viewModelScope.launch(Dispatchers.IO) {
            setCompleteActionUseCase.setComplete(action)
        }
    }

    fun addActionText(action: Action, text: String) {
        textActions[action] = text
        viewModelScope.launch(Dispatchers.IO) {
            setCompleteActionUseCase.setComplete(action)
        }
    }

    fun sendActionsResult(qrId: Int, actionType: ActionType) {
        viewModelScope.launch(Dispatchers.IO) {
            sendQrActionsResultUseCase.send(qrId, actionType, photoActions, textActions)
        }
    }
}