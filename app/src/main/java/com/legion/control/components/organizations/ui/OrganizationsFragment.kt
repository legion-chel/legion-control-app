package com.legion.control.components.organizations.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.legion.control.databinding.FragmentOrganizationsBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.control.models.ui.adapter.MapAdapter
import com.legion.control.models.ui.adapter.MapItemDecoration
import javax.inject.Inject

class OrganizationsFragment @Inject constructor(
    private val factory: ViewModelFactory,
    private val mapAdapter: MapAdapter,
    private val mapItemDecoration: MapItemDecoration
) : Fragment() {
    private lateinit var binding: FragmentOrganizationsBinding

    private val viewModel: OrganizationsViewModel by viewModels { factory }

    private val organizationsObserver = Observer<List<Pair<String, String>>> {
        mapAdapter.update(it ?: return@Observer)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentOrganizationsBinding.inflate(inflater, container, false)

        with(binding.organizationsList) {
            adapter = mapAdapter
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(mapItemDecoration)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.load()

        viewModel.organizations.observe(viewLifecycleOwner, organizationsObserver)
    }
}