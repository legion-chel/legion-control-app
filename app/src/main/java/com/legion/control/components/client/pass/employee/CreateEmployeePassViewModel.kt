package com.legion.control.components.client.pass.employee

import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.*
import com.legion.control.models.client.scenario.CreateEmployeePassScenario
import com.legion.core.state.UIState
import com.legion.utils.InputDelegate
import com.legion.utils.extensions.empty
import com.legion.utils.extensions.observeDelegate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class CreateEmployeePassViewModel @Inject constructor(
    private val createEmployeePassScenario: CreateEmployeePassScenario
) : ViewModel() {

    private var secondName = String.empty
    private var firstName = String.empty
    private var patronymic = String.empty
    private var photos = mutableListOf<Uri>()

    val inputSecondNameDelegate: InputDelegate = {
        secondName = it?.toString() ?: String.empty
    }

    val inputFirstNameDelegate: InputDelegate = {
        firstName = it?.toString() ?: String.empty
    }

    val inputPatronymicDelegate: InputDelegate = {
        patronymic = it?.toString() ?: String.empty
    }

    val appendedPhotos = createEmployeePassScenario
        .appendedPhotos
        .asLiveData(Dispatchers.Default)

    private val _uiState = MutableLiveData<UIState<String>>()
    val uiState: LiveData<UIState<String>>
        get() = _uiState

    fun create() {
        viewModelScope.launch {
            createEmployeePassScenario
                .create(secondName, firstName, patronymic)
                .collect(_uiState::observeDelegate)
        }
    }

    fun appendPhoto(uri: Uri, bitmap: Bitmap?) {
        viewModelScope.launch(Dispatchers.Default) {
            photos.add(uri)
            createEmployeePassScenario.appendPhoto(uri, bitmap)
        }
    }
}