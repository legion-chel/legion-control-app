package com.legion.control.components.guard.schedules.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.legion.control.models.schedules.cases.GetCurrentScheduleUseCase
import com.legion.control.models.schedules.cases.LoadScheduleUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class ShiftViewModel @Inject constructor(
    private val loadScheduleUseCase: LoadScheduleUseCase,
    private val getCurrentScheduleUseCase: GetCurrentScheduleUseCase,
) : ViewModel() {

    val scheduleLoadingState = loadScheduleUseCase
        .loadingState
        .asLiveData(Dispatchers.Default)

    val currentSchedule = getCurrentScheduleUseCase
        .currentSchedule
        .asLiveData(Dispatchers.Default)
}