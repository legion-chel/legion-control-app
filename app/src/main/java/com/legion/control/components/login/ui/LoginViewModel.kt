package com.legion.control.components.login.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.legion.control.models.auth.data.AuthState
import com.legion.control.models.auth.model.AuthModel
import com.legion.control.models.validatior.LoginValidator
import com.legion.control.models.validatior.PasswordValidator
import com.legion.utils.InputState
import com.legion.utils.extensions.asImmutable
import com.legion.utils.extensions.empty
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val authModel: AuthModel,
    private val loginValidator: LoginValidator,
    private val passwordValidator: PasswordValidator
) : ViewModel() {

    private var login: String? = String.empty
    private var password: String? = String.empty

    val loginState = loginValidator
        .loginState
        .asLiveData(Main)

    val passwordState = passwordValidator
        .passwordState
        .asLiveData(Main)

    private val _fieldInputCorrect = MutableStateFlow(false)
    val fieldInputCorrect
        get() = _fieldInputCorrect.asLiveData()

    private val _authState = MutableLiveData<AuthState>()
    val authState
        get() = _authState.asImmutable()

    init {
        checkInputCorrect()
    }

    fun authorization() {
        val loginValue = login ?: return
        val passwordValue = password ?: return

        viewModelScope.launch(IO) {
            val state = authModel.auth(loginValue, passwordValue)
            _authState.postValue(state)
        }
    }

    suspend fun validateLogin(login: String?) {
        this.login = login.also {
            loginValidator.validate(it)
        }
    }

    suspend fun validatePassword(password: String?) {
        this.password = password.also {
            passwordValidator.validate(it)
        }
    }

    private fun checkInputCorrect() {
        viewModelScope.launch(IO) {
            loginValidator
                .loginState
                .combine(passwordValidator.passwordState, this@LoginViewModel::inputStateTransform)
                .collect(this@LoginViewModel::onInputState)
        }
    }

    private fun inputStateTransform(
        inputLoginState: InputState?,
        inputPasswordState: InputState?
    ): InputState {
        return if (inputLoginState is InputState.Correct && inputPasswordState is InputState.Correct)
            InputState.Correct
        else InputState.Incorrect()
    }

    private suspend fun onInputState(inputState: InputState?) {
        _fieldInputCorrect.emit(inputState is InputState.Correct)
    }
}