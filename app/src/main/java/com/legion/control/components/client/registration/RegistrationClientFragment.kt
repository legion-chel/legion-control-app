package com.legion.control.components.client.registration

import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.legion.camera.TakeImageBottomSheet
import com.legion.camera.core.CameraType
import com.legion.camera.core.TakeImageCameraResultListener
import com.legion.control.R
import com.legion.control.components.utils.ImageGallery
import com.legion.control.components.utils.ImageGalleryItemDecorator
import com.legion.control.databinding.FragmentClientRegistationBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.control.models.ui.AppFragment
import com.legion.core.state.UIState
import com.legion.utils.InputState
import com.legion.utils.extensions.*
import javax.inject.Inject

class RegistrationClientFragment @Inject constructor(
    private val viewModelFactory: ViewModelFactory
) : AppFragment() {
    
    private var _binding: FragmentClientRegistationBinding? = null
    private val binding get() = _binding!!
    
    private val viewModel: RegistrationClientViewModel by viewModels { viewModelFactory }
    
    private val imageGallery = ImageGallery()
    
    private val firstNameInputStateObserver = Observer<InputState> {
        binding.inputFirstNameLayout.setInputState(it)
    }
    
    private val secondNameInputStateObserver = Observer<InputState> {
        binding.inputSecondNameLayout.setInputState(it)
    }
    
    private val emailInputStateObserver = Observer<InputState> {
        binding.inputEmailLayout.setInputState(it)
    }
    
    private val documentSerialInputStateObserver = Observer<InputState> {
        binding.inputDocumentSerialLayout.setInputState(it)
    }
    
    private val attachedPhotosObserver = Observer<List<Bitmap>> {
        imageGallery.update(it ?: return@Observer)
    }
    
    private val buttonEnabledObserver = Observer<Boolean> {
        binding.buttonSaveInfo.isEnabled = it
    }
    
    private val registrationUIStateObserver = Observer<UIState<String>> {
        it ?: return@Observer
        
        it.handleState(
            loadingDelegate = this::onLoadingSaveInfo,
            successDelegate = this::onSuccessSaveInfo,
            errorDelegate = this::onErrorSaveInfo
        )
    }
    
    private val buttonInputBirthDateClickListener = View.OnClickListener {
        val picker = createCalendarPicker()
        
        picker.addOnPositiveButtonClickListener {
            binding.inputBirthDate.text = viewModel.getBirthDateString(it)
        }
        
        picker.show(parentFragmentManager, picker.toString())
    }
    
    private val takeImageAvatarResultListener = object : TakeImageCameraResultListener {
        override fun onSuccess(imageUri: Uri, bitmap: Bitmap?) {
            viewModel.addAvatar(imageUri, bitmap)
        }
    
        override fun onError(throwable: Throwable?) {
            val errorMessage = getString(R.string.error_getting_photo)
            showToast(errorMessage)
        }
    
    }
    
    private val buttonAddAvatarClickHandler = View.OnClickListener {
        val takePhotoFragment = TakeImageBottomSheet(takeImageAvatarResultListener, CameraType.FRONT)
        takePhotoFragment.takeImage(parentFragmentManager)
    }
    
    private val takeImageDocumentResultListener = object : TakeImageCameraResultListener {
        override fun onSuccess(imageUri: Uri, bitmap: Bitmap?) {
            viewModel.addDocumentPhoto(imageUri, bitmap)
        }
    
        override fun onError(throwable: Throwable?) {
            val errorMessage = getString(R.string.error_getting_photo)
            showToast(errorMessage)
        }
    
    }
    
    private val buttonAddDocumentPhotoClickHandler = View.OnClickListener {
        val takePhotoFragment = TakeImageBottomSheet(takeImageDocumentResultListener, CameraType.MAIN)
        takePhotoFragment.takeImage(parentFragmentManager)
    }
    
    private val buttonRegistrationClientClickHandler = View.OnClickListener {
        viewModel.registerClient()
    }
    
    private val inputActionListener = TextView.OnEditorActionListener { v, actionId, _ ->
        handleInputAction(v.id, actionId)
        true
    }
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentClientRegistationBinding.inflate(inflater, container, false)
        return binding.root
    }
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        
        val imageItemsHorizontalMargin = resources.getDimension(R.dimen.app_margin).toInt()
        binding.photosList.apply {
            adapter = imageGallery
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.HORIZONTAL,
                false
            )
            addItemDecoration(ImageGalleryItemDecorator(horizontalMargin = imageItemsHorizontalMargin))
        }
        
        binding.inputFirstName.apply {
            doAfterTextChanged(viewModel.firstNameInputDelegate)
            setOnEditorActionListener(inputActionListener)
        }
        
        binding.inputSecondName.apply {
            doAfterTextChanged(viewModel.secondNameInputDelegate)
            setOnEditorActionListener(inputActionListener)
        }
        
        binding.inputEmail.apply {
            doAfterTextChanged(viewModel.emailInputDelegate)
            setOnEditorActionListener(inputActionListener)
        }
        
        binding.inputDocumentSerial.apply {
            doAfterTextChanged(viewModel.documentSerialInputDelegate)
            setOnEditorActionListener(inputActionListener)
        }
    
        binding.apply {
            buttonAddDocumentAvatar.setOnClickListener(buttonAddAvatarClickHandler)
            buttonAddDocumentPhoto.setOnClickListener(buttonAddDocumentPhotoClickHandler)
            buttonSaveInfo.setOnClickListener(buttonRegistrationClientClickHandler)
        }
        
        binding.inputBirthDate.setOnClickListener(buttonInputBirthDateClickListener)
        
        with(viewModel) {
            firstNameInputState.observe(viewLifecycleOwner, firstNameInputStateObserver)
            secondNameInputState.observe(viewLifecycleOwner, secondNameInputStateObserver)
            
            emailInputState.observe(viewLifecycleOwner, emailInputStateObserver)
            
            documentSerialInputState.observe(viewLifecycleOwner, documentSerialInputStateObserver)
            
            attachedPhotos.observe(viewLifecycleOwner, attachedPhotosObserver)
            
            buttonEnabled.observe(viewLifecycleOwner, buttonEnabledObserver)
            
            registrationUIState.observe(viewLifecycleOwner, registrationUIStateObserver)
        }
    }
    
    override fun onDestroy() {
        super.onDestroy()
        
        viewModel.logoutIfNeed()
        _binding = null
    }
    
    private fun setLoadingState(isLoading: Boolean) {
        if (isLoading) progressIndicator.showProgress()
        else progressIndicator.hideProgress()
        
        with(binding) {
            inputFirstName.isEnabled = !isLoading
            inputSecondName.isEnabled = !isLoading
            inputBirthDate.isEnabled = !isLoading
    
            inputDocumentSerial.isEnabled = !isLoading
    
            buttonAddDocumentAvatar.isEnabled = !isLoading
            buttonAddDocumentPhoto.isEnabled = !isLoading
            buttonSaveInfo.isEnabled = !isLoading
        }
    }
    
    private fun onLoadingSaveInfo() = setLoadingState(true)
    
    private fun onSuccessSaveInfo(message: String) {
        setLoadingState(false)
        
        showMessageWithAction(message, "Ok") {
            val directions = RegistrationClientFragmentDirections.toHome()
            navigate(directions)
        }
    }
    
    private fun onErrorSaveInfo(message: String) {
        setLoadingState(false)
        showToast(message)
    }
    
    private fun handleInputAction(viewId: Int, actionId: Int) {
        val editText = when(viewId) {
            R.id.inputSecondName -> binding.inputFirstName
            R.id.inputFirstName -> binding.inputEmail
            R.id.inputEmail -> {
                hideKeyboard()
                return
            }
            R.id.inputBirthDate -> binding.inputDocumentSerial
            R.id.inputDocumentSerial -> {
                hideKeyboard()
                binding.buttonAddDocumentAvatar
            }
            else -> return
        }
        
        if (actionId == EditorInfo.IME_ACTION_DONE) editText.requestFocus()
    }
}