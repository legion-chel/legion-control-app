package com.legion.control.components.guard.schedules.start

import com.legion.control.components.guard.schedules.core.ActionShiftViewModel
import com.legion.control.models.schedules.cases.StartScheduleUseCase
import com.legion.control.models.schedules.scenario.StartScheduleUIScenario
import com.legion.control.models.upload.scenario.UploadImageScenario
import javax.inject.Inject

class StartScheduleViewModel @Inject constructor(
    uploadImageScenario: UploadImageScenario,
	startScheduleUseCase: StartScheduleUseCase,
    startScheduleUIScenario: StartScheduleUIScenario
) : ActionShiftViewModel(
    uploadImageScenario = uploadImageScenario,
    actScheduleUseCase = startScheduleUseCase,
    actUIScenario = startScheduleUIScenario
)