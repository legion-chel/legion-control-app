package com.legion.control.components.guard.qr.visitor

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.legion.camera.TakeImageBottomSheet
import com.legion.camera.core.TakeImageCamera
import com.legion.camera.core.TakeImageCameraResultListener
import com.legion.control.components.guard.qr.adapter.QrActionsAdapter
import com.legion.control.components.guard.qr.adapter.listeners.OnActionPhotoClickListener
import com.legion.control.components.guard.qr.adapter.listeners.OnActionTextClickListener
import com.legion.control.databinding.FragmentVisitorPassBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.control.models.qr.info.data.QrContent
import com.legion.control.models.qr.info.data.visitor.VisitorQrInfoUIWrapper
import com.legion.core.state.UIState
import com.legion.utils.extensions.setVisibility
import com.legion.utils.extensions.showToast

class VisitorPassScreen(
    private val factory: ViewModelFactory,
    private val actionsAdapter: QrActionsAdapter,
    private val qrContent: QrContent
) : BottomSheetDialogFragment(), TakeImageCamera {

    private var _binding: FragmentVisitorPassBinding? = null
    val binding
        get() = _binding!!

    private val viewModel: VisitorPassViewModel by viewModels { factory }

    private val qrInfoUIStateObserver = Observer<UIState<VisitorQrInfoUIWrapper>?> {
        it?.handleState(
            loadingDelegate = this::onLoadingQRInfo,
            successDelegate = this::onSuccessLoadingQrInfo,
            errorDelegate = this::onErrorLoadingQrInfo
        )
    }

    private val sendQrResultUIStateObserver = Observer<UIState<String>> {
        it.handleState(
            loadingDelegate = this::onLoadingSendQrResult,
            successDelegate = this::onSuccessSendQrResult,
            errorDelegate = this::onErrorSendQrResult
        )
    }

    private val closeClickHandler = View.OnClickListener {
        dismiss()
    }

    private val buttonSendResultClickHandler = View.OnClickListener {
        val qrId = actionsAdapter.qrId
        val qrType = actionsAdapter.actionType ?: return@OnClickListener

        viewModel.sendActionsResult(qrId, qrType)
    }

    private val photoActionClickListener = OnActionPhotoClickListener { action, imageUri, _ ->
        viewModel.addActionPhoto(action, imageUri)
    }

    private val textActionClickListener = OnActionTextClickListener { action, text ->
        viewModel.addActionText(action, text)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentVisitorPassBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        actionsAdapter.apply {
            takeImageCamera = this@VisitorPassScreen
            photoActionClickListener = this@VisitorPassScreen.photoActionClickListener
            textActionClickListener = this@VisitorPassScreen.textActionClickListener
        }

        binding.actionsList.apply {
            adapter = actionsAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        binding.apply {
            closeButton.setOnClickListener(closeClickHandler)
            sendButton.setOnClickListener(buttonSendResultClickHandler)
        }

        viewModel.apply {
            qrInfoUIState.observe(viewLifecycleOwner, qrInfoUIStateObserver)
            sendQrActionsUIState.observe(viewLifecycleOwner, sendQrResultUIStateObserver)
        }

        viewModel.loadQrInfo(qrContent)
    }
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun takeImage(listener: TakeImageCameraResultListener) {
        val takeImageBottomSheet = TakeImageBottomSheet(listener)
        takeImageBottomSheet.show(parentFragmentManager, takeImageBottomSheet.tag)
    }

    private fun setLoadingStateQrInfo(isLoading: Boolean, isError: Boolean = false) {
        binding.apply {
            loadingProgress.setVisibility(isLoading)
            content.setVisibility(!isLoading && !isError)
            loadingErrorMessage.setVisibility(isError)
        }
    }

    private fun onLoadingQRInfo() = setLoadingStateQrInfo(true)

    private fun onSuccessLoadingQrInfo(qrInfoUIWrapper: VisitorQrInfoUIWrapper) {
        setLoadingStateQrInfo(false)
        binding.fullName.text = qrInfoUIWrapper.fullName
        binding.accessPass.text = qrInfoUIWrapper.access

        actionsAdapter.updateItems(qrInfoUIWrapper.qrId, qrInfoUIWrapper.actionType, qrInfoUIWrapper.actions)
    }

    private fun onErrorLoadingQrInfo(error: String) {
        setLoadingStateQrInfo(isLoading = false, isError = true)
        binding.loadingErrorMessage.text = error
    }

    private fun setLoadingSendQrResult(isLoading: Boolean) {
        binding.apply {
            sendProgress.setVisibility(isLoading)
            sendButton.isEnabled = !isLoading
        }
    }

    private fun onLoadingSendQrResult() {
        setLoadingSendQrResult(true)
    }

    private fun onSuccessSendQrResult(message: String) {
        setLoadingSendQrResult(false)
        showToast(message)
        dismiss()
    }

    private fun onErrorSendQrResult(errorMessage: String) {
        setLoadingSendQrResult(false)
        showToast(errorMessage)
    }
}