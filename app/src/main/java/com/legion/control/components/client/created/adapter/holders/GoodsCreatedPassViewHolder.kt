package com.legion.control.components.client.created.adapter.holders

import com.legion.control.components.client.created.adapter.CreatedPassClickListener
import com.legion.control.components.client.created.adapter.CreatedPassViewHolder
import com.legion.control.databinding.ItemCreatedPassBinding
import com.legion.control.models.client.data.CreatedPass

class GoodsCreatedPassViewHolder(
    binding: ItemCreatedPassBinding
) : CreatedPassViewHolder(binding) {

    override fun bind(pass: CreatedPass, clickListener: CreatedPassClickListener?) {
        super.bind(pass, clickListener)

        val goodsPass = pass as? CreatedPass.Goods ?: return
        binding.name.text = goodsPass.description
    }
}