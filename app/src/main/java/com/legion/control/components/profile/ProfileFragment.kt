package com.legion.control.components.profile

import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import coil.imageLoader
import coil.request.ImageRequest
import com.legion.camera.TakeImageBottomSheet
import com.legion.camera.core.CameraType
import com.legion.camera.core.TakeImageCameraResultListener
import com.legion.control.databinding.FragmentProfileBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.control.models.ui.AppFragment
import com.legion.utils.extensions.showMessage
import javax.inject.Inject

class ProfileFragment @Inject constructor(
    factory: ViewModelFactory
) : AppFragment() {

    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    private val viewModel: ProfileViewModel by viewModels { factory }
    
    private val avatarUrlObserver = Observer<String> {
        progressIndicator.hideProgress()
        setAvatar(it ?: return@Observer)
    }

    private val takeIMageCameraListener = object : TakeImageCameraResultListener {
        override fun onSuccess(imageUri: Uri, bitmap: Bitmap?) {
            progressIndicator.showProgress()
            viewModel.uploadAvatar(imageUri)
        }
    
        override fun onError(throwable: Throwable?) {
            showMessage("Ошибка получения фотографии")
        }
    
    }
    
    private var avatarClickHandler: (View) -> Unit = {
        val getImageBottomSheet = TakeImageBottomSheet(takeIMageCameraListener, CameraType.FRONT)
        getImageBottomSheet.show(parentFragmentManager, getImageBottomSheet.tag)
    }
    
    private val logoutClickHandler = View.OnClickListener {
        viewModel.logout()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            fullName.text = viewModel.getFullName()
            buttonLogout.setOnClickListener(logoutClickHandler)
            avatar.setOnClickListener(avatarClickHandler)
        }
        
        viewModel.avatarUri.observe(viewLifecycleOwner, avatarUrlObserver)
    }
    
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setAvatar(avatarUrl: String) {
        val request = ImageRequest.Builder(requireContext())
            .addHeader("Authorization", viewModel.getAuthToken())
            .data(avatarUrl)
            .target { drawable ->
                binding.avatar.setImageDrawable(drawable)
            }
            .build()
        requireContext().imageLoader.enqueue(request)
    }
}