package com.legion.control.components.guard.qr.adapter.utils

enum class ActionViewType(val viewType: Int) {
    PHOTO(0),
    TEXT(1)
}