package com.legion.control.components.client.main

import androidx.lifecycle.ViewModel
import com.legion.control.models.client.cases.GetDirectionByButtonTypeUseCase
import com.legion.control.models.ui.menu.ButtonType
import javax.inject.Inject

class ClientMainViewModel @Inject constructor(
    private val getDirectionByButtonTypeUseCase: GetDirectionByButtonTypeUseCase
) : ViewModel() {

    fun getDirection(buttonType: ButtonType) = getDirectionByButtonTypeUseCase.getDirection(buttonType)
}