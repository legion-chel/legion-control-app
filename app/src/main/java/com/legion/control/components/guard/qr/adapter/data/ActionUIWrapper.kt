package com.legion.control.components.guard.qr.adapter.data

import com.legion.control.models.qr.actions.data.Action

class ActionUIWrapper(
    val action: Action,
    var actionText: String,
    val buttonEnabled: Boolean,
)