package com.legion.control.components.guard.qr.goods.data

sealed class GoodsQrInfoUIState {
    object Loading : GoodsQrInfoUIState()
    class Success(val wrapper: GoodsQrInfoUIWrapper) : GoodsQrInfoUIState()
    class Error(val message: String) : GoodsQrInfoUIState()
}