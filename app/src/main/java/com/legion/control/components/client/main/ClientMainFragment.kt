package com.legion.control.components.client.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.legion.control.components.client.pass.goods.CreateGoodsPassFragment
import com.legion.control.databinding.FragmentMainClientBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory
import javax.inject.Inject

class ClientMainFragment @Inject constructor(
    private val factory: ViewModelFactory,
) : Fragment() {

    private var _binding: FragmentMainClientBinding? = null
    val binding
        get() = _binding!!

    private val buttonCreateClickHandler: (View) -> Unit = {
        val fragment = CreateGoodsPassFragment(factory)
        fragment.show(parentFragmentManager, fragment.tag)
    }
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainClientBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonCreate.setOnClickListener(buttonCreateClickHandler)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}