package com.legion.control.components.login.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.legion.control.databinding.FragmentLoginBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.control.models.auth.data.AuthState
import com.legion.control.models.auth.data.AuthState.*
import com.legion.control.models.ui.AppFragment
import com.legion.utils.InputState
import com.legion.utils.extensions.empty
import com.legion.utils.extensions.hideKeyboard
import com.legion.utils.extensions.navigate
import com.legion.utils.extensions.showMessage
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginFragment @Inject constructor(
    private val factory: ViewModelFactory
) : AppFragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val viewModel: LoginViewModel by viewModels { factory }

    private val loginContainer: TextInputLayout by lazy {
        binding.loginContainer
    }

    private val login: TextInputEditText by lazy {
        binding.login
    }

    private val passwordContainer: TextInputLayout by lazy {
        binding.passwordContainer
    }

    private val password: TextInputEditText by lazy {
        binding.password
    }

    private val logInButton: MaterialButton by lazy {
        binding.logInButton.apply {
            setOnClickListener(loginButtonClickHandler)
        }
    }

    private val loginButtonClickHandler = View.OnClickListener {
        runAuth()
    }

    private val loginTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable?) {
            validateInput(s?.toString(), viewModel::validateLogin)
        }
    }

    private val loginActionListener = TextView.OnEditorActionListener { _, _, _ ->
        passwordContainer.requestFocus()
    }

    private val passwordTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable?) {
            validateInput(s?.toString(), viewModel::validatePassword)
        }
    }

    private val passwordActionListener = TextView.OnEditorActionListener { _, _, _ ->
        runAuth()
        true
    }

    private val fieldInputCorrectObserver = Observer<Boolean> {
        logInButton.isEnabled = it ?: return@Observer
    }

    private val loginStateObserver = Observer<InputState?> {
        if (it is InputState.Incorrect) loginContainer.error = it.error ?: return@Observer
        else loginContainer.error = String.empty
    }

    private val passwordStateObserver = Observer<InputState?> {
        if (it is InputState.Incorrect) passwordContainer.error = it.error ?: return@Observer
        else passwordContainer.error = String.empty
    }

    private val authStateObserver = Observer<AuthState> {
        it ?: return@Observer
        onAuthState(it)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewActions()
        observeData()
    }

    private fun bindViewActions() {
        with(login) {
            addTextChangedListener(loginTextWatcher)
            setOnEditorActionListener(loginActionListener)
        }

        with(password) {
            addTextChangedListener(passwordTextWatcher)
            setOnEditorActionListener(passwordActionListener)
        }
    }

    private fun setLoadingState(isLoading: Boolean) {
        val action = if (isLoading) progressIndicator::showProgress
        else progressIndicator::hideProgress

        action.invoke()
    }

    private fun validateInput(input: String?, delegate: suspend (String?) -> Unit) {
        lifecycleScope.launch(IO) {
            delegate.invoke(input)
        }
    }

    private fun runAuth() {
        hideKeyboard()
        setLoadingState(true)
        viewModel.authorization()
    }

    private fun observeData() {
        with(viewModel) {
            loginState.observe(viewLifecycleOwner, loginStateObserver)
            passwordState.observe(viewLifecycleOwner, passwordStateObserver)
            fieldInputCorrect.observe(viewLifecycleOwner, fieldInputCorrectObserver)
            authState.observe(viewLifecycleOwner, authStateObserver)
        }
    }

    private fun onAuthState(authState: AuthState) {
        setLoadingState(false)
        when(authState) {
            is Success -> onSuccessAuth()
            is Failed -> onErrorAuth(authState.message)
        }
    }

    private fun onSuccessAuth() {
        val action = LoginFragmentDirections.toSplash()
        navigate(action)
    }

    private fun onErrorAuth(message: String) {
        showMessage(message)
    }
}