package com.legion.control.components.user.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.legion.control.models.user.UserInfoModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

class UserInfoViewModel @Inject constructor(
    private val userInfoModel: UserInfoModel
) : ViewModel() {

    val userInfo = userInfoModel
        .userInfo
        .asLiveData(IO)

    fun load() {
        viewModelScope.launch(IO) {
            userInfoModel.loadInfo()
        }
    }
}