package com.legion.control.components.home.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.forEachIndexed
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.legion.control.databinding.FragmentHomeBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.control.models.auth.utils.LogoutBroadcast
import com.legion.control.models.ui.AppFragment
import com.legion.utils.extensions.navigate
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeFragment @Inject constructor(
    private val factory: ViewModelFactory,
    private val logoutBroadcast: LogoutBroadcast
) : AppFragment() {
    private lateinit var binding: FragmentHomeBinding

    private val viewModel: HomeViewModel by viewModels { factory }

    private val pages: ViewPager2 by lazy {
        binding.pages
    }

    private val navMenu: BottomNavigationView by lazy {
        binding.navView
    }

    private val pageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            changeCurrentItem(position)
        }
    }

    private val navigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
        navMenu.menu.forEachIndexed { index, item ->
            if (it.itemId == item.itemId) changeCurrentItem(index)
        }

        true
    }

    private val logoutObserver = LogoutBroadcast.Observer {
        lifecycleScope.launch(Main) {
            val action = HomeFragmentDirections.toLoginFromHome()
            navigate(action)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)

        pages.registerOnPageChangeCallback(pageChangeCallback)

        navMenu.setOnNavigationItemSelectedListener(navigationItemSelectedListener)

        return binding.root
    }
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setPages()
        setNavMenu()
    }

    override fun onResume() {
        super.onResume()
        logoutBroadcast.observe(logoutObserver)
    }

    override fun onDestroy() {
        super.onDestroy()
        logoutBroadcast.removeObserving(logoutObserver)
    }

    private fun changeCurrentItem(position: Int) {
        pages.setCurrentItem(position, true)
        val itemId = navMenu.menu.getItem(position)
        itemId.isChecked = true
    }

    private fun setPages() {
        val pagesList = viewModel.getPages()
        pages.adapter = HomePageAdapter(requireActivity(), pagesList)
    }

    private fun setNavMenu() {
        val menuId = viewModel.getMenuId() ?: return
        with(navMenu) {
            menu.clear()
            inflateMenu(menuId)
        }
    }
}