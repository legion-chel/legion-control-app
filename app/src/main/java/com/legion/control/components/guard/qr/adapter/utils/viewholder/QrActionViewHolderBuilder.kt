package com.legion.control.components.guard.qr.adapter.utils.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import com.legion.control.components.guard.qr.adapter.holders.QrActionHolder
import com.legion.control.components.guard.qr.adapter.utils.ActionViewType

interface QrActionViewHolderBuilder {
    fun build(viewType: ActionViewType, inflater: LayoutInflater, viewGroup: ViewGroup?): QrActionHolder
}