package com.legion.control.components.guard.qr.goods

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.legion.control.components.guard.qr.goods.data.GoodsQrInfoUIWrapper
import com.legion.control.models.qr.actions.cases.SendQrActionsResultUseCase
import com.legion.control.models.qr.actions.cases.SetCompleteActionUseCase
import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.info.cases.goods.GetGoodsQrInfoUIWrapperUseCase
import com.legion.control.models.qr.info.data.QrContent
import com.legion.control.models.qr.info.data.ActionType
import com.legion.control.models.schedules.repository.ScheduleRepository
import com.legion.control.models.utils.TransformLoadingStateToUIStateUseCase
import com.legion.core.state.UIState
import com.legion.utils.extensions.asImmutable
import com.legion.utils.extensions.observeDelegate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.launch
import javax.inject.Inject

class GoodsQrInfoViewModel @Inject constructor(
    private val repository: ScheduleRepository,
    private val getGoodsQrInfoUIWrapper: GetGoodsQrInfoUIWrapperUseCase,
    private val transformLoadingStateToUIStateUseCase: TransformLoadingStateToUIStateUseCase,
    private val sendQrActionsResultUseCase: SendQrActionsResultUseCase,
    private val setCompleteActionUseCase: SetCompleteActionUseCase
) : ViewModel() {

    private val photoActions = mutableMapOf<Action, Uri>()
    private val textActions = mutableMapOf<Action, String>()

    private val _qrInfoUIState = MutableLiveData<UIState<GoodsQrInfoUIWrapper>>(UIState.Loading)
    val qrInfoUIState
        get() = _qrInfoUIState.asImmutable()

    val sendQrResultUIState = sendQrActionsResultUseCase
        .sendLoadingState
        .transform {
            val state = transformLoadingStateToUIStateUseCase(it) ?: return@transform
            emit(state)
        }
        .asLiveData(Dispatchers.Default)

    fun loadQrInfo(qrContent: QrContent) {
        viewModelScope.launch {
            val scheduleId = repository.syncGetCurrentSchedule()?.id
            getGoodsQrInfoUIWrapper(qrContent, scheduleId)
                .transform {
                    val state = transformLoadingStateToUIStateUseCase(it) ?: return@transform
                    emit(state)
                }
                .collect(_qrInfoUIState::observeDelegate)
        }
    }

    fun addActionPhoto(action: Action, imageUri: Uri) {
        photoActions[action] = imageUri
        viewModelScope.launch(IO) {
            setCompleteActionUseCase.setComplete(action)
        }
    }

    fun addActionText(action: Action, text: String) {
        textActions[action] = text
        viewModelScope.launch(IO) {
            setCompleteActionUseCase.setComplete(action)
        }
    }

    fun sendActionsResult(qrId: Int, actionType: ActionType) {
        viewModelScope.launch(IO) {
            sendQrActionsResultUseCase.send(qrId, actionType, photoActions, textActions)
        }
    }
}