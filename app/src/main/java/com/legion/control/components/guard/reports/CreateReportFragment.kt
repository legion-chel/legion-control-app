package com.legion.control.components.guard.reports

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.legion.camera.core.TakeImageCamera
import com.legion.camera.core.TakeImageCameraResultListener
import com.legion.control.components.guard.qr.adapter.QrActionsAdapter
import com.legion.control.databinding.FragmentCreateReportBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory

class CreateReportFragment(
    private val factory: ViewModelFactory,
    private val actionsAdapter: QrActionsAdapter
) : BottomSheetDialogFragment(), TakeImageCamera {

    private var _binding: FragmentCreateReportBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: CreateReportViewModel by viewModels { factory }

    private val buttonCloseClickHandler: View.OnClickListener by lazy {
        View.OnClickListener {
            dismiss()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCreateReportBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.closeButton.setOnClickListener(buttonCloseClickHandler)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun takeImage(listener: TakeImageCameraResultListener) {
        TODO("Not yet implemented")
    }


}