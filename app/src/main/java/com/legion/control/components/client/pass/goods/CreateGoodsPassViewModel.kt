package com.legion.control.components.client.pass.goods

import android.graphics.Bitmap
import android.net.Uri
import android.text.Editable
import androidx.lifecycle.*
import com.legion.control.models.client.scenario.CreateGoodsPassScenario
import com.legion.control.models.client.scenario.GetGoodsDirectionsScenario
import com.legion.control.models.qr.info.data.goods.GoodsDirection
import com.legion.core.state.UIState
import com.legion.utils.extensions.empty
import com.legion.utils.extensions.observeDelegate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

private typealias InputDelegate = (Editable?) -> Unit

class CreateGoodsPassViewModel @Inject constructor(
    private val createGoodsPassScenario: CreateGoodsPassScenario,
    private val getGoodsDirectionsScenario: GetGoodsDirectionsScenario
) : ViewModel() {

    private var description = String.empty
    private var count = String.empty
    private var direction = GoodsDirection.IN

    private val files = mutableListOf<Uri>()

    val createdPhotos = createGoodsPassScenario
        .appendedPhotos
        .asLiveData(Dispatchers.Default)

    val inputDescriptionDelegate: InputDelegate = {
        description = it?.toString() ?: String.empty
    }

    val inputCountDelegate: InputDelegate = {
        count = it?.toString() ?: String.empty
    }

    private val _uiState = MutableLiveData<UIState<String>>()
    val uiState: LiveData<UIState<String>>
        get() = _uiState

    fun create() {
        viewModelScope.launch {
            createGoodsPassScenario
                .create(description, count, direction)
                .collect(_uiState::observeDelegate)
        }
    }

    fun getGoodsDirections() = getGoodsDirectionsScenario.getGoodsDirections()

    fun onSelectDirection(position: Int) {
        viewModelScope.launch {
            direction = getGoodsDirectionsScenario.getDirectionByNumber(position)
        }
    }

    fun appendImage(uri: Uri, bitmap: Bitmap?) {
        files.add(uri)
        viewModelScope.launch(Dispatchers.Default) {
            createGoodsPassScenario.appendPhoto(uri, bitmap)
        }
    }
}