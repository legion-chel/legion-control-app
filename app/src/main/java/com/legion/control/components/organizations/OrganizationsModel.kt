package com.legion.control.components.organizations

import com.legion.core.session.UserSessionFacade
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class OrganizationsModel @Inject constructor(
    private val userSessionFacade: UserSessionFacade
) {

    private val _organizations = MutableStateFlow<List<Pair<String, String>>>(emptyList())
    val organization
        get() = _organizations.asStateFlow()

    suspend fun loadOrganizations() {
        val organizationsInfo = userSessionFacade
            .organizations
            .map {
                it.name to it.address
            }

        _organizations.emit(organizationsInfo)
    }
}