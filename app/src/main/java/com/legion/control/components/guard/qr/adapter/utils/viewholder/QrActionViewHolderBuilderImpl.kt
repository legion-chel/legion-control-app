package com.legion.control.components.guard.qr.adapter.utils.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import com.legion.control.components.guard.qr.adapter.holders.QrActionHolder
import com.legion.control.components.guard.qr.adapter.holders.QrPhotoActionHolder
import com.legion.control.components.guard.qr.adapter.holders.QrTextActionHolder
import com.legion.control.components.guard.qr.adapter.utils.ActionViewType
import com.legion.control.databinding.ItemQrPhotoActionBinding
import com.legion.control.databinding.ItemQrTextHolderBinding
import javax.inject.Inject

class QrActionViewHolderBuilderImpl @Inject constructor() : QrActionViewHolderBuilder {

    override fun build(
        viewType: ActionViewType,
        inflater: LayoutInflater,
        viewGroup: ViewGroup?
    ): QrActionHolder {
        return when(viewType) {
            ActionViewType.PHOTO -> createPhotoViewHolder(inflater, viewGroup)
            ActionViewType.TEXT -> createTextViewHolder(inflater, viewGroup)
        }
    }

    private fun createPhotoViewHolder(
        layoutInflater: LayoutInflater,
        viewGroup: ViewGroup?
    ): QrActionHolder {
        val binding = ItemQrPhotoActionBinding.inflate(layoutInflater, viewGroup, false)
        return QrPhotoActionHolder(binding)
    }

    private fun createTextViewHolder(
        layoutInflater: LayoutInflater,
        viewGroup: ViewGroup?
    ): QrActionHolder {
        val binding = ItemQrTextHolderBinding.inflate(layoutInflater, viewGroup, false)
        return QrTextActionHolder(binding)
    }
}