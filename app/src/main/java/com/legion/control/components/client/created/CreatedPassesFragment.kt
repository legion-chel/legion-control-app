package com.legion.control.components.client.created

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.legion.control.components.client.created.adapter.CreatedPassClickListener
import com.legion.control.components.client.created.adapter.CreatedPassesAdapter
import com.legion.control.components.client.created.alert.ShowQrDialog
import com.legion.control.databinding.FragmentCreatedPassesBinding
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.control.models.client.data.CreatedPass
import com.legion.control.models.ui.AppFragment
import com.legion.core.state.UIState
import com.legion.utils.extensions.setVisibility
import com.legion.utils.extensions.showToast
import javax.inject.Inject

class CreatedPassesFragment @Inject constructor(
    private val factory: ViewModelFactory
) : AppFragment() {

    private var _binding: FragmentCreatedPassesBinding? = null
    val binding
        get() = _binding!!

    private val viewModel: CreatedPassesViewModel by viewModels { factory }

    private val createdPassClickListener = CreatedPassClickListener {
        val fragment = ShowQrDialog(it.qrUrl)
        fragment.show(parentFragmentManager, fragment.tag)
    }

    private val createdPassesAdapter = CreatedPassesAdapter().apply {
        clickListener = createdPassClickListener
    }

    private val uiStateObserver = Observer<UIState<List<CreatedPass>>> {
        when(it) {
            is UIState.Loading -> onLoading()
            is UIState.Success -> onSuccess(it.data)
            is UIState.Error -> onError(it.error)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCreatedPassesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding.createdPasses) {
            adapter = createdPassesAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        viewModel.uiState.observe(viewLifecycleOwner, uiStateObserver)
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadCreatedPasses()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun onLoading() = setLoadingState(true)

    private fun onSuccess(createdPasses: List<CreatedPass>) {
        setLoadingState(false)
        createdPassesAdapter.updatePasses(createdPasses)
    }

    private fun onError(error: String) {
        setLoadingState(false)
        showToast(error)
    }

    private fun setLoadingState(isLoading: Boolean) {
        binding.loadingMessage.setVisibility(isLoading)
        binding.createdPasses.setVisibility(!isLoading)

        val delegate = if (isLoading) progressIndicator::showProgress
        else progressIndicator::hideProgress
        delegate.invoke()
    }
}