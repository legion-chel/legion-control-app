package com.legion.control.components.home.ui

import androidx.lifecycle.ViewModel
import com.legion.control.models.home.cases.GetHomeMenuIdUseCase
import com.legion.control.models.home.cases.GetHomePagesUseCase
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val getHomePagesUseCase: GetHomePagesUseCase,
    private val getHomeMenuIdUseCase: GetHomeMenuIdUseCase
) : ViewModel() {

    fun getPages() = getHomePagesUseCase.getPages()

    fun getMenuId() = getHomeMenuIdUseCase.getMenuId()
}