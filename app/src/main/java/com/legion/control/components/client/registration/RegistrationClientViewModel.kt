package com.legion.control.components.client.registration

import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.legion.control.models.auth.model.AuthModel
import com.legion.control.models.client.data.ClientInfo
import com.legion.control.models.client.scenario.AddClientPhotoScenario
import com.legion.control.models.client.scenario.RegistrationClientScenario
import com.legion.control.models.client.template.ValidateInputClientInfoTemplate
import com.legion.control.models.utils.GetDateStringUseCase
import com.legion.core.state.UIState
import com.legion.utils.InputDelegate
import com.legion.utils.extensions.asImmutable
import com.legion.utils.extensions.empty
import com.legion.utils.extensions.observeDelegate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.launch
import javax.inject.Inject

class RegistrationClientViewModel @Inject constructor(
    private val validateInputClientInfoTemplate: ValidateInputClientInfoTemplate,
    private val addClientPhotoScenario: AddClientPhotoScenario,
    private val getDateStringUseCase: GetDateStringUseCase,
    private val registrationClientScenario: RegistrationClientScenario,
    private val authModel: AuthModel,
) : ViewModel() {
    
    val firstNameInputState = validateInputClientInfoTemplate
        .firstNameInputState
        .transform {
            emit(it ?: return@transform)
        }
        .asLiveData(Dispatchers.Default)
    
    val secondNameInputState = validateInputClientInfoTemplate
        .secondNameInputState
        .transform {
            emit(it ?: return@transform)
        }
        .asLiveData(Dispatchers.Default)
    
    val emailInputState = validateInputClientInfoTemplate
        .emailInputState
        .transform {
            emit(it ?: return@transform)
        }
        .asLiveData(Dispatchers.Default)
    
    val documentSerialInputState = validateInputClientInfoTemplate
        .documentSerialInputState
        .transform {
            emit(it ?: return@transform)
        }
        .asLiveData(Dispatchers.Default)
    
    val documentNumberInputState = validateInputClientInfoTemplate
        .documentNumberInputState
        .transform {
            emit(it ?: return@transform)
        }
        .asLiveData(Dispatchers.Default)
    
    private var firstName = String.empty
    val firstNameInputDelegate: InputDelegate = {
        firstName = it
            ?.toString()
            ?: String.empty
        
        viewModelScope.launch {
            validateInputClientInfoTemplate.validateFirstName(firstName)
        }
    }
    
    private var secondName = String.empty
    val secondNameInputDelegate: InputDelegate = {
        secondName = it
            ?.toString()
            ?: String.empty
    
        viewModelScope.launch {
            validateInputClientInfoTemplate.validateSecondName(secondName)
        }
    }
    
    private var email = String.empty
    val emailInputDelegate: InputDelegate = {
        email = it
            ?.toString()
            ?: String.empty
        
        viewModelScope.launch {
            validateInputClientInfoTemplate.validateEmail(email)
        }
    }
    
    private var documentSerial = String.empty
    val documentSerialInputDelegate: InputDelegate = {
        documentSerial = it
            ?.toString()
            ?: String.empty
    
        viewModelScope.launch {
            validateInputClientInfoTemplate.validateDocumentSerial(documentSerial)
        }
    }
    
    private var documentNumber = String.empty
    val documentNumberInputDelegate: InputDelegate = {
        documentNumber = it
            ?.toString()
            ?: String.empty
    
        viewModelScope.launch {
            validateInputClientInfoTemplate.validateDocumentNumber(documentNumber)
        }
    }
    
    val attachedPhotos = addClientPhotoScenario
        .attachedPhotos
        .asLiveData(Dispatchers.Default)
    
    val buttonEnabled = validateInputClientInfoTemplate
        .buttonEnabled
        .asLiveData(Dispatchers.Default)
    
    private val _registrationUIState = MutableLiveData<UIState<String>>()
    val registrationUIState
        get() = _registrationUIState.asImmutable()
    
    private var birthDate = String.empty
    fun getBirthDateString(long: Long): String = getDateStringUseCase(long).also {
        birthDate = it
    }
    
    fun addAvatar(avatarUri: Uri, avatarBitmap: Bitmap?) {
        viewModelScope.launch {
            addClientPhotoScenario.addAvatar(avatarUri, avatarBitmap)
        }
    }
    
    fun addDocumentPhoto(documentPhotoUri: Uri, documentBitmap: Bitmap?) {
        viewModelScope.launch {
            addClientPhotoScenario.addDocumentPhoto(documentPhotoUri, documentBitmap)
        }
    }
    
    fun registerClient() {
        viewModelScope.launch {
            val avatar = addClientPhotoScenario.getAvatarUri() ?: return@launch
            val documentsPhoto = addClientPhotoScenario.getDocumentsUrl()
            
            val clientInfo = ClientInfo(
                firstName = firstName,
                secondName = secondName,
                email = email,
                birthDate = birthDate,
                passportData = documentSerial,
                avatar = avatar,
                documentPhoto = documentsPhoto
            )

            registrationClientScenario
                .registrationClient(clientInfo)
                .collect(_registrationUIState::observeDelegate)
        }
    }
    
    fun logoutIfNeed() {
        val state = registrationUIState
            .value
        
        if (state !is UIState.Success) viewModelScope.launch {
            authModel.logout()
        }
    }
}