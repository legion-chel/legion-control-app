package com.legion.control.components.guard.qr.adapter.listeners

import android.graphics.Bitmap
import android.net.Uri
import com.legion.control.models.qr.actions.data.Action

fun interface OnActionPhotoClickListener {
    fun onClick(action: Action, imageUri: Uri, bitmap: Bitmap?)
}

fun interface OnActionTextClickListener {
    fun onClick(action: Action, text: String)
}