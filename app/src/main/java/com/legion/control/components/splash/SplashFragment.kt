package com.legion.control.components.splash

import androidx.fragment.app.viewModels
import com.legion.control.R
import com.legion.control.di.core.viewmodel.ViewModelFactory
import com.legion.control.models.ui.AppFragment
import com.legion.utils.extensions.navigate
import javax.inject.Inject

class SplashFragment @Inject constructor(
    private val factory: ViewModelFactory
) : AppFragment(R.layout.fragment_splash) {

    private val viewModel: SplashViewModel by viewModels { factory }

    override fun onResume() {
        super.onResume()
        
        viewModel.loadingState.observe(viewLifecycleOwner) {
            val delegate = if (it) progressIndicator::showProgress
            else progressIndicator::hideProgress
            
            delegate()
        }

        viewModel.syncData()
    }

    private fun toLogin() {
        val action = SplashFragmentDirections.toLoginFromSplash()
        navigate(action)
    }

    private fun toHome() {
        val action = SplashFragmentDirections.toHome()
        navigate(action)
    }
    
    private fun toEnterInfo() {
        val action = SplashFragmentDirections.toRegistration()
        navigate(action)
    }
}