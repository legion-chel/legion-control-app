package com.legion.control.components.client.pass.visitor

import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.legion.control.models.client.cases.AppendPhotoForPassUseCase
import com.legion.control.models.client.cases.visitor.CreateVisitorPassUseCase
import com.legion.control.models.client.data.VisitorPass
import com.legion.core.state.LoadingState
import com.legion.core.state.UIState
import com.legion.utils.InputDelegate
import com.legion.utils.extensions.empty
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.launch
import javax.inject.Inject

class CreateVisitorPassViewModel @Inject constructor(
    private val createVisitorPassUseCase: CreateVisitorPassUseCase,
    private val appendPhotoForPassUseCase: AppendPhotoForPassUseCase
) : ViewModel() {

    private var secondName = String.empty
    private var firstName = String.empty
    private var patronymic = String.empty
    private var photos = mutableListOf<Uri>()

    val inputSecondNameDelegate: InputDelegate = {
        secondName = it?.toString() ?: String.empty
    }

    val inputFirstNameDelegate: InputDelegate = {
        firstName = it?.toString() ?: String.empty
    }

    val inputPatronymicDelegate: InputDelegate = {
        patronymic = it?.toString() ?: String.empty
    }

    val appendedPhotos = appendPhotoForPassUseCase
        .appendedPhotos
        .asLiveData(Dispatchers.Default)

    val uiState = createVisitorPassUseCase
        .loadingState
        .transform {
            val state = when (it) {
                null -> UIState.Loading
                is LoadingState.Success -> UIState.Success(it.data)
                is LoadingState.Error -> UIState.Error(it.error)
                else -> return@transform
            }

            emit(state)
        }
        .asLiveData(Dispatchers.Default)

    fun create() {
        viewModelScope.launch {
            val visitorPass = VisitorPass(firstName, secondName, patronymic, photos)
            createVisitorPassUseCase.create(visitorPass)
        }
    }

    fun appendPhoto(uri: Uri, bitmap: Bitmap?) {
        viewModelScope.launch(Dispatchers.Default) {
            photos.add(uri)
            appendPhotoForPassUseCase.append(uri, bitmap)
        }
    }
}