package com.legion.control.components.guard.qr.goods.data

import com.legion.control.components.guard.qr.adapter.data.ActionUIWrapper
import com.legion.control.models.qr.info.data.ActionType

class GoodsQrInfoUIWrapper(
    val qrId: Int,
    val actionType: ActionType,
    val description: String,
    val count: Int,
    val direction: String,
    val actions: List<ActionUIWrapper>
)