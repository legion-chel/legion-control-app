package com.legion.control.components.guard.schedules.finish

import com.legion.control.components.guard.schedules.core.ActionShiftViewModel
import com.legion.control.models.schedules.cases.FinishScheduleUseCase
import com.legion.control.models.schedules.scenario.FinishScheduleUIScenario
import com.legion.control.models.upload.scenario.UploadImageScenario
import javax.inject.Inject

class FinishShiftViewModel @Inject constructor(
    uploadImageScenario: UploadImageScenario,
	finishScheduleUseCase: FinishScheduleUseCase,
    finishScheduleUIScenario: FinishScheduleUIScenario,
) : ActionShiftViewModel(
    uploadImageScenario = uploadImageScenario,
	actScheduleUseCase = finishScheduleUseCase,
    actUIScenario = finishScheduleUIScenario
)