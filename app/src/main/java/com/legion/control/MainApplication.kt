package com.legion.control

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.legion.control.di.components.AppComponent
import com.legion.control.di.components.DaggerAppComponent
import com.legion.utils.log.LogConfigurator
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class MainApplication : Application(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var logConfigurator: LogConfigurator

    private val appComponent: AppComponent by lazy {
        DaggerAppComponent
            .builder()
            .application(this)
            .build()
    }
    
    private lateinit var _currentActivity: MainActivity
    val currentActivity: MainActivity get() = _currentActivity
    
    private val activityCallback = object : ActivityLifecycleCallbacks {
        override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
            _currentActivity = activity as? MainActivity ?: return
        }
    
        override fun onActivityStarted(activity: Activity) {}
    
        override fun onActivityResumed(activity: Activity) {}
    
        override fun onActivityPaused(activity: Activity) {}
    
        override fun onActivityStopped(activity: Activity) {}
    
        override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}
    
        override fun onActivityDestroyed(activity: Activity) {}
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
        
        logConfigurator.configure(BuildConfig.DEBUG)
        registerActivityLifecycleCallbacks(activityCallback)
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector
}