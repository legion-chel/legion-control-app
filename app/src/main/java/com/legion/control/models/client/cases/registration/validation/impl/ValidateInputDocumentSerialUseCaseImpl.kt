package com.legion.control.models.client.cases.registration.validation.impl

import com.legion.control.R
import com.legion.control.models.client.cases.registration.validation.ValidateInputDocumentSerialUseCase
import com.legion.utils.InputState
import com.legion.utils.ResourceProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ValidateInputDocumentSerialUseCaseImpl @Inject constructor(
    private val resourceProvider: ResourceProvider
) : ValidateInputDocumentSerialUseCase {
    
    private val _inputState = MutableStateFlow<InputState?>(null)
    override val inputState: Flow<InputState?>
        get() = _inputState
    
    override suspend fun validate(serial: String) {
        withContext(Dispatchers.IO) {
            val state = when {
                serial.isEmpty() -> {
                    val errMessage = resourceProvider.getString(R.string.error_empty_input)
                    InputState.Incorrect(errMessage)
                }
                
                serial.length != 11 -> {
                    val errMessage = resourceProvider.getString(R.string.error_doc_serial_incorrect_length)
                    InputState.Incorrect(errMessage)
                }
                
                else -> InputState.Correct
            }
            
            _inputState.emit(state)
        }
    }
}