package com.legion.control.models.upload.cases

import android.net.Uri
import com.legion.control.models.upload.data.UploadImageState
import com.legion.control.models.upload.data.UploadImageType
import kotlinx.coroutines.flow.StateFlow

interface UploadImageUseCase {
	val uploadImageState: StateFlow<UploadImageState?>
	suspend fun uploadImage(uri: Uri?, imageType: UploadImageType)
}