package com.legion.control.models.client.repository

import com.legion.api.http.ApiResult
import com.legion.control.models.client.data.CreatedPass
import com.legion.control.models.client.data.EmployeePass
import com.legion.control.models.client.data.GoodsPass
import com.legion.control.models.client.data.VisitorPass

interface PassRepository {
    suspend fun createGoodsPass(goodsPass: GoodsPass): ApiResult<Nothing>
    suspend fun createEmployeePass(employeePass: EmployeePass): ApiResult<Nothing>
    suspend fun createVisitorPass(visitorPass: VisitorPass): ApiResult<Nothing>

    suspend fun getGoodsCreatedPasses(): ApiResult<List<CreatedPass.Goods>>
    suspend fun getEmployeeCreatedPasses(): ApiResult<List<CreatedPass.Employee>>
    suspend fun getVisitorCreatedPasses(): ApiResult<List<CreatedPass.Visitor>>
}