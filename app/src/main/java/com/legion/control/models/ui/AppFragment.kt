package com.legion.control.models.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.transition.MaterialSharedAxis
import com.legion.utils.extensions.empty

abstract class AppFragment : Fragment {
    constructor() : super()
    constructor(layout: Int) : super(layout)

    protected val progressIndicator: ProgressIndicator by lazy {
        requireActivity() as ProgressIndicator
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        
        exitTransition = MaterialSharedAxis(MaterialSharedAxis.X, true)
        reenterTransition = MaterialSharedAxis(MaterialSharedAxis.X, false)
    }
    
    fun createCalendarPicker(title: String = String.empty): MaterialDatePicker<Long> {
        return MaterialDatePicker.Builder.datePicker()
            .setTitleText(title)
            .setInputMode(MaterialDatePicker.INPUT_MODE_CALENDAR)
            .build()
    }
}