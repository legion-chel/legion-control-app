package com.legion.control.models.qr.info.data.employee

import com.legion.control.components.guard.qr.adapter.data.ActionUIWrapper
import com.legion.control.models.qr.info.data.ActionType

data class EmployeeQrInfoUIWrapper(
    val qrId: Int,
    val actionType: ActionType,
    val fullName: String,
    val access: String,
    val actions: List<ActionUIWrapper>
)