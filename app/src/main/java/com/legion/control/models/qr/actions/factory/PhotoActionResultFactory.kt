package com.legion.control.models.qr.actions.factory

import android.net.Uri
import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.actions.data.result.QrActionResult
import com.legion.control.models.qr.info.data.ActionType

interface PhotoActionResultFactory {
    suspend fun create(qrId: Int, actionType: ActionType, actions: Map<Action, Uri>): List<QrActionResult>
}