package com.legion.control.models.client.cases.visitor.impl

import com.legion.api.http.ApiResult
import com.legion.control.models.client.cases.visitor.CreateVisitorPassUseCase
import com.legion.control.models.client.data.VisitorPass
import com.legion.control.models.client.repository.PassRepository
import com.legion.core.state.LoadingState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CreateVisitorPassUseCaseImpl @Inject constructor(
    private val repository: PassRepository
) : CreateVisitorPassUseCase {

    private val _loadingState = MutableStateFlow<LoadingState<String>?>(null)
    override val loadingState: StateFlow<LoadingState<String>?>
        get() = _loadingState.asStateFlow()

    override suspend fun create(visitorPass: VisitorPass) = withContext(IO) {
        if (visitorPass.photos.isEmpty()) {
            val error = LoadingState.Error("Необходимо приложить фотографию документов")
            setState(error)
            return@withContext
        }

        setState(LoadingState.Processing)

        val result = repository.createVisitorPass(visitorPass)
        val state = handleApiResult(result)

        setState(state)
    }

    private fun handleApiResult(result: ApiResult<Nothing>): LoadingState<String> {
        return if (result is ApiResult.Success) LoadingState.Success("Пропуск создан")
        else LoadingState.Error("Ошибка создания пропуска")
    }

    private suspend fun setState(state: LoadingState<String>?) {
        withContext(Dispatchers.Default) {
            _loadingState.emit(state)
        }
    }
}