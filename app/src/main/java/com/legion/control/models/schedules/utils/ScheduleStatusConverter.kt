package com.legion.control.models.schedules.utils

import com.legion.control.models.schedules.data.ScheduleActionDelegate
import com.legion.control.models.schedules.data.ScheduleStatus

interface ScheduleStatusConverter {
    fun convert(status: ScheduleStatus): ScheduleActionDelegate?
}