package com.legion.control.models.ui.factory

interface Factory<T> {
    fun create(): List<T>
}