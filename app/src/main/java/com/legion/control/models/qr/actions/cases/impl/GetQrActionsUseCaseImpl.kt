package com.legion.control.models.qr.actions.cases.impl

import com.legion.control.models.qr.actions.cases.GetQrActionsUseCase
import com.legion.control.models.qr.actions.data.state.ActionLoadingState
import com.legion.control.models.qr.actions.repository.QrActionRepository
import com.legion.control.models.qr.info.data.ActionType
import com.legion.control.models.schedules.repository.ScheduleRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class GetQrActionsUseCaseImpl @Inject constructor(
    private val schedulesRepository: ScheduleRepository,
    private val qrRepository: QrActionRepository
) : GetQrActionsUseCase {

    override suspend fun getActions(actionType: ActionType): StateFlow<ActionLoadingState?> {
        return MutableStateFlow(null)
    }
}