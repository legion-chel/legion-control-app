package com.legion.control.models.qr.actions.builder

import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.actions.data.result.QrActionResult
import com.legion.control.models.qr.info.data.ActionType
import com.legion.utils.extensions.empty

class ActionResultBuilder {

    private var result = String.empty
    private var qrType = String.empty
    private var qrId = 0
    private var insertDate = 0L

    fun setResult(result: String) = apply {
        this.result = result
    }

    fun setQrType(actionType: ActionType) = apply {
        this.qrType = actionType.title
    }

    fun setQrId(qrId: Int) = apply {
        this.qrId = qrId
    }

    fun setInsertDate(insertDate: Long) = apply {
        this.insertDate = insertDate
    }

    fun build(action: Action): QrActionResult = QrActionResult(
        scenarioId = action.scenarioId,
        templateActionId = action.templateActionId,
        qrType = qrType,
        qrId = qrId,
        insertDate = insertDate,
        result = result
    )
}