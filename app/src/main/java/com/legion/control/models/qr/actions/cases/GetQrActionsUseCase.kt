package com.legion.control.models.qr.actions.cases

import com.legion.control.models.qr.actions.data.state.ActionLoadingState
import com.legion.control.models.qr.info.data.ActionType
import kotlinx.coroutines.flow.StateFlow

interface GetQrActionsUseCase {
    suspend fun getActions(actionType: ActionType): StateFlow<ActionLoadingState?>
}