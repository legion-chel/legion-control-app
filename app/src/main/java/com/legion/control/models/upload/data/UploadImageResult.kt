package com.legion.control.models.upload.data

import com.google.gson.annotations.SerializedName

data class UploadImageResult(
    @SerializedName("id")
    val imageId: Int,

    @SerializedName("photoUrl")
    val url: String
)