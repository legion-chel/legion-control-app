package com.legion.control.models.sync.usecases

import com.legion.control.models.sync.data.SyncState

interface GetLoadingStateBySyncStateUseCase {
    operator fun invoke(syncState: SyncState): Boolean
}