package com.legion.control.models.upload.data

sealed class UploadImageState {
	class Success(val result: UploadImageResult) : UploadImageState()
	class Error(val error: String) : UploadImageState()
}