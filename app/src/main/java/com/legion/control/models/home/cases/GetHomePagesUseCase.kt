package com.legion.control.models.home.cases

import androidx.fragment.app.Fragment

interface GetHomePagesUseCase {
    fun getPages(): List<Fragment>
}