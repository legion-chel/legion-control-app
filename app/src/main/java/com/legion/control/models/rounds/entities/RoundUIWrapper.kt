package com.legion.control.models.rounds.entities

import com.legion.control.components.guard.qr.adapter.data.ActionUIWrapper
import com.legion.control.models.qr.info.data.ActionType

data class RoundUIWrapper(
    val roundId: Int,
    val title: String,
    val actionType: ActionType,
    val description: String,
    val scenarioTemplateId: Int,
    val actions: List<ActionUIWrapper>
)