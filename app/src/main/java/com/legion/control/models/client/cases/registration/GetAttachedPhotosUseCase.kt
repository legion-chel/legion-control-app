package com.legion.control.models.client.cases.registration

import android.graphics.Bitmap
import kotlinx.coroutines.flow.Flow

interface GetAttachedPhotosUseCase {
    operator fun invoke(): Flow<List<Bitmap>>
}