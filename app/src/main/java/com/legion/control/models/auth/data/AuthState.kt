package com.legion.control.models.auth.data

import com.legion.core.session.auth.AuthInfo

sealed class AuthState {
    class Success(val authInfo: AuthInfo) : AuthState()
    class Failed(val message: String) : AuthState()
}