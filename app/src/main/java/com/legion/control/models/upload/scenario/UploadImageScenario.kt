package com.legion.control.models.upload.scenario

import android.graphics.Bitmap
import android.net.Uri
import com.legion.control.models.upload.data.UploadImageState
import com.legion.control.models.upload.data.UploadImageType
import kotlinx.coroutines.flow.StateFlow

interface UploadImageScenario {
    val uploadImageState: StateFlow<UploadImageState?>
    val bitmap: StateFlow<Bitmap?>

    suspend fun saveImage(imageUri: Uri)
    suspend fun uploadImage(imageType: UploadImageType)
}