package com.legion.control.models.client.cases.goods.impl

import com.legion.control.R
import com.legion.control.models.client.cases.goods.CreateGoodsPassUseCase
import com.legion.control.models.client.data.GoodsPass
import com.legion.control.models.client.repository.PassRepository
import com.legion.core.state.LoadingState
import com.legion.utils.ResourceProvider
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class CreateGoodsPassUseCaseImpl @Inject constructor(
    private val passRepository: PassRepository,
    private val resourceProvider: ResourceProvider
) : CreateGoodsPassUseCase {

    override fun invoke(goodsPass: GoodsPass): Flow<LoadingState<String>?>  = flow {
        if (goodsPass.photos.isEmpty()) {
            val errString = resourceProvider.getString(R.string.need_add_photo_for_pass)
            val error = LoadingState.Error(errString)
            emit(error)
            return@flow
        }
        
        emit(LoadingState.Processing)

        val result = passRepository.createGoodsPass(goodsPass)
        result.handleResult(
            successDelegate = {
                val message = resourceProvider.getString(R.string.pass_created)
                emit(LoadingState.Success(message))
            },
            httpErrorDelegate = {
                val message = resourceProvider.getString(R.string.network_error)
                emit(LoadingState.Error(message))
            },
            errorDelegate = {
                val message = resourceProvider.getString(R.string.error_on_create_pass)
                emit(LoadingState.Error(message))
            }
        )
    }
}