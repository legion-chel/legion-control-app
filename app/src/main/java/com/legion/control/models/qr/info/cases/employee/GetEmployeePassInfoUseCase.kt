package com.legion.control.models.qr.info.cases.employee

import com.legion.control.models.qr.info.data.employee.EmployeeQrInfo
import kotlinx.coroutines.flow.StateFlow

interface GetEmployeePassInfoUseCase {
    val fullName: StateFlow<String>
    val isEnabledPass: StateFlow<String>
    suspend fun getPassInfo(qrInfo: EmployeeQrInfo)
}