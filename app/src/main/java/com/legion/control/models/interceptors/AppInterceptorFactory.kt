package com.legion.control.models.interceptors

import com.legion.api.factory.InterceptorsFactory
import okhttp3.logging.HttpLoggingInterceptor

class AppInterceptorFactory(
    private val authInterceptor: AuthInterceptor,
    private val userInterceptor: UserInterceptor
) : InterceptorsFactory {
    override fun create() = listOf(
        authInterceptor,
        userInterceptor,
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BASIC
        }
    )
}