package com.legion.control.models.client.data

import android.net.Uri

data class ClientInfo(
    val firstName: String,
    val secondName: String,
    val email: String,
    val birthDate: String,
    val passportData: String,
    val avatar: Uri,
    val documentPhoto: List<Uri>
)