package com.legion.control.models.client.cases.registration.validation

import com.legion.utils.InputState
import kotlinx.coroutines.flow.Flow

interface ValidateInputDocumentNumberUseCase {
    val inputState: Flow<InputState?>
    suspend fun validate(number: String)
}