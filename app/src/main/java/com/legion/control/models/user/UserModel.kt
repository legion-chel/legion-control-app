package com.legion.control.models.user

import com.legion.api.http.ApiResult
import com.legion.core.session.UserSessionFacade
import javax.inject.Inject

class UserModel @Inject constructor(
    private val apiService: UserService,
    private val userSessionFacade: UserSessionFacade
) {

    suspend fun loadAndSave() {
        val userInfoResult = apiService.getUserInfo()
        if (userInfoResult is ApiResult.Success) userSessionFacade.saveUser(userInfoResult.value)

        val organizationsResult = apiService.getOrganizations()
        if (organizationsResult is ApiResult.Success)
            userSessionFacade.saveOrganizations(organizationsResult.value)
    }
}