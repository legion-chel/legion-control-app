package com.legion.control.models.client.cases.visitor

import com.legion.control.models.client.data.CreatedPass
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.StateFlow

interface GetVisitorCreatedPassesUseCase {
    val loadingState: StateFlow<LoadingState<List<CreatedPass>>?>
    val createdPasses: StateFlow<List<CreatedPass>>
    suspend fun getCreatedPasses()
}