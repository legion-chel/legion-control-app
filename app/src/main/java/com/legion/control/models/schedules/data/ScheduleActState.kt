package com.legion.control.models.schedules.data

sealed class ScheduleActState {
	open class Success : ScheduleActState() {
		class WithMessage(val message: String): Success()
	}


	open class Error : ScheduleActState() {
		class WithMessage(val message: String): Error()
	}
}