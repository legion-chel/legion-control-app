package com.legion.control.models.schedules.cases

import com.legion.control.models.schedules.data.ScheduleActState
import kotlinx.coroutines.flow.StateFlow

interface LoadScheduleUseCase {
	val loadingState: StateFlow<ScheduleActState?>
	suspend fun loadSchedule()
}