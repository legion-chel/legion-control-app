package com.legion.control.models.auth.model

import com.legion.api.UNAUTHORIZED
import com.legion.api.http.ApiResult
import com.legion.control.R
import com.legion.control.models.auth.data.AuthState
import com.legion.core.session.auth.AuthInfo
import com.legion.utils.ResourceProvider
import com.legion.utils.log.Logger
import javax.inject.Inject

class AuthValidator @Inject constructor(
    private val resourceProvider: ResourceProvider,
) {
    fun validateAuthResponse(result: ApiResult<AuthInfo>): AuthState {
        return when (result) {
            is ApiResult.Success -> AuthState.Success(result.value)
            is ApiResult.Failure.HttpError -> handleHttpError(result)
            is ApiResult.Failure.Error -> handleUnknownError(result)
        }
    }

    private fun handleHttpError(result: ApiResult.Failure.HttpError): AuthState {
        return if (result.statusCode == UNAUTHORIZED) {
            Logger.e(result.error, "Auth error. Incorrect data")
            AuthState.Failed(resourceProvider.getString(R.string.auth_error_invalid_data))
        } else handleUnknownError(result)
    }

    private fun handleUnknownError(result: ApiResult.Failure<*>): AuthState {
        Logger.e(result.error, "Auth error. Unknown error")
        return AuthState.Failed(resourceProvider.getString(R.string.auth_error_unknown))
    }
}