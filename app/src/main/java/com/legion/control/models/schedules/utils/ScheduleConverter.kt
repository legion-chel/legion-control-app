package com.legion.control.models.schedules.utils

import com.legion.control.models.schedules.data.Schedule
import com.legion.control.models.schedules.data.ui.ScheduleUIWrapper

interface ScheduleConverter {
    fun convert(schedule: Schedule): ScheduleUIWrapper
}