package com.legion.control.models.client.cases.visitor

import com.legion.control.models.client.data.VisitorPass
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.StateFlow

interface CreateVisitorPassUseCase {
    val loadingState: StateFlow<LoadingState<String>?>
    suspend fun create(visitorPass: VisitorPass)
}