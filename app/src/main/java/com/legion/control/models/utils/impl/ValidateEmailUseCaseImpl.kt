package com.legion.control.models.utils.impl

import android.util.Patterns
import com.legion.control.R
import com.legion.control.models.utils.ValidateEmailUseCase
import com.legion.utils.InputState
import com.legion.utils.ResourceProvider
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ValidateEmailUseCaseImpl @Inject constructor(
    private val resourceProvider: ResourceProvider
) : ValidateEmailUseCase {
    
    private val _inputState = MutableStateFlow<InputState?>(null)
    override val inputState: Flow<InputState?>
        get() = _inputState
    
    override suspend fun validate(email: String) {
        withContext(IO) {
            val isCorrect = Patterns
                .EMAIL_ADDRESS
                .matcher(email)
                .matches()
            
            val state = if (isCorrect) InputState.Correct
            else {
                val error = resourceProvider.getString(R.string.incorrect_email)
                InputState.Incorrect(error)
            }
            
            _inputState.emit(state)
        }
    }
}