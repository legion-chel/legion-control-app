package com.legion.control.models.client.scenario

import android.graphics.Bitmap
import android.net.Uri
import com.legion.control.models.qr.info.data.goods.GoodsDirection
import com.legion.core.state.UIState
import kotlinx.coroutines.flow.Flow

interface CreateGoodsPassScenario {
    val appendedPhotos: Flow<List<Bitmap>>

    suspend fun create(
        description: String,
        count: String,
        direction: GoodsDirection
    ): Flow<UIState<String>>

    suspend fun appendPhoto(uri: Uri, bitmap: Bitmap?)
}