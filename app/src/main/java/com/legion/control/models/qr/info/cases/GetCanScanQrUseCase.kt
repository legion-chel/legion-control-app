package com.legion.control.models.qr.info.cases

interface GetCanScanQrUseCase {
    suspend operator fun invoke(): Boolean
}