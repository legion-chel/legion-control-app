package com.legion.control.models.schedules.cases.ui.impl

import android.graphics.Bitmap
import com.legion.control.R
import com.legion.control.models.schedules.cases.ui.GetFinishScheduleButtonTitleUseCase
import com.legion.utils.ResourceProvider
import com.legion.utils.extensions.empty
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class GetFinishScheduleButtonTitleUseCaseImpl @Inject constructor(
    private val resourceProvider: ResourceProvider
) : GetFinishScheduleButtonTitleUseCase {

    private val _title = MutableStateFlow(String.empty)
    override val title: StateFlow<String>
        get() = _title

    override suspend fun getButtonTitle(bitmap: Bitmap?) {
        val stringId = if (bitmap == null) R.string.take_image_button_text
        else R.string.finish_shift_button_text

        _title.emit(resourceProvider.getString(stringId))
    }
}