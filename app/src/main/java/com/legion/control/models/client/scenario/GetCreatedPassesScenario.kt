package com.legion.control.models.client.scenario

import com.legion.control.models.client.data.CreatedPass
import com.legion.core.state.UIState
import kotlinx.coroutines.flow.Flow

interface GetCreatedPassesScenario {
    val uiState: Flow<UIState<List<CreatedPass>>>
    suspend fun getCreatedPasses()
}