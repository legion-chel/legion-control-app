package com.legion.control.models.client.api

import com.legion.api.http.ApiResult
import com.legion.control.models.client.data.CreatedPass
import com.legion.control.models.client.data.VisitorPass
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ClientApi {
    @Multipart
    @POST("/goods/add")
    suspend fun createGoodsPass(
        @Part("description") description: RequestBody,
        @Part("count") count: RequestBody,
        @Part("direction") direction: RequestBody,
        @Part photos: List<MultipartBody.Part>
    ): ApiResult<Nothing>

    @Multipart
    @POST("/employees/add")
    suspend fun createEmployeePass(
        @Part("employee_name") firstName: RequestBody,
        @Part("employee_last_name") secondName: RequestBody,
        @Part("employee_patronymic_name") patronymic: RequestBody,
        @Part photos: List<MultipartBody.Part>
    ): ApiResult<Nothing>

    @POST("/visitor-pass/add")
    suspend fun createVisitorPass(@Body visitorPass: VisitorPass): ApiResult<Nothing>

    @GET("/goods")
    suspend fun getGoodsCreatedPasses(): ApiResult<List<CreatedPass.Goods>>

    @GET("/employees")
    suspend fun getEmployeeCreatedPasses(): ApiResult<List<CreatedPass.Employee>>

    @GET("/visitor-pass")
    suspend fun getVisitorCreatedPasses(): ApiResult<List<CreatedPass.Visitor>>
    
    @Multipart
    @POST("/clients/users/{id}")
    suspend fun registrationClient(
        @Path("id") userId:Int?,
        @Part("first_name") firstName: RequestBody,
        @Part("last_name") secondName: RequestBody,
        @Part("email") email: RequestBody,
        @Part("date_of_birth") birthDate: RequestBody,
        @Part("passport_data") passportData: RequestBody,
        @Part avatar: MultipartBody.Part,
        @Part documents: List<MultipartBody.Part>
    ): ApiResult<Nothing>
}