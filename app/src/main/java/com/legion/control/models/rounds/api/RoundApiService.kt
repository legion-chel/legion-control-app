package com.legion.control.models.rounds.api

import com.legion.api.http.ApiResult
import com.legion.control.models.rounds.entities.api.GetRoundsResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface RoundApiService {
    @GET("/schedules/{scheduleId}/rounds")
    suspend fun getRounds(@Path("scheduleId") scheduleId: Int?): ApiResult<List<GetRoundsResponse>>
}