package com.legion.control.models.user

import com.legion.api.http.ApiResult
import com.legion.core.session.organization.Organization
import com.legion.core.session.user.User
import retrofit2.http.GET

interface UserService {
    @GET("/user/info")
    suspend fun getUserInfo(): ApiResult<User>

    @GET("/user/organizations")
    suspend fun getOrganizations(): ApiResult<List<Organization>>
}