package com.legion.control.models.schedules.scenario

import android.graphics.Bitmap
import kotlinx.coroutines.flow.StateFlow

interface ActScheduleUIScenario {
    val buttonTitle: StateFlow<String>
    val buttonIsEnabled: StateFlow<Boolean>
    val textIsVisible: StateFlow<Boolean>

    suspend fun getButtonTitle(bitmap: Bitmap?)
    suspend fun getTextVisibility(bitmap: Bitmap?)
    suspend fun getButtonEnabled(isLoading: Boolean)
}