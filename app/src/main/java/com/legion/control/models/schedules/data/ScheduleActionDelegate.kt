package com.legion.control.models.schedules.data

import androidx.fragment.app.FragmentManager

data class ScheduleActionDelegate(
    val title: String,
    val action: (FragmentManager) -> Unit
)