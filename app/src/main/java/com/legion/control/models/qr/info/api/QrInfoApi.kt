package com.legion.control.models.qr.info.api

import com.legion.api.http.ApiResult
import com.legion.control.models.qr.info.data.QrData
import com.legion.control.models.qr.info.data.employee.EmployeeInfo
import com.legion.control.models.qr.info.data.goods.GoodsInfo
import com.legion.control.models.qr.info.data.visitor.VisitorInfo
import retrofit2.http.Body
import retrofit2.http.POST

interface QrInfoApi {
    @POST("/qrdata/goods/validate")
    suspend fun getGoodsInfo(@Body qrData: QrData): ApiResult<GoodsInfo>

    @POST("/employees/validate")
    suspend fun getEmployeeInfo(@Body qrData: QrData): ApiResult<EmployeeInfo>

    @POST("/visitor-pass/validate")
    suspend fun getVisitorInfo(@Body qrData: QrData): ApiResult<VisitorInfo>
}