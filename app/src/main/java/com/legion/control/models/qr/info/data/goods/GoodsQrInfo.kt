package com.legion.control.models.qr.info.data.goods

import com.legion.control.models.qr.info.data.ActionType

data class GoodsQrInfo(
    val qrId: Int,
    val description: String,
    val count: Int,
    val direction: GoodsDirection,
    val actionType: ActionType
) {
    override fun equals(other: Any?) = false

    override fun hashCode(): Int {
        var result = qrId
        result = 31 * result + description.hashCode()
        result = 31 * result + count
        result = 31 * result + direction.hashCode()
        result = 31 * result + actionType.hashCode()
        return result
    }
}