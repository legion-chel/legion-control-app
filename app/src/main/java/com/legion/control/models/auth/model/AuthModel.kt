package com.legion.control.models.auth.model

import com.legion.api.http.ApiResult
import com.legion.control.models.auth.api.AuthService
import com.legion.control.models.auth.data.AuthData
import com.legion.control.models.auth.data.AuthState
import com.legion.control.models.auth.utils.LogoutBroadcast
import com.legion.control.models.schedules.db.ScheduleDao
import com.legion.core.session.UserSessionFacade
import com.legion.core.session.auth.AuthInfo
import com.legion.utils.getCurrentTime
import com.legion.utils.log.Logger
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class AuthModel @Inject constructor(
    private val authService: AuthService,
    private val userSessionFacade: UserSessionFacade,
    private val logoutSender: LogoutBroadcast.Sender,
    private val authValidator: AuthValidator,
    private val scheduleDao: ScheduleDao
) {
    val isAuthorized: Boolean
        get() = userSessionFacade.authInfo?.token?.isNotEmpty() ?: false

    val isNeedRefresh
        get() = checkNeedRefresh()

    suspend fun auth(login: String, password: String): AuthState {
        val authData = AuthData(login, password)
        val authResult = authService.auth(authData)

        val state = authValidator.validateAuthResponse(authResult)
        if (state is AuthState.Success) saveAuth(state.authInfo)

        return state
    }

    suspend fun refresh() {
        when (val result = authService.refresh()) {
            is ApiResult.Success -> saveAuth(result.value)
            is ApiResult.Failure<*> -> Logger.e(result.error, "Error refresh token")
        }
    }

    suspend fun logout() {
        userSessionFacade.clear()
        
        withContext(IO) {
            scheduleDao.deleteAll()
        }
        
        logoutSender.send()
    }

    private fun checkNeedRefresh(): Boolean {
        val authInfo = userSessionFacade.authInfo ?: return false

        val expiredTime = TimeUnit.MILLISECONDS.convert(authInfo.expireIn, TimeUnit.SECONDS)
        val currentTime = getCurrentTime()

        val diff = expiredTime - currentTime

        return TimeUnit.HOURS.convert(-diff, TimeUnit.MILLISECONDS) < NEEDED_DIFF
    }

    private fun saveAuth(authInfo: AuthInfo) = userSessionFacade.saveAuth(authInfo)

    companion object {
        private const val NEEDED_DIFF = 12
    }
}