package com.legion.control.models.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class AppAdapter<T>(
    factory: FactoryItems<T>,
    private val clickListener: ItemClickListener<T>
) : RecyclerView.Adapter<AppAdapter.AppHolder<T>>() {

    private val items = factory.create()

    override fun onBindViewHolder(holder: AppHolder<T>, position: Int) {
        val item = items[position]
        holder.bind(item, clickListener)
    }

    override fun getItemCount() = items.size

    abstract class AppHolder<T>(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(item: T, itemClickListener: ItemClickListener<T>)
    }

    fun interface ItemClickListener<T> {
        fun onClick(item: T)
    }

    fun interface FactoryItems<T> {
        fun create(): List<T>
    }
}