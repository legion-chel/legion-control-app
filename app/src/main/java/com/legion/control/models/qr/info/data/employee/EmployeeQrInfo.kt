package com.legion.control.models.qr.info.data.employee

import com.legion.control.models.qr.info.data.ActionType

class EmployeeQrInfo(
    val qrId: Int,
    val actionType: ActionType,
    val employeeInfo: EmployeeInfo
) {
    override fun equals(other: Any?) = false

    override fun hashCode(): Int {
        var result = qrId
        result = 31 * result + actionType.hashCode()
        result = 31 * result + employeeInfo.hashCode()
        return result
    }
}