package com.legion.control.models.client.scenario

import android.graphics.Bitmap
import android.net.Uri
import com.legion.core.state.UIState
import kotlinx.coroutines.flow.Flow


interface CreateEmployeePassScenario {
    val appendedPhotos: Flow<List<Bitmap>>

    suspend fun appendPhoto(uri: Uri, bitmap: Bitmap?)
    suspend fun create(
        secondName: String,
        firstName: String,
        patronymic: String
    ): Flow<UIState<String>>
}