package com.legion.control.models.qr.actions.utils.impl

import com.legion.control.components.guard.qr.adapter.data.ActionUIWrapper
import com.legion.control.models.qr.actions.data.state.ActionState
import com.legion.control.models.qr.actions.data.wrapper.ActionStateWrapper
import com.legion.control.models.qr.actions.utils.ActionStateWrapperConverter
import com.legion.utils.ResourceProvider
import javax.inject.Inject

class ActionStateWrapperConverterImpl @Inject constructor(
    private val resourceProvider: ResourceProvider
) : ActionStateWrapperConverter {

    override fun convert(wrapper: ActionStateWrapper): ActionUIWrapper {
        val actionText = when(wrapper.state) {
            ActionState.NOT_COMPLETE -> wrapper.action.description
            ActionState.COMPLETE -> "Выполнено"
        }

        val hasButtonEnabled = wrapper.state == ActionState.NOT_COMPLETE

        return ActionUIWrapper(wrapper.action, actionText, hasButtonEnabled)
    }
}