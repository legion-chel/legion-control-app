package com.legion.control.models.rounds.entities

class Round(
    val id: Int,
    val scenarioTemplateId: Int,
    val name: String,
    val description: String,
    val scheduleId: Int
)