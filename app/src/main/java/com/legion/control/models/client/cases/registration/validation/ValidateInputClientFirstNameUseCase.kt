package com.legion.control.models.client.cases.registration.validation

import com.legion.utils.InputState
import kotlinx.coroutines.flow.Flow

interface ValidateInputClientFirstNameUseCase {
    val inputState: Flow<InputState?>
    suspend fun validate(firstName: String)
}