package com.legion.control.models.schedules.cases.impl

import com.legion.control.models.schedules.cases.GetCurrentScheduleUseCase
import com.legion.control.models.schedules.data.ui.ScheduleUIWrapper
import com.legion.control.models.schedules.repository.ScheduleRepository
import com.legion.control.models.schedules.utils.ScheduleConverter
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.transform
import javax.inject.Inject

class GetCurrentScheduleUseCaseImpl @Inject constructor(
    private val repository: ScheduleRepository,
    private val converter: ScheduleConverter
) : GetCurrentScheduleUseCase {

    override val currentSchedule: Flow<ScheduleUIWrapper>
        get() = repository
            .getCurrentSchedule()
            .transform {
                it ?: return@transform
                val wrapper = converter.convert(it)
                emit(wrapper)
            }

}