package com.legion.control.models.upload.cases.impl

import android.net.Uri
import com.legion.control.models.upload.cases.SaveImageUriUseCase
import javax.inject.Inject

class SaveImageUriUseCaseImpl @Inject constructor(

) : SaveImageUriUseCase {
    private var _imageUri: Uri? = null
    override val imageUri: Uri?
        get() = _imageUri

    override fun save(imageUri: Uri) {
        this._imageUri = imageUri
    }
}