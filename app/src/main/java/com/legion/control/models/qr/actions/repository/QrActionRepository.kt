package com.legion.control.models.qr.actions.repository

import com.legion.control.models.qr.actions.data.wrapper.ActionStateWrapper
import com.legion.control.models.qr.info.data.ActionType
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.StateFlow

interface QrActionRepository {
    val actions: StateFlow<LoadingState<List<ActionStateWrapper>>?>
    suspend fun loadActions(actionType: ActionType, shiftId: Int?)
    suspend fun updateActions(wrapper: ActionStateWrapper)
}