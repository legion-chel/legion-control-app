package com.legion.control.models.client.cases.registration.validation.impl

import com.legion.control.models.client.cases.registration.GetAttachedPhotosUseCase
import com.legion.control.models.client.repository.ClientPhotoRepository
import javax.inject.Inject

class GetAttachedPhotosUseCaseImpl @Inject constructor(
    private val clientPhotoRepository: ClientPhotoRepository
) : GetAttachedPhotosUseCase {
    override fun invoke() = clientPhotoRepository.attachedPhotos
}