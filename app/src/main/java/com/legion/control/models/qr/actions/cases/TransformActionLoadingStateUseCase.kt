package com.legion.control.models.qr.actions.cases

import com.legion.control.components.guard.qr.goods.data.GoodsQrInfoUIState
import com.legion.control.models.qr.actions.data.state.ActionLoadingState
import com.legion.control.models.qr.info.data.goods.GoodsQrInfo
import kotlinx.coroutines.flow.StateFlow

interface TransformActionLoadingStateUseCase {
    val state: StateFlow<GoodsQrInfoUIState>

    suspend fun transform(
        loadingState: ActionLoadingState?,
        goodsInfo: GoodsQrInfo
    )
}