package com.legion.control.models.client.cases.employee.impl

import com.legion.api.http.ApiResult
import com.legion.control.models.client.cases.employee.GetEmployeeCreatesPassesUseCase
import com.legion.control.models.client.data.CreatedPass
import com.legion.control.models.client.repository.PassRepository
import com.legion.core.state.LoadingState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetEmployeeCreatedPassesUseCaseImpl @Inject constructor(
    private val repository: PassRepository
) : GetEmployeeCreatesPassesUseCase {

    private val _loadingState = MutableStateFlow<LoadingState<List<CreatedPass>>?>(null)
    override val loadingState: StateFlow<LoadingState<List<CreatedPass>>?>
        get() = _loadingState.asStateFlow()

    private val _createdPasses = MutableStateFlow(emptyList<CreatedPass>())
    override val createdPasses: StateFlow<List<CreatedPass>>
        get() = _createdPasses.asStateFlow()

    override suspend fun getCreatedPasses() {
        setState(null)

        val response = withContext(Dispatchers.IO) {
            repository.getEmployeeCreatedPasses()
        }
        val loadingState = handleApiResult(response)
        setState(loadingState)
    }

    private suspend fun handleApiResult(
        result: ApiResult<List<CreatedPass>>
    ) = withContext(Dispatchers.Default) {
        if (result is ApiResult.Success) LoadingState.Success(result.value)
        else LoadingState.Error()
    }

    suspend fun setState(loadingState: LoadingState<List<CreatedPass>>?) {
        withContext(Dispatchers.Default) {
            _loadingState.emit(loadingState)
        }
    }
}