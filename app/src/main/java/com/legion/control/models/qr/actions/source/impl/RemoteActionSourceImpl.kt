package com.legion.control.models.qr.actions.source.impl

import com.legion.control.models.qr.actions.api.ActionApi
import com.legion.control.models.qr.actions.source.RemoteActionSource
import javax.inject.Inject

class RemoteActionSourceImpl @Inject constructor(
    private val api: ActionApi
) : RemoteActionSource {

    override suspend fun getRemoteActions(qrType: String, shiftId: Int?) = api.getActions(qrType, shiftId)
}