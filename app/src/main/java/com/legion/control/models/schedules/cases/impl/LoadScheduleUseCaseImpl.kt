package com.legion.control.models.schedules.cases.impl

import com.legion.control.models.schedules.cases.LoadScheduleUseCase
import com.legion.control.models.schedules.data.ScheduleActState
import com.legion.control.models.schedules.repository.ScheduleRepository
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class LoadScheduleUseCaseImpl @Inject constructor(
	private val scheduleRepository: ScheduleRepository
) : LoadScheduleUseCase {

	override val loadingState: StateFlow<ScheduleActState?>
		get() = scheduleRepository.loadingState

	override suspend fun loadSchedule() = scheduleRepository.updateSchedule()
}