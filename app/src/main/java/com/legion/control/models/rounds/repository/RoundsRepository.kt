package com.legion.control.models.rounds.repository

import com.legion.control.models.rounds.entities.Round
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.Flow

interface RoundsRepository {
    suspend fun getRounds(scheduleId: Int?): Flow<LoadingState<Round>>
}