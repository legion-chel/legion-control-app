package com.legion.control.models.qr.actions.repository.impl

import com.legion.api.http.ApiResult
import com.legion.control.models.qr.actions.api.ActionApi
import com.legion.control.models.qr.actions.data.state.ActionState
import com.legion.control.models.qr.actions.data.wrapper.ActionStateWrapper
import com.legion.control.models.qr.actions.repository.QrActionRepository
import com.legion.control.models.qr.info.data.ActionType
import com.legion.core.state.LoadingState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class QrActionRepositoryImpl @Inject constructor(
    private val api: ActionApi
) : QrActionRepository {

    private val _actions = MutableStateFlow<LoadingState<List<ActionStateWrapper>>?>(null)
    override val actions: StateFlow<LoadingState<List<ActionStateWrapper>>?>
        get() = _actions.asStateFlow()

    override suspend fun loadActions(actionType: ActionType, shiftId: Int?) {
        setState(LoadingState.Processing)

        val apiResult = api.getActions(actionType.title, shiftId)
        val state = if (apiResult is ApiResult.Success) {
            val state = apiResult
                .value
                .map { ActionStateWrapper(it, ActionState.NOT_COMPLETE) }

            LoadingState.Success(state)
        }
        else LoadingState.Error("Ошибка загрузки данных")

        setState(state)
    }

    override suspend fun updateActions(wrapper: ActionStateWrapper) {
        val state = actions
            .value
            ?: return

        val wrappers = if (state is LoadingState.Success) state.data.toMutableList()
        else return

        val wrapperIndex = wrappers
            .indexOfFirst { it.action == wrapper.action }
            .takeIf { it >= 0 }
            ?: return

        wrappers[wrapperIndex] = wrapper

        val newLoadingState = LoadingState.Success(wrappers)
        _actions.emit(newLoadingState)
    }

    private suspend fun setState(loadingState: LoadingState<List<ActionStateWrapper>>?) {
        withContext(Dispatchers.Default) {
            _actions.emit(loadingState)
        }
    }
}