package com.legion.control.models.home.cases

interface GetHomeMenuIdUseCase {
    fun getMenuId(): Int?
}