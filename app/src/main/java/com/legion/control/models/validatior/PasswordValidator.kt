package com.legion.control.models.validatior

import com.legion.utils.InputState
import com.legion.utils.ResourceProvider
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class PasswordValidator @Inject constructor(
    resourceProvider: ResourceProvider
) : BaseValidator(resourceProvider) {

    private val _passwordState = MutableStateFlow<InputState?>(null)
    val passwordState
        get() = _passwordState.asStateFlow()

    override suspend fun validate(input: String?) {
        val inputState = getInputState(input)
        _passwordState.emit(inputState)
    }
}