package com.legion.control.models.client.cases.employee

import com.legion.control.models.client.data.EmployeePass
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.Flow

interface CreateEmployeePassUseCase {
    operator fun invoke(employeePass: EmployeePass): Flow<LoadingState<String>?>
}