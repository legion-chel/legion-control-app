package com.legion.control.models.schedules.cases.ui

import android.graphics.Bitmap
import kotlinx.coroutines.flow.StateFlow

interface GetActScheduleTextVisibilityUseCase {
    val textIsVisible: StateFlow<Boolean>
    suspend fun getTextVisibility(bitmap: Bitmap?)
}