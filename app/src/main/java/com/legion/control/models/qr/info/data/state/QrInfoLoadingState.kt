package com.legion.control.models.qr.info.data.state

sealed class QrInfoLoadingState {
    object Success : QrInfoLoadingState()
    class Error(val message: String) : QrInfoLoadingState()

    override fun equals(other: Any?) = false

    override fun hashCode() = super.hashCode()
}