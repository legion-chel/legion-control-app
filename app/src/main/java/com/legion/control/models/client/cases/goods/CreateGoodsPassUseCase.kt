package com.legion.control.models.client.cases.goods

import com.legion.control.models.client.data.GoodsPass
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.Flow

interface CreateGoodsPassUseCase {
    operator fun invoke(goodsPass: GoodsPass): Flow<LoadingState<String>?>
}