package com.legion.control.models.client.template

import com.legion.utils.InputState
import kotlinx.coroutines.flow.Flow

interface ValidateInputClientInfoTemplate {
    val firstNameInputState: Flow<InputState?>
    val secondNameInputState: Flow<InputState?>
    
    val emailInputState: Flow<InputState?>
    
    val documentSerialInputState: Flow<InputState?>
    val documentNumberInputState: Flow<InputState?>
    
    val buttonEnabled: Flow<Boolean>
    
    suspend fun validateFirstName(firstName: String)
    suspend fun validateSecondName(secondName: String)
    suspend fun validateEmail(email: String)
    
    suspend fun validateDocumentSerial(serial: String)
    suspend fun validateDocumentNumber(number: String)
}