package com.legion.control.models.upload.cases.impl

import android.graphics.Bitmap
import android.net.Uri
import com.legion.control.models.upload.cases.CreateBitmapUseCase
import com.legion.utils.BitmapBuilder
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class CreateBitmapUseCaseImpl @Inject constructor(
    private val bitmapBuilder: BitmapBuilder
) : CreateBitmapUseCase {

    private val _bitmap = MutableStateFlow<Bitmap?>(null)
    override val bitmap: StateFlow<Bitmap?>
        get() = _bitmap

    override suspend fun create(uri: Uri) {
        val bitmap = bitmapBuilder
            .buildFromUri(uri, -90f)
        _bitmap.emit(bitmap)
    }
}