package com.legion.control.models.validatior

import com.legion.utils.InputState
import com.legion.utils.ResourceProvider
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class LoginValidator @Inject constructor(
    resourceProvider: ResourceProvider
) : BaseValidator(resourceProvider) {

    private val _loginState = MutableStateFlow<InputState?>(null)
    val loginState
        get() = _loginState.asStateFlow()

    override suspend fun validate(input: String?) {
        val inputState = getInputState(input)
        _loginState.emit(inputState)
    }

}