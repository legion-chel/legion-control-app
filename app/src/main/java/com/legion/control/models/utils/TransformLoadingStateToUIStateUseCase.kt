package com.legion.control.models.utils

import com.legion.core.state.LoadingState
import com.legion.core.state.UIState

interface TransformLoadingStateToUIStateUseCase {
    operator fun <T> invoke(loadingState: LoadingState<T>?): UIState<T>?
}