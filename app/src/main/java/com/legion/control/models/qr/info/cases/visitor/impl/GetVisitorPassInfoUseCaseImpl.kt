package com.legion.control.models.qr.info.cases.visitor.impl

import com.legion.control.models.qr.info.cases.visitor.GetVisitorPassInfoUseCase
import com.legion.control.models.qr.info.data.visitor.VisitorQrInfo
import com.legion.utils.ResourceProvider
import com.legion.utils.extensions.empty
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class GetVisitorPassInfoUseCaseImpl @Inject constructor(
    private val resourceProvider: ResourceProvider
) : GetVisitorPassInfoUseCase {

    private val _fullName = MutableStateFlow(String.empty)
    override val fullName: StateFlow<String>
        get() = _fullName

    private val _isEnabledPass = MutableStateFlow(String.empty)
    override val isEnabledPass: StateFlow<String>
        get() = _isEnabledPass

    override suspend fun getPassInfo(qrInfo: VisitorQrInfo) {
        val (firstName, secondName, patronymic) = qrInfo.visitorInfo
        val nameString = "$secondName $firstName $patronymic"

        _fullName.emit(nameString)

    }
}