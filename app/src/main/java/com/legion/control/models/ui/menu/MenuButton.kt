package com.legion.control.models.ui.menu

enum class ButtonType {
    SCAN_QR,
    DETOUR,
    CREATE_INCIDENT,
    TASKS,

    CREATE_GOODS_PASS,
    CREATE_EMPLOYEE_PASS,
    CREATE_VISITOR_PASS
}

data class MenuButton(
    val type: ButtonType,
    val text: String
)