package com.legion.control.models.qr.actions.data.state

import com.legion.control.models.qr.actions.data.wrapper.ActionStateWrapper

sealed class ActionLoadingState {
    class Success(val wrappers: List<ActionStateWrapper>) : ActionLoadingState()
    class Error(val message: String) : ActionLoadingState()
}