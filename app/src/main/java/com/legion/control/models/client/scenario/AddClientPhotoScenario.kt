package com.legion.control.models.client.scenario

import android.graphics.Bitmap
import android.net.Uri
import kotlinx.coroutines.flow.Flow

interface AddClientPhotoScenario {
    val attachedPhotos: Flow<List<Bitmap>>
    
    suspend fun addDocumentPhoto(uri: Uri, bitmap: Bitmap?)
    suspend fun addAvatar(uri: Uri, bitmap: Bitmap?)
    
    fun getDocumentsUrl(): List<Uri>
    fun getAvatarUri(): Uri?
}