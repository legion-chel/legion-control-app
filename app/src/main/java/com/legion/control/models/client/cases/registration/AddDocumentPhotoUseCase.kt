package com.legion.control.models.client.cases.registration

import android.graphics.Bitmap
import android.net.Uri

interface AddDocumentPhotoUseCase {
    suspend fun add(documentPhotoUri: Uri, documentPhotoBitmap: Bitmap?)
    fun getPhotosUri(): List<Uri>
}