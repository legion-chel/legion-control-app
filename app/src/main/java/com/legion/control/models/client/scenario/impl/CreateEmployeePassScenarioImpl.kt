package com.legion.control.models.client.scenario.impl

import android.graphics.Bitmap
import android.net.Uri
import com.legion.control.models.client.cases.AppendPhotoForPassUseCase
import com.legion.control.models.client.cases.employee.CreateEmployeePassUseCase
import com.legion.control.models.client.data.EmployeePass
import com.legion.control.models.client.scenario.CreateEmployeePassScenario
import com.legion.control.models.utils.TransformLoadingStateToUIStateUseCase
import com.legion.core.state.UIState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.transform
import javax.inject.Inject

class CreateEmployeePassScenarioImpl @Inject constructor(
    private val createEmployeePassUseCase: CreateEmployeePassUseCase,
    private val appendPhotoForPassUseCase: AppendPhotoForPassUseCase,
    private val transformLoadingStateToUIStateUseCase: TransformLoadingStateToUIStateUseCase
) : CreateEmployeePassScenario {

    override val appendedPhotos: Flow<List<Bitmap>>
        get() = appendPhotoForPassUseCase.appendedPhotos

    override suspend fun appendPhoto(uri: Uri, bitmap: Bitmap?) {
        appendPhotoForPassUseCase.append(uri, bitmap)
    }

    override suspend fun create(
        secondName: String,
        firstName: String,
        patronymic: String
    ): Flow<UIState<String>> {
        val appendedPhotos = appendPhotoForPassUseCase.appendedPhotoUri
        val pass = EmployeePass(firstName, secondName, patronymic, appendedPhotos)

        return createEmployeePassUseCase(pass)
            .transform {
                val state = transformLoadingStateToUIStateUseCase(it) ?: return@transform
                emit(state)
            }
    }
}