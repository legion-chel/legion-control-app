package com.legion.control.models.qr.info.cases.visitor

import com.legion.control.models.qr.info.data.QrContent
import com.legion.control.models.qr.info.data.visitor.VisitorQrInfoUIWrapper
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.Flow

interface GetVisitorQrInfoUIWrapperUseCase {
    suspend operator fun invoke(qrContent: QrContent, scheduleId: Int?): Flow<LoadingState<VisitorQrInfoUIWrapper>?>
}