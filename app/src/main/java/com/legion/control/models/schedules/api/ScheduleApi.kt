package com.legion.control.models.schedules.api

import com.legion.api.http.ApiResult
import com.legion.control.models.schedules.data.Schedule
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ScheduleApi {
    @GET("/schedules/nearest")
    suspend fun getCurrentSchedule(): ApiResult<Schedule>

    @POST("/schedules/{id}/start")
    suspend fun startSchedule(@Path("id") shiftId: Int, @Body photoId: Int): ApiResult<Schedule>

    @POST("/schedules/{id}/stop")
    suspend fun finishShift(@Path("id") shiftId: Int, @Body photoId: Int): ApiResult<Schedule>
}