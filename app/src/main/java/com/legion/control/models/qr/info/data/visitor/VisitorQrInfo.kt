package com.legion.control.models.qr.info.data.visitor

import com.legion.control.models.qr.info.data.ActionType

class VisitorQrInfo(
    val qrId: Int,
    val actionType: ActionType,
    val visitorInfo: VisitorInfo
) {
    override fun equals(other: Any?) = false

    override fun hashCode(): Int {
        var result = qrId
        result = 31 * result + actionType.hashCode()
        result = 31 * result + visitorInfo.hashCode()
        return result
    }
}