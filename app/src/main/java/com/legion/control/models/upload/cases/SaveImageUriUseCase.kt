package com.legion.control.models.upload.cases

import android.net.Uri

interface SaveImageUriUseCase {
    val imageUri: Uri?
    fun save(imageUri: Uri)
}