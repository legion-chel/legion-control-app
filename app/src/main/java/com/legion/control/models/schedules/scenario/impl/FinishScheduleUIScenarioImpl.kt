package com.legion.control.models.schedules.scenario.impl

import android.graphics.Bitmap
import com.legion.control.models.schedules.cases.ui.GetActScheduleButtonEnabledUseCase
import com.legion.control.models.schedules.cases.ui.GetActScheduleTextVisibilityUseCase
import com.legion.control.models.schedules.cases.ui.GetFinishScheduleButtonTitleUseCase
import com.legion.control.models.schedules.scenario.FinishScheduleUIScenario
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class FinishScheduleUIScenarioImpl @Inject constructor(
    private val getFinishScheduleButtonTitleUseCase: GetFinishScheduleButtonTitleUseCase,
    private val getActScheduleButtonEnabledUseCase: GetActScheduleButtonEnabledUseCase,
    private val getActScheduleTextVisibilityUseCase: GetActScheduleTextVisibilityUseCase
) : FinishScheduleUIScenario {

    override val buttonTitle: StateFlow<String>
        get() = getFinishScheduleButtonTitleUseCase.title

    override val buttonIsEnabled: StateFlow<Boolean>
        get() = getActScheduleButtonEnabledUseCase.isEnabled

    override val textIsVisible: StateFlow<Boolean>
        get() = getActScheduleTextVisibilityUseCase.textIsVisible

    override suspend fun getButtonTitle(bitmap: Bitmap?) {
       getFinishScheduleButtonTitleUseCase.getButtonTitle(bitmap)
    }

    override suspend fun getTextVisibility(bitmap: Bitmap?) {
        getActScheduleTextVisibilityUseCase.getTextVisibility(bitmap)
    }

    override suspend fun getButtonEnabled(isLoading: Boolean) {
        getActScheduleButtonEnabledUseCase.getEnabled(isLoading)
    }
}