package com.legion.control.models.schedules.data

sealed class ShiftState {
    class Success(val isSuccess: Boolean) : ShiftState()
    class Failure(val message: String) : ShiftState()
}