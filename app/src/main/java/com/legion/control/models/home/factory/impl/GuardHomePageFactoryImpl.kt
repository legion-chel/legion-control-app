package com.legion.control.models.home.factory.impl

import com.legion.control.components.guard.main.GuardMainFragment
import com.legion.control.components.guard.schedules.main.ShiftFragment
import com.legion.control.components.profile.ProfileFragment
import com.legion.control.models.home.factory.GuardHomePageFactory
import javax.inject.Inject

class GuardHomePageFactoryImpl @Inject constructor(
    private val mainFragment: GuardMainFragment,
    private val shiftFragment: ShiftFragment,
    private val profileFragment: ProfileFragment,
) : GuardHomePageFactory {

    override fun create() = listOf(
        mainFragment,
        shiftFragment,
        profileFragment
    )
}