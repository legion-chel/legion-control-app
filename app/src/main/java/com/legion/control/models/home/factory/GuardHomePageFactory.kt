package com.legion.control.models.home.factory

import androidx.fragment.app.Fragment
import com.legion.control.models.ui.factory.PageFactory

interface GuardHomePageFactory : PageFactory<Fragment>