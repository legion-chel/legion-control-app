package com.legion.control.models.schedules.data.ui

class ScheduleActButtonWrapper(
    val title: String,
    val isEnabled: Boolean
)