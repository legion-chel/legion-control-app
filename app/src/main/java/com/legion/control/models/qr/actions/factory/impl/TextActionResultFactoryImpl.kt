package com.legion.control.models.qr.actions.factory.impl

import com.legion.control.models.qr.actions.builder.ActionResultBuilder
import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.actions.data.result.QrActionResult
import com.legion.control.models.qr.actions.factory.TextActionResultFactory
import com.legion.control.models.qr.info.data.ActionType
import com.legion.utils.getCurrentTimeUnix
import javax.inject.Inject

class TextActionResultFactoryImpl @Inject constructor() : TextActionResultFactory {
    override fun create(
        qrId: Int,
        actionType: ActionType,
        actions: Map<Action, String>
    ): List<QrActionResult> {
        val textResults = mutableListOf<QrActionResult>()

        actions.forEach { (action, text) ->
            val result = ActionResultBuilder()
                .setResult(text)
                .setQrId(qrId)
                .setQrType(actionType)
                .setInsertDate(getCurrentTimeUnix())
                .build(action)

            textResults.add(result)
        }

        return textResults
    }
}