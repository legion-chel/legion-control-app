package com.legion.control.models.schedules.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.legion.control.models.schedules.data.Schedule
import kotlinx.coroutines.flow.Flow

@Dao
interface ScheduleDao {
    @Query("SELECT * FROM Schedule LIMIT 1")
    fun getCurrentSchedule(): Flow<Schedule>

    @Query("SELECT * FROM Schedule LIMIT 1")
    suspend fun syncGetCurrentSchedule(): Schedule?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(schedule: Schedule)
    
    @Query("DELETE FROM Schedule")
    suspend fun deleteAll()
}