package com.legion.control.models.schedules.scenario.impl

import android.graphics.Bitmap
import com.legion.control.models.schedules.cases.ui.GetActScheduleButtonEnabledUseCase
import com.legion.control.models.schedules.cases.ui.GetActScheduleTextVisibilityUseCase
import com.legion.control.models.schedules.cases.ui.GetStartScheduleButtonTitleUseCase
import com.legion.control.models.schedules.scenario.StartScheduleUIScenario
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class StartScheduleUIScenarioImpl @Inject constructor(
    private val getStartScheduleButtonTitleUseCase: GetStartScheduleButtonTitleUseCase,
    private val getActScheduleButtonEnabledUseCase: GetActScheduleButtonEnabledUseCase,
    private val getActScheduleTextVisibility: GetActScheduleTextVisibilityUseCase
) : StartScheduleUIScenario {

    override val buttonTitle: StateFlow<String>
        get() = getStartScheduleButtonTitleUseCase.title

    override val buttonIsEnabled: StateFlow<Boolean>
        get() = getActScheduleButtonEnabledUseCase.isEnabled

    override val textIsVisible: StateFlow<Boolean>
        get() = getActScheduleTextVisibility.textIsVisible

    override suspend fun getButtonTitle(bitmap: Bitmap?) {
        getStartScheduleButtonTitleUseCase.getButtonTitle(bitmap)
    }

    override suspend fun getTextVisibility(bitmap: Bitmap?) {
        getActScheduleTextVisibility.getTextVisibility(bitmap)
    }

    override suspend fun getButtonEnabled(isLoading: Boolean) {
        getActScheduleButtonEnabledUseCase.getEnabled(isLoading)
    }

}