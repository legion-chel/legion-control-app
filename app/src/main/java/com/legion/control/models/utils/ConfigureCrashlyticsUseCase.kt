package com.legion.control.models.utils

interface ConfigureCrashlyticsUseCase {
    operator fun invoke()
}