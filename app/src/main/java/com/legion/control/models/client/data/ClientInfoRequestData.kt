package com.legion.control.models.client.data

import com.google.gson.annotations.SerializedName
import okhttp3.MultipartBody
import okhttp3.RequestBody

class ClientInfoRequestData(
    @SerializedName("first_name")
    val firstName: String,
    
    @SerializedName("last_name")
    val secondName: String,
    
    @SerializedName("date_of_birth")
    val birthDate: String,
    
    @SerializedName("passport_data")
    val passportData: RequestBody,
    
    @SerializedName("avatar")
    val avatar: MultipartBody.Part,
    @SerializedName("documents")
    val documents: List<MultipartBody.Part>
)