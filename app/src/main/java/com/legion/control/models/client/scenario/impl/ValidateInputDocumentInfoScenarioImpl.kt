package com.legion.control.models.client.scenario.impl

import com.legion.control.models.client.cases.registration.validation.ValidateInputDocumentNumberUseCase
import com.legion.control.models.client.cases.registration.validation.ValidateInputDocumentSerialUseCase
import com.legion.control.models.client.scenario.ValidateInputDocumentInfoScenario
import com.legion.utils.InputState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ValidateInputDocumentInfoScenarioImpl @Inject constructor(
    private val validateInputDocumentSerialUseCase: ValidateInputDocumentSerialUseCase,
    private val validateInputDocumentNumberUseCase: ValidateInputDocumentNumberUseCase
) : ValidateInputDocumentInfoScenario {
    override val serialInputState: Flow<InputState?>
        get() = validateInputDocumentSerialUseCase.inputState
    
    override val numberInputState: Flow<InputState?>
        get() = validateInputDocumentNumberUseCase.inputState
    
    override suspend fun validateSerial(serial: String) =
        validateInputDocumentSerialUseCase.validate(serial)
    
    override suspend fun validateNumber(number: String) =
        validateInputDocumentNumberUseCase.validate(number)
}