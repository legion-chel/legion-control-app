package com.legion.control.models.client.data

import android.net.Uri
import com.legion.control.models.qr.info.data.goods.GoodsDirection

class GoodsPass(
    val description: String,
    val count: String,
    val direction: GoodsDirection,
    val photos: List<Uri>
)