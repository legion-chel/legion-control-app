package com.legion.control.models.client.cases.registration.validation.impl

import com.legion.control.R
import com.legion.control.models.client.cases.registration.validation.ValidateInputClientSecondNameUseCase
import com.legion.utils.InputState
import com.legion.utils.ResourceProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ValidateInputClientSecondNameUseCaseImpl @Inject constructor(
    private val resourceProvider: ResourceProvider
) : ValidateInputClientSecondNameUseCase {
    
    private val _inputState = MutableStateFlow<InputState?>(null)
    override val inputState: Flow<InputState?>
        get() = _inputState
    
    override suspend fun validate(secondName: String) {
        withContext(Dispatchers.IO) {
            withContext(Dispatchers.IO) {
                val state = if (secondName.isEmpty()) {
                    val errMessage = resourceProvider.getString(R.string.error_empty_input)
                    InputState.Incorrect(errMessage)
                } else InputState.Correct
            
                _inputState.emit(state)
            }
        }
    }
}