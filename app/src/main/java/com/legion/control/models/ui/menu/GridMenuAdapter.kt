package com.legion.control.models.ui.menu

import android.view.LayoutInflater
import android.view.ViewGroup
import com.legion.control.databinding.ItemMenuGridBinding
import com.legion.control.models.ui.adapter.AppAdapter

class GridMenuAdapter(
    buttonFactory: FactoryItems<MenuButton>,
    clickListener: ItemClickListener<MenuButton>
) : AppAdapter<MenuButton>(buttonFactory, clickListener) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GridMenuHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemMenuGridBinding.inflate(inflater, parent, false)

        return GridMenuHolder(binding)
    }

    class GridMenuHolder(
        private val binding: ItemMenuGridBinding
    ) : AppHolder<MenuButton>(binding.root) {

        override fun bind(item: MenuButton, itemClickListener: ItemClickListener<MenuButton>) {
            binding.menuItem.apply {
                text = item.text
                setOnClickListener { itemClickListener.onClick(item) }
            }
        }
    }
}