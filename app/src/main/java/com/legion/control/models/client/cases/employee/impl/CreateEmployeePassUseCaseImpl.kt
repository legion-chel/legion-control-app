package com.legion.control.models.client.cases.employee.impl

import com.legion.control.R
import com.legion.control.models.client.cases.employee.CreateEmployeePassUseCase
import com.legion.control.models.client.data.EmployeePass
import com.legion.control.models.client.repository.PassRepository
import com.legion.core.state.LoadingState
import com.legion.utils.ResourceProvider
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class CreateEmployeePassUseCaseImpl @Inject constructor(
    private val repository: PassRepository,
    private val resourceProvider: ResourceProvider
) : CreateEmployeePassUseCase {

    override fun invoke(employeePass: EmployeePass): Flow<LoadingState<String>?> = flow {
        if (employeePass.photos.isEmpty()) {
            val messageError = resourceProvider.getString(R.string.need_add_photo_for_pass)
            val error = LoadingState.Error(messageError)
            emit(error)
            return@flow
        }

        emit(LoadingState.Processing)

        val result = repository.createEmployeePass(employeePass)
        result.handleResult(
            successDelegate = {
                val message = resourceProvider.getString(R.string.pass_created)
                emit(LoadingState.Success(message))
            },
            httpErrorDelegate = {
                val message = resourceProvider.getString(R.string.network_error)
                emit(LoadingState.Error(message))
            },
            errorDelegate = {
                val message = resourceProvider.getString(R.string.error_on_create_pass)
                emit(LoadingState.Error(message))
            }
        )
    }
}