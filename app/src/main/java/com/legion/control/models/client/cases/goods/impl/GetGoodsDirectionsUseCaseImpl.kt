package com.legion.control.models.client.cases.goods.impl

import com.legion.control.R
import com.legion.control.models.client.cases.goods.GetGoodsDirectionsUseCase
import com.legion.utils.ResourceProvider
import javax.inject.Inject

class GetGoodsDirectionsUseCaseImpl @Inject constructor(
    private val resourceProvider: ResourceProvider
) : GetGoodsDirectionsUseCase {

    override fun invoke() = resourceProvider
        .getStringArray(R.array.goods_directions)
        .toList()
}