package com.legion.control.models.client.scenario.impl

import com.legion.control.models.client.cases.registration.validation.ValidateInputClientFirstNameUseCase
import com.legion.control.models.client.cases.registration.validation.ValidateInputClientSecondNameUseCase
import com.legion.control.models.client.scenario.ValidateInputClientNameScenario
import com.legion.utils.InputState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ValidateInputClientNameScenarioImpl @Inject constructor(
    private val validateInputClientFirstNameUseCase: ValidateInputClientFirstNameUseCase,
    private val validateInputClientSecondNameUseCase: ValidateInputClientSecondNameUseCase,
) : ValidateInputClientNameScenario {
    override val firstNameInputState: Flow<InputState?>
        get() = validateInputClientFirstNameUseCase.inputState
    
    override val secondNameInputState: Flow<InputState?>
        get() = validateInputClientSecondNameUseCase.inputState
    
    override suspend fun validateFirstName(firstName: String) =
        validateInputClientFirstNameUseCase.validate(firstName)
    
    override suspend fun validateSecondName(secondName: String) =
        validateInputClientSecondNameUseCase.validate(secondName)
}