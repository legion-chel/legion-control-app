package com.legion.control.models.upload.scenario.impl

import android.graphics.Bitmap
import android.net.Uri
import com.legion.control.models.upload.cases.CreateBitmapUseCase
import com.legion.control.models.upload.cases.SaveImageUriUseCase
import com.legion.control.models.upload.cases.UploadImageUseCase
import com.legion.control.models.upload.data.UploadImageState
import com.legion.control.models.upload.data.UploadImageType
import com.legion.control.models.upload.scenario.UploadImageScenario
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class UploadImageScenarioImpl @Inject constructor(
    private val saveImageUriUseCase: SaveImageUriUseCase,
    private val uploadImageUseCase: UploadImageUseCase,
    private val createBitmapUseCase: CreateBitmapUseCase
) : UploadImageScenario {

    override val uploadImageState: StateFlow<UploadImageState?>
        get() = uploadImageUseCase.uploadImageState

    override val bitmap: StateFlow<Bitmap?>
        get() = createBitmapUseCase.bitmap

    override suspend fun saveImage(imageUri: Uri) {
        saveImageUriUseCase.save(imageUri)
        createBitmapUseCase.create(imageUri)
    }

    override suspend fun uploadImage(imageType: UploadImageType) {
        val imageUri = saveImageUriUseCase.imageUri
        uploadImageUseCase.uploadImage(imageUri, imageType)
    }
}