package com.legion.control.models.client.scenario.impl

import com.legion.control.models.client.cases.goods.GetGoodsDirectionsByNumberUseCase
import com.legion.control.models.client.cases.goods.GetGoodsDirectionsUseCase
import com.legion.control.models.client.scenario.GetGoodsDirectionsScenario
import com.legion.control.models.qr.info.data.goods.GoodsDirection
import javax.inject.Inject

class GetGoodsDirectionsScenarioImpl @Inject constructor(
    private val getGoodsDirectionsUseCase: GetGoodsDirectionsUseCase,
    private val getGoodsDirectionsByNumberUseCase: GetGoodsDirectionsByNumberUseCase
) : GetGoodsDirectionsScenario {

    override fun getGoodsDirections() = getGoodsDirectionsUseCase()

    override suspend fun getDirectionByNumber(number: Int): GoodsDirection {
        return getGoodsDirectionsByNumberUseCase(number)
    }
}