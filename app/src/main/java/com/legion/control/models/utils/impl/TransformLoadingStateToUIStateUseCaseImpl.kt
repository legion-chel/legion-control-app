package com.legion.control.models.utils.impl

import com.legion.control.models.utils.TransformLoadingStateToUIStateUseCase
import com.legion.core.state.LoadingState
import com.legion.core.state.UIState
import javax.inject.Inject

class TransformLoadingStateToUIStateUseCaseImpl @Inject constructor() : TransformLoadingStateToUIStateUseCase {
    override fun <T> invoke(loadingState: LoadingState<T>?): UIState<T>? {
        return when(loadingState) {
            is LoadingState.Processing -> UIState.Loading
            is LoadingState.Success -> UIState.Success(loadingState.data)
            is LoadingState.Error -> UIState.Error(loadingState.error)
            else -> null
        }
    }
}