package com.legion.control.models.schedules.data

import com.google.gson.annotations.SerializedName

enum class ScheduleStatus {
    @SerializedName("active")
    ACTIVE,

    @SerializedName("planned")
    PLANNED,

    @SerializedName("ended")
    ENDED
}