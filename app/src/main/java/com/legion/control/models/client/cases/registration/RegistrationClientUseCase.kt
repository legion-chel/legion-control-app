package com.legion.control.models.client.cases.registration

import com.legion.control.models.client.data.ClientInfo
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.Flow

interface RegistrationClientUseCase {
    suspend operator fun invoke(clientInfo: ClientInfo): Flow<LoadingState<String>?>
}