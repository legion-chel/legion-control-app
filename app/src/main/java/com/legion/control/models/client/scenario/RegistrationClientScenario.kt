package com.legion.control.models.client.scenario

import com.legion.control.models.client.data.ClientInfo
import com.legion.core.state.UIState
import kotlinx.coroutines.flow.Flow

interface RegistrationClientScenario {
    suspend fun registrationClient(clientInfo: ClientInfo): Flow<UIState<String>>
}