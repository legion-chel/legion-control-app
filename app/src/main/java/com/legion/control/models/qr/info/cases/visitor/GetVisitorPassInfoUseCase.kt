package com.legion.control.models.qr.info.cases.visitor

import com.legion.control.models.qr.info.data.visitor.VisitorQrInfo
import kotlinx.coroutines.flow.StateFlow

interface GetVisitorPassInfoUseCase {
    val fullName: StateFlow<String>
    val isEnabledPass: StateFlow<String>
    suspend fun getPassInfo(qrInfo: VisitorQrInfo)
}