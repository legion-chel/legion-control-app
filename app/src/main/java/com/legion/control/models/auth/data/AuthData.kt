package com.legion.control.models.auth.data

import com.google.gson.annotations.SerializedName

data class AuthData(
    @SerializedName("username")
    val login: String,

    @SerializedName("password")
    val password: String
)