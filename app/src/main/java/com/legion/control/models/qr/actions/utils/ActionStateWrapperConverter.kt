package com.legion.control.models.qr.actions.utils

import com.legion.control.components.guard.qr.adapter.data.ActionUIWrapper
import com.legion.control.models.qr.actions.data.wrapper.ActionStateWrapper

interface ActionStateWrapperConverter {
    fun convert(wrapper: ActionStateWrapper): ActionUIWrapper
}