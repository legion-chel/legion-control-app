package com.legion.control.models.sync.usecases.impl

import com.legion.control.components.splash.SplashFragmentDirections
import com.legion.control.models.sync.data.SyncState
import com.legion.control.models.sync.usecases.NavigateBySyncStateUseCase
import com.legion.control.navigation.Navigator
import javax.inject.Inject

class NavigateBySyncStateUseCaseImpl @Inject constructor(
    private val navigator: Navigator
) : NavigateBySyncStateUseCase {
    
    override fun invoke(syncState: SyncState) {
        val direction = when(syncState) {
            SyncState.DONE -> SplashFragmentDirections.toHome()
            SyncState.NEED_AUTH -> SplashFragmentDirections.toLoginFromSplash()
            SyncState.NEED_ENTER_INFO -> SplashFragmentDirections.toRegistration()
            else -> return
        }
        
        navigator.navigate(direction)
    }
}