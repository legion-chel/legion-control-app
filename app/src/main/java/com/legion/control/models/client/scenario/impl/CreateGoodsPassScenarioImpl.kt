package com.legion.control.models.client.scenario.impl

import android.graphics.Bitmap
import android.net.Uri
import com.legion.control.models.client.cases.AppendPhotoForPassUseCase
import com.legion.control.models.client.cases.goods.CreateGoodsPassUseCase
import com.legion.control.models.client.data.GoodsPass
import com.legion.control.models.client.scenario.CreateGoodsPassScenario
import com.legion.control.models.qr.info.data.goods.GoodsDirection
import com.legion.control.models.utils.TransformLoadingStateToUIStateUseCase
import com.legion.core.state.UIState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.transform
import javax.inject.Inject

class CreateGoodsPassScenarioImpl @Inject constructor(
    private val createGoodsPassUseCase: CreateGoodsPassUseCase,
    private val appendPhotoForPassUseCase: AppendPhotoForPassUseCase,
    private val transformLoadingStateToUIStateUseCase: TransformLoadingStateToUIStateUseCase
) : CreateGoodsPassScenario {

    override val appendedPhotos: Flow<List<Bitmap>>
        get() = appendPhotoForPassUseCase.appendedPhotos

    override suspend fun create(
        description: String,
        count: String,
        direction: GoodsDirection
    ): Flow<UIState<String>> {
        val photos = appendPhotoForPassUseCase.appendedPhotoUri
        val goodsPass = GoodsPass(description, count, direction, photos)

        return createGoodsPassUseCase(goodsPass)
            .transform {
                val state = transformLoadingStateToUIStateUseCase(it) ?: return@transform
                emit(state)
            }
    }

    override suspend fun appendPhoto(uri: Uri, bitmap: Bitmap?) {
        appendPhotoForPassUseCase.append(uri, bitmap)
    }
}