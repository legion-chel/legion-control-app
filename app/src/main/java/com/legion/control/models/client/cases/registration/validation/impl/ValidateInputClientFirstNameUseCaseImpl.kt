package com.legion.control.models.client.cases.registration.validation.impl

import com.legion.control.R
import com.legion.control.models.client.cases.registration.validation.ValidateInputClientFirstNameUseCase
import com.legion.utils.InputState
import com.legion.utils.ResourceProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ValidateInputClientFirstNameUseCaseImpl @Inject constructor(
    private val resourceProvider: ResourceProvider
) : ValidateInputClientFirstNameUseCase {
    
    private val _inputState = MutableStateFlow<InputState?>(null)
    override val inputState: Flow<InputState?>
        get() = _inputState
    
    override suspend fun validate(firstName: String) {
        withContext(Dispatchers.IO) {
            val state = if (firstName.isEmpty()) {
                val errMessage = resourceProvider.getString(R.string.error_empty_input)
                InputState.Incorrect(errMessage)
            } else InputState.Correct
    
            _inputState.emit(state)
        }
    }
}