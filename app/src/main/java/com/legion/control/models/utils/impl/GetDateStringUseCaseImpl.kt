package com.legion.control.models.utils.impl

import com.legion.control.models.utils.GetDateStringUseCase
import com.legion.utils.SEPARATED_DATE_PATTERN
import com.legion.utils.toString
import java.util.*
import javax.inject.Inject

class GetDateStringUseCaseImpl @Inject constructor() : GetDateStringUseCase {
    override fun invoke(time: Long) = Date(time).toString(SEPARATED_DATE_PATTERN)
}