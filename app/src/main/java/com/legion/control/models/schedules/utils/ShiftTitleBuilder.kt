package com.legion.control.models.schedules.utils

import com.legion.control.R
import com.legion.control.models.schedules.data.ScheduleStatus
import com.legion.utils.ResourceProvider
import com.legion.utils.extensions.empty
import javax.inject.Inject

class ShiftTitleBuilder @Inject constructor(
    private val resourceProvider: ResourceProvider
) {

    fun build(status: ScheduleStatus): String {
        val stringId = when (status) {
            ScheduleStatus.ACTIVE -> R.string.label_shift_active
            ScheduleStatus.PLANNED -> R.string.label_shift_planned
            else -> return String.empty
        }

        return resourceProvider.getString(stringId)
    }
}