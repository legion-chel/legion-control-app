package com.legion.control.models.schedules.repository.impl

import com.legion.api.http.ApiResult
import com.legion.control.models.schedules.api.ScheduleApi
import com.legion.control.models.schedules.data.Schedule
import com.legion.control.models.schedules.data.ScheduleActState
import com.legion.control.models.schedules.db.ScheduleDao
import com.legion.control.models.schedules.repository.ScheduleRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class ScheduleRepositoryImpl @Inject constructor(
    private val api: ScheduleApi,
    private val dao: ScheduleDao
) : ScheduleRepository {

    private val _loadingState = MutableStateFlow<ScheduleActState?>(null)
    override val loadingState: StateFlow<ScheduleActState?>
        get() = _loadingState

    private val _startState = MutableStateFlow<ScheduleActState?>(null)
    override val startState: StateFlow<ScheduleActState?>
        get() = _startState

    private val _finishState = MutableStateFlow<ScheduleActState?>(null)
    override val finishState: StateFlow<ScheduleActState?>
        get() = _finishState

    override fun getCurrentSchedule() = dao.getCurrentSchedule()

    override suspend fun syncGetCurrentSchedule() = dao.syncGetCurrentSchedule()

    override suspend fun updateSchedule() {
        val apiResult = api.getCurrentSchedule()
        val loadingState = handleScheduleApiResult(apiResult)

        _loadingState.emit(loadingState)
    }

    override suspend fun startSchedule(scheduleId: Int?, photoId: Int?) {
        if (scheduleId == null) {
            val error = ScheduleActState.Error.WithMessage("Идентификатор смены не найден")
            _finishState.emit(error)

            return
        }

        if (photoId == null) {
            val error = ScheduleActState.Error.WithMessage("Пустой путь до файла")
            _finishState.emit(error)

            return
        }

        val apiResult = api.startSchedule(scheduleId, photoId)
        val state = handleActScheduleApiResult("Смена успешно начата", apiResult)

        _startState.emit(state)
    }

    override suspend fun finishSchedule(scheduleId: Int?, photoId: Int?) {
        if (scheduleId == null) {
            val error = ScheduleActState.Error.WithMessage("Идентификатор смены не найден")
            _finishState.emit(error)

            return
        }

        if (photoId == null) {
            val error = ScheduleActState.Error.WithMessage("Пустой путь до файла")
            _finishState.emit(error)

            return
        }

        val apiResult = api.finishShift(scheduleId, photoId)
        if (apiResult is ApiResult.Success) dao.deleteAll()
        val state = handleActScheduleApiResult("Смена успешно завершена", apiResult)

        _finishState.emit(state)
    }

    private suspend fun handleActScheduleApiResult(
        successMessage: String,
        apiResult: ApiResult<Schedule>
    ): ScheduleActState {
        return when(apiResult) {
            is ApiResult.Success -> {
                val schedule = apiResult.value
                dao.save(schedule)

                ScheduleActState.Success.WithMessage(successMessage)
            }

            is ApiResult.Failure.HttpError ->  {
                val errMessage = apiResult.statusMessage ?: "Неизвестная ошибка"
                ScheduleActState.Error.WithMessage(errMessage)
            }

            else -> ScheduleActState.Error.WithMessage("Неизвестная ошибка")
        }
    }

    private suspend fun handleScheduleApiResult(apiResult: ApiResult<Schedule>): ScheduleActState {
        return when(apiResult) {
            is ApiResult.Success -> {
                val schedule = apiResult.value
                dao.save(schedule)

                ScheduleActState.Success()
            }

            is ApiResult.Failure<*> -> ScheduleActState.Error()
        }
    }
}