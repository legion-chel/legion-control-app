package com.legion.control.models.qr.info.data

import com.google.gson.annotations.SerializedName

enum class ActionType(val title: String) {
    @SerializedName("qr_goods")
    GOODS("qr_goods"),

    @SerializedName("qr_visitor_pass")
    VISITOR_PASS("qr_visitor_pass"),

    @SerializedName("qr_employee_pass")
    EMPLOYEE_PASS("qr_employee_pass"),

    @SerializedName("qr_transport")
    TRANSPORT("qr_transport"),
    
    @SerializedName("round")
    ROUNDS("round")
}