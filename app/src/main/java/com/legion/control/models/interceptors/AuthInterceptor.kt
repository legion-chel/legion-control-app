package com.legion.control.models.interceptors

import com.legion.core.session.UserSessionFacade
import com.legion.utils.log.Logger
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(
    private val userSessionFacade: UserSessionFacade
): Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain
            .request()
            .newBuilder()

        val token = "$TOKEN_PREFIX ${userSessionFacade.token}"

        requestBuilder
            .addHeader(HEADER_TOKEN_KEY, token)

        Logger.d("Auth token: $token")

        return chain.proceed(requestBuilder.build())
    }

    companion object {
        private const val HEADER_TOKEN_KEY = "authorization"
        private const val TOKEN_PREFIX = "Bearer"
    }
}