package com.legion.control.models.sync.usecases.impl

import com.legion.control.models.sync.data.SyncState
import com.legion.control.models.sync.usecases.GetLoadingStateBySyncStateUseCase
import javax.inject.Inject

class GetLoadingStateBySyncStateUseCaseImpl @Inject constructor() : GetLoadingStateBySyncStateUseCase {
    override fun invoke(syncState: SyncState) = syncState == SyncState.PROCESSING
}