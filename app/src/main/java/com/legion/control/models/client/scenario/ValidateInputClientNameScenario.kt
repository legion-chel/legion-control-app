package com.legion.control.models.client.scenario

import com.legion.utils.InputState
import kotlinx.coroutines.flow.Flow

interface ValidateInputClientNameScenario {
    val firstNameInputState: Flow<InputState?>
    val secondNameInputState: Flow<InputState?>
    
    suspend fun validateFirstName(firstName: String)
    suspend fun validateSecondName(secondName: String)
}