package com.legion.control.models.qr

import com.legion.api.http.ApiResult
import com.legion.control.models.qr.info.api.QrInfoApi
import com.legion.control.models.qr.info.data.QrContent
import com.legion.control.models.qr.info.data.QrData
import com.legion.control.models.qr.info.data.goods.GoodsQrInfo
import com.legion.utils.fromJson
import javax.inject.Inject

class QrModel @Inject constructor(
    private val api: QrInfoApi
) {
    suspend fun commitQrCode(qrData: String): GoodsQrInfo? {
        val data = fromJson<QrContent>(qrData) ?: return null
        val result = api.getGoodsInfo(QrData(data.hash))

        return if (result is ApiResult.Success) {
            val goodsInfo = result.value
            GoodsQrInfo(
                description = goodsInfo.description,
                count = goodsInfo.count,
                direction = goodsInfo.direction,
                actionType = data.type,
                qrId = data.id
            )
        }
        else null
    }
}