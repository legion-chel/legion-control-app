package com.legion.control.models.auth.api

import com.legion.api.http.ApiResult
import com.legion.control.models.auth.data.AuthData
import com.legion.core.session.auth.AuthInfo
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {
    @POST("/auth/login")
    suspend fun auth(@Body authData: AuthData): ApiResult<AuthInfo>

    @POST("/auth/refresh")
    suspend fun refresh(): ApiResult<AuthInfo>
}