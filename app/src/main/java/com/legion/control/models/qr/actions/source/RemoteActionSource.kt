package com.legion.control.models.qr.actions.source

import com.legion.api.http.ApiResult
import com.legion.control.models.qr.actions.data.Action

interface RemoteActionSource : ActionSource {
    suspend fun getRemoteActions(qrType: String, shiftId: Int?): ApiResult<List<Action>>
}