package com.legion.control.models.client.cases.goods.impl

import com.legion.control.models.client.cases.goods.GetGoodsDirectionsByNumberUseCase
import com.legion.control.models.qr.info.data.goods.GoodsDirection
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetGoodsDirectionsByNumberUseCaseImpl @Inject constructor() : GetGoodsDirectionsByNumberUseCase {

    override suspend fun invoke(number: Int): GoodsDirection = withContext(IO) {
        if (number == 0) GoodsDirection.IN else GoodsDirection.OUT
    }
}