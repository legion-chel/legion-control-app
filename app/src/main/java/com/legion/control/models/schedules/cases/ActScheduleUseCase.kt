package com.legion.control.models.schedules.cases

import com.legion.control.models.schedules.data.ScheduleActState
import kotlinx.coroutines.flow.StateFlow

interface ActScheduleUseCase {
	val actState: StateFlow<ScheduleActState?>
	suspend fun executeAct(imageId: Int)
}