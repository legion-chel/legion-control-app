package com.legion.control.models.upload

import android.content.Context
import android.net.Uri
import com.legion.api.http.ApiResult
import com.legion.control.models.upload.api.UploadApi
import com.legion.control.models.upload.data.UploadImageResult
import com.legion.control.models.upload.data.UploadImageType
import com.legion.utils.getImageFile
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import javax.inject.Inject

class Uploader @Inject constructor(
    private val context: Context,
    private val api: UploadApi
) {
    suspend fun uploadFile(imageType: UploadImageType, uri: Uri): UploadImageResult? {
        val result = withContext(IO) {
            val typePart = createTypePart(imageType.type)
            val filePart = createFileMultipart(uri)

            api.uploadImage(typePart, filePart)

        }

        return if (result is ApiResult.Success) result.value
        else null
    }

    private fun createFileMultipart(uri: Uri): MultipartBody.Part {
        val imageName = uri.lastPathSegment
        checkNotNull(imageName) { "required image name" }
        val file = getImageFile(context, imageName)
        val requestFile = file.asRequestBody()
        return MultipartBody.Part.createFormData(FILE_PART_NAME, file.name, requestFile)
    }

    private fun createTypePart(type: String): RequestBody {
        val part = TYPE_MEDIA_TYPE.toMediaTypeOrNull()
        return type.toRequestBody(part)
    }

    companion object {
        private const val FILE_PART_NAME = "photo"
        private const val TYPE_MEDIA_TYPE = "multipart/form-data"
    }
}