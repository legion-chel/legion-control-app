package com.legion.control.models.sync

import com.legion.control.models.schedules.cases.LoadScheduleUseCase
import com.legion.control.models.user.UserModel
import javax.inject.Inject

class DataSynchronizationModule @Inject constructor(
    private val userModel: UserModel,
    private val loadScheduleUseCase: LoadScheduleUseCase
) {
    suspend fun startSync() {
        userModel.loadAndSave()
        loadScheduleUseCase.loadSchedule()
    }
}