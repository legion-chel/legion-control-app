package com.legion.control.models.qr.actions.data.state

enum class ActionState {
    NOT_COMPLETE,
    COMPLETE
}