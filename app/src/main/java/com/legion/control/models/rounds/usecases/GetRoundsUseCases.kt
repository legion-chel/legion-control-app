package com.legion.control.models.rounds.usecases

import com.legion.control.models.rounds.entities.RoundUIWrapper
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.Flow

interface GetRoundsUseCases {
    suspend operator fun invoke(): Flow<LoadingState<RoundUIWrapper>>
}