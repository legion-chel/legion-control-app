package com.legion.control.models.client.repository.impl

import android.content.Context
import com.legion.api.http.ApiResult
import com.legion.api.utils.createFileMultipart
import com.legion.api.utils.createRequestBodyWithMediaType
import com.legion.control.models.client.api.ClientApi
import com.legion.control.models.client.data.CreatedPass
import com.legion.control.models.client.data.EmployeePass
import com.legion.control.models.client.data.GoodsPass
import com.legion.control.models.client.data.VisitorPass
import com.legion.control.models.client.repository.PassRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PassRepositoryImpl @Inject constructor(
    private val context: Context,
    private val api: ClientApi
) : PassRepository {

    override suspend fun createGoodsPass(goodsPass: GoodsPass): ApiResult<Nothing> {
        return withContext(Dispatchers.IO) {
            val descriptionPart = createRequestBodyWithMediaType(goodsPass.description)
            val countPart = createRequestBodyWithMediaType(goodsPass.count)
            val directionPart = createRequestBodyWithMediaType(goodsPass.direction.name.toLowerCase())
            val fileParts = goodsPass.photos
                .map { createFileMultipart(context, it) }

            api.createGoodsPass(
                description = descriptionPart,
                count = countPart,
                direction = directionPart,
                photos = fileParts
            )
        }
    }

    override suspend fun createEmployeePass(employeePass: EmployeePass): ApiResult<Nothing> {
        return withContext(Dispatchers.IO) {
            val firstNamePart = createRequestBodyWithMediaType(employeePass.firstName)
            val secondName = createRequestBodyWithMediaType(employeePass.secondName)
            val patronymic = createRequestBodyWithMediaType(employeePass.patronymic)
            val photosPart = employeePass
                .photos
                .map { createFileMultipart(context, it) }

            api.createEmployeePass(firstNamePart, secondName, patronymic, photosPart)
        }
    }

    override suspend fun createVisitorPass(visitorPass: VisitorPass): ApiResult<Nothing> {
        return withContext(Dispatchers.IO) {
            val firstNamePart = createRequestBodyWithMediaType(visitorPass.firstName)
            val secondName = createRequestBodyWithMediaType(visitorPass.secondName)
            val patronymic = createRequestBodyWithMediaType(visitorPass.patronymic)
            val photosPart = visitorPass
                .photos
                .map { createFileMultipart(context, it) }

            api.createEmployeePass(firstNamePart, secondName, patronymic, photosPart)
        }
    }

    override suspend fun getGoodsCreatedPasses(): ApiResult<List<CreatedPass.Goods>> {
        return withContext(Dispatchers.IO) {
            api.getGoodsCreatedPasses()
        }
    }

    override suspend fun getEmployeeCreatedPasses(): ApiResult<List<CreatedPass.Employee>> {
        return withContext(Dispatchers.IO) {
            api.getEmployeeCreatedPasses()
        }
    }

    override suspend fun getVisitorCreatedPasses(): ApiResult<List<CreatedPass.Visitor>> {
        return withContext(Dispatchers.IO) {
            api.getVisitorCreatedPasses()
        }
    }
}