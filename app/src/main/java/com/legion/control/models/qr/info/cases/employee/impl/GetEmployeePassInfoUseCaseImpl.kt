package com.legion.control.models.qr.info.cases.employee.impl

import com.legion.control.models.qr.info.cases.employee.GetEmployeePassInfoUseCase
import com.legion.control.models.qr.info.data.employee.EmployeeQrInfo
import com.legion.utils.ResourceProvider
import com.legion.utils.extensions.empty
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class GetEmployeePassInfoUseCaseImpl @Inject constructor(
    private val resourceProvider: ResourceProvider
) : GetEmployeePassInfoUseCase {

    private val _fullName = MutableStateFlow(String.empty)
    override val fullName: StateFlow<String>
        get() = _fullName

    private val _isEnabledPass = MutableStateFlow(String.empty)
    override val isEnabledPass: StateFlow<String>
        get() = _isEnabledPass

    override suspend fun getPassInfo(qrInfo: EmployeeQrInfo) {
        val (firstName, secondName, patronymic) = (qrInfo.employeeInfo)
        val fullName = "$secondName $firstName $patronymic"
        _fullName.emit(fullName)

        val isEnabled = qrInfo.employeeInfo.dateStart > qrInfo.employeeInfo.dateEnd
        val isEnabledText = if (isEnabled) "Активный" else "Неактивный"
        _isEnabledPass.emit(isEnabledText)
    }
}