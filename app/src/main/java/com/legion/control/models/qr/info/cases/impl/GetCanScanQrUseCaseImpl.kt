package com.legion.control.models.qr.info.cases.impl

import com.legion.control.models.qr.info.cases.GetCanScanQrUseCase
import com.legion.control.models.schedules.data.ScheduleStatus
import com.legion.control.models.schedules.repository.ScheduleRepository
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetCanScanQrUseCaseImpl @Inject constructor(
    private val scheduleRepository: ScheduleRepository
) : GetCanScanQrUseCase {
    
    override suspend fun invoke(): Boolean = withContext(IO) {
        val status = scheduleRepository.syncGetCurrentSchedule()?.status
        return@withContext status == ScheduleStatus.ACTIVE
    }
}