package com.legion.control.models.schedules.cases

import com.legion.control.models.schedules.data.ui.ScheduleUIWrapper
import kotlinx.coroutines.flow.Flow

interface GetCurrentScheduleUseCase {
    val currentSchedule: Flow<ScheduleUIWrapper>
}