package com.legion.control.models.qr.actions.data.result

import com.google.gson.annotations.SerializedName

class QrActionResultData(
    @SerializedName("actions")
    val actions: List<QrActionResult>
)