package com.legion.control.models.qr.info.cases.goods

import com.legion.control.components.guard.qr.goods.data.GoodsQrInfoUIWrapper
import com.legion.control.models.qr.info.data.QrContent
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.Flow

interface GetGoodsQrInfoUIWrapperUseCase {
    suspend operator fun invoke(qrContent: QrContent, scheduleId: Int?): Flow<LoadingState<GoodsQrInfoUIWrapper>?>
}