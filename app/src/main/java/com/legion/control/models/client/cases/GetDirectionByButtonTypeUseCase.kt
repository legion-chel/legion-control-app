package com.legion.control.models.client.cases

import androidx.navigation.NavDirections
import com.legion.control.models.ui.menu.ButtonType

interface GetDirectionByButtonTypeUseCase {
    fun getDirection(buttonType: ButtonType): NavDirections?
}