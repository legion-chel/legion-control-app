package com.legion.control.models.qr.info.cases.employee.impl

import com.legion.control.models.qr.actions.data.wrapper.ActionStateWrapper
import com.legion.control.models.qr.actions.repository.QrActionRepository
import com.legion.control.models.qr.actions.utils.ActionStateWrapperConverter
import com.legion.control.models.qr.info.cases.employee.GetEmployeeQrInfoUIWrapperUseCase
import com.legion.control.models.qr.info.data.QrContent
import com.legion.control.models.qr.info.data.QrData
import com.legion.control.models.qr.info.data.employee.EmployeeInfo
import com.legion.control.models.qr.info.data.employee.EmployeeQrInfoUIWrapper
import com.legion.control.models.qr.info.repository.QrInfoRepository
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class GetEmployeeQrInfoUIWrapperUseCaseImpl @Inject constructor(
    private val qrInfoRepository: QrInfoRepository,
    private val actionsRepository: QrActionRepository,
    private val converter: ActionStateWrapperConverter,
) : GetEmployeeQrInfoUIWrapperUseCase {


    override suspend fun invoke(qrContent: QrContent, scheduleId: Int?): Flow<LoadingState<EmployeeQrInfoUIWrapper>?> {
        val qrData = QrData(qrContent.hash)
        qrInfoRepository.getEmployeeQrInfo(qrData)
        actionsRepository.loadActions(qrContent.type, scheduleId)

        return qrInfoRepository.employeeInfo
            .combine(actionsRepository.actions) { employee, actions ->
                employee to actions
            }
            .transform {
                val employeeState = it.first
                val actionsState = it.second

                val state = when {
                    employeeState == null && actionsState == null -> null
                    employeeState is LoadingState.Success && actionsState is LoadingState.Success ->
                        createEmployeeQrInfoUIWrapper(
                            qrContent,
                            employeeState.data,
                            actionsState.data
                        )
                    employeeState is LoadingState.Error -> LoadingState.Error(employeeState.error)
                    actionsState is LoadingState.Error -> LoadingState.Error(actionsState.error)
                    else -> return@transform
                }

                emit(state)
            }
    }

    private fun createEmployeeQrInfoUIWrapper(
        qrContent: QrContent,
        employeeInfo: EmployeeInfo,
        actions: List<ActionStateWrapper>
    ): LoadingState.Success<EmployeeQrInfoUIWrapper> {
        val actionsUIWrappers = actions
            .map(converter::convert)

        val (name, secondName, patronymic) = employeeInfo

        val uiWrapper = EmployeeQrInfoUIWrapper(
            qrId = qrContent.id,
            actionType = qrContent.type,
            fullName = "$secondName $name $patronymic",
            access = "Доступ есть",
            actions = actionsUIWrappers
        )

        return LoadingState.Success(uiWrapper)
    }
}