package com.legion.control.models.upload.api

import com.legion.api.http.ApiResult
import com.legion.control.models.upload.data.UploadImageResult
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface UploadApi {
    @Multipart
    @POST("/photos/upload")
    suspend fun uploadImage(
        @Part("type") type: RequestBody,
        @Part photo: MultipartBody.Part
    ): ApiResult<UploadImageResult>
}