package com.legion.control.models.qr.actions.builder

import com.legion.api.http.ApiResult
import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.actions.data.state.ActionLoadingState

interface ActionLoadingStateBuilder {
    fun build(apiResult: ApiResult<List<Action>>): ActionLoadingState
}