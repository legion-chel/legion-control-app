package com.legion.control.models.schedules.utils.impl

import com.legion.control.models.schedules.data.Schedule
import com.legion.control.models.schedules.data.ui.ScheduleUIWrapper
import com.legion.control.models.schedules.utils.ScheduleConverter
import com.legion.control.models.schedules.utils.ScheduleStatusConverter
import com.legion.control.models.schedules.utils.ShiftTitleBuilder
import com.legion.utils.DATE_PATTERN_WITH_TIME
import com.legion.utils.timeUnixToDate
import javax.inject.Inject

class ScheduleDataUIConverterImpl @Inject constructor(
    private val statusConverter: ScheduleStatusConverter,
    private val scheduleTitleBuilder: ShiftTitleBuilder
) : ScheduleConverter {

    override fun convert(schedule: Schedule): ScheduleUIWrapper {
        val title = scheduleTitleBuilder.build(schedule.status)
        val startDate = timeUnixToDate(schedule.timeStart, DATE_PATTERN_WITH_TIME)
        val endDate = timeUnixToDate(schedule.timeEnd, DATE_PATTERN_WITH_TIME)
        val delegate = statusConverter.convert(schedule.status)

        return ScheduleUIWrapper
            .Builder()
            .setTitle(title)
            .setDescription(schedule.description)
            .setTimeStart(startDate)
            .setTimeEnd(endDate)
            .setActionDelegate(delegate)
            .build()
    }
}