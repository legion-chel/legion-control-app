package com.legion.control.models.qr.actions.data

import com.google.gson.annotations.SerializedName

enum class ActionType {
    @SerializedName("photo")
    PHOTO,

    @SerializedName("text")
    TEXT
}