package com.legion.control.models.schedules.cases.impl

import com.legion.control.models.schedules.cases.StartScheduleUseCase
import com.legion.control.models.schedules.repository.ScheduleRepository
import javax.inject.Inject

class StartScheduleUseCaseImpl @Inject constructor(
	private val repository: ScheduleRepository
) : StartScheduleUseCase {

	override val actState
		get() = repository.startState

	override suspend fun executeAct(imageId: Int) {
		val currentScheduleId = repository
			.syncGetCurrentSchedule()
			?.id

		repository.startSchedule(currentScheduleId, imageId)
	}
}