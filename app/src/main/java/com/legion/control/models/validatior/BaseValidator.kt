package com.legion.control.models.validatior

import com.legion.control.R
import com.legion.utils.InputState
import com.legion.utils.ResourceProvider

abstract class BaseValidator(
    private val resourceProvider: ResourceProvider
) {
    abstract suspend fun validate(input: String?)

    protected open fun getInputState(input: String?): InputState {
        return if (input.isNullOrEmpty()) {
            val error = resourceProvider.getString(R.string.error_empty_input)
            InputState.Incorrect(error)
        } else InputState.Correct
    }
}