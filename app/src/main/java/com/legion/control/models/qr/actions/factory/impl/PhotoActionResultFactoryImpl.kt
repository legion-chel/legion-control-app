package com.legion.control.models.qr.actions.factory.impl

import android.net.Uri
import com.legion.control.models.qr.actions.builder.ActionResultBuilder
import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.actions.data.result.QrActionResult
import com.legion.control.models.qr.actions.factory.PhotoActionResultFactory
import com.legion.control.models.qr.info.data.ActionType
import com.legion.control.models.upload.Uploader
import com.legion.control.models.upload.data.UploadImageType
import com.legion.utils.getCurrentTimeUnix
import javax.inject.Inject

class PhotoActionResultFactoryImpl @Inject constructor(
    private val uploader: Uploader
) : PhotoActionResultFactory {
    override suspend fun create(qrId: Int, actionType: ActionType, actions: Map<Action, Uri>): List<QrActionResult> {
        val photoResults = mutableListOf<QrActionResult>()

        actions.forEach { (action, uri) ->
            val uploadResult = uploader.uploadFile(UploadImageType.INCIDENT, uri)
            if (uploadResult != null) {
                val result = ActionResultBuilder()
                    .setResult(uploadResult.url)
                    .setQrId(qrId)
                    .setInsertDate(getCurrentTimeUnix())
                    .setQrType(actionType)
                    .build(action)

                photoResults.add(result)
            }
        }

        return photoResults
    }
}