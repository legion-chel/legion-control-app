package com.legion.control.models.ui.adapter

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.legion.control.R
import com.legion.utils.ResourceProvider
import javax.inject.Inject

class MapItemDecoration @Inject constructor(
    resourceProvider: ResourceProvider
) : RecyclerView.ItemDecoration() {

    private val margin = resourceProvider.getDimen(R.dimen.app_margin)

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.bottom = margin

        // Add top margin only for the first item to avoid double space between items
        if (parent.getChildLayoutPosition(view) == 0) {
            outRect.top = margin
        } else {
            outRect.top = 0
        }
    }
}