package com.legion.control.models.schedules.cases.ui.impl

import com.legion.control.models.schedules.cases.ui.GetActScheduleButtonEnabledUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class GetActScheduleButtonEnabledUseCaseImpl @Inject constructor() : GetActScheduleButtonEnabledUseCase {

    private val _buttonEnabled = MutableStateFlow(true)
    override val isEnabled: StateFlow<Boolean>
        get() = _buttonEnabled

    override suspend fun getEnabled(isLoading: Boolean) {
        _buttonEnabled.emit(isLoading.not())
    }
}