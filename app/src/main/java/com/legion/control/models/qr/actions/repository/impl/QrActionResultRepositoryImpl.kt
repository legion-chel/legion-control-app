package com.legion.control.models.qr.actions.repository.impl

import com.legion.api.http.ApiResult
import com.legion.control.models.qr.actions.api.ActionApi
import com.legion.control.models.qr.actions.data.result.QrActionResult
import com.legion.control.models.qr.actions.data.result.QrActionResultData
import com.legion.control.models.qr.actions.repository.QrActionResultRepository
import javax.inject.Inject

class QrActionResultRepositoryImpl @Inject constructor(
    private val api:ActionApi
) : QrActionResultRepository {

    override suspend fun send(result: List<QrActionResult>): ApiResult<Nothing> {
        val apiResult = QrActionResultData(result)
        return api.commitResult(apiResult)
    }
}