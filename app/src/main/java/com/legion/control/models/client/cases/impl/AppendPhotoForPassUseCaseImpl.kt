package com.legion.control.models.client.cases.impl

import android.graphics.Bitmap
import android.net.Uri
import com.legion.control.models.client.cases.AppendPhotoForPassUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class AppendPhotoForPassUseCaseImpl @Inject constructor() : AppendPhotoForPassUseCase {
    private val _appendedPhotos = MutableStateFlow<List<Bitmap>>(emptyList())
    override val appendedPhotos: StateFlow<List<Bitmap>>
        get() = _appendedPhotos.asStateFlow()

    private val _appendedPhotoUri = mutableListOf<Uri>()
    override val appendedPhotoUri: List<Uri>
        get() = _appendedPhotoUri

    override suspend fun append(uri: Uri, bitmap: Bitmap?) {
        bitmap ?: return

        _appendedPhotoUri.add(uri)

        val aPhotos = _appendedPhotos.value.toMutableList()
        aPhotos.add(bitmap)

        _appendedPhotos.emit(aPhotos)
    }
}