package com.legion.control.models.sync.usecases

import com.legion.control.models.sync.data.SyncState

interface NavigateBySyncStateUseCase {
    operator fun invoke(syncState: SyncState)
}