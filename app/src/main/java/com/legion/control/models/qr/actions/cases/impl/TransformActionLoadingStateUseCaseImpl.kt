package com.legion.control.models.qr.actions.cases.impl

import com.legion.control.components.guard.qr.goods.data.GoodsQrInfoUIState
import com.legion.control.components.guard.qr.goods.data.GoodsQrInfoUIWrapper
import com.legion.control.models.qr.actions.cases.TransformActionLoadingStateUseCase
import com.legion.control.models.qr.actions.data.state.ActionLoadingState
import com.legion.control.models.qr.actions.utils.ActionStateWrapperConverter
import com.legion.control.models.qr.info.data.goods.GoodsQrInfo
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class TransformActionLoadingStateUseCaseImpl @Inject constructor(
    private val converter: ActionStateWrapperConverter
) : TransformActionLoadingStateUseCase {

    private val _uiState = MutableStateFlow<GoodsQrInfoUIState>(GoodsQrInfoUIState.Loading)
    override val state: StateFlow<GoodsQrInfoUIState>
        get() = _uiState

    override suspend fun transform(
        loadingState: ActionLoadingState?,
        goodsInfo: GoodsQrInfo
    ) {
        val uiState = when(loadingState) {
            is ActionLoadingState.Success -> getSuccessState(loadingState, goodsInfo)
            is ActionLoadingState.Error -> GoodsQrInfoUIState.Error(loadingState.message)
            else -> return
        }

        _uiState.emit(uiState)
    }

    private fun getSuccessState(
        loadingState: ActionLoadingState.Success,
        goodsInfo: GoodsQrInfo
    ): GoodsQrInfoUIState {
        val (qrId, description, count, direction) = goodsInfo
        val wrappers = loadingState
            .wrappers
            .map(converter::convert)

        val wrapper = GoodsQrInfoUIWrapper(
            qrId = qrId,
            actionType = goodsInfo.actionType,
            description = description,
            count = count,
            direction = direction.name,
            actions = wrappers
        )

        return GoodsQrInfoUIState.Success(wrapper)
    }
}