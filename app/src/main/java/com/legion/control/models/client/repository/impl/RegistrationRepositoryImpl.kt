package com.legion.control.models.client.repository.impl

import android.content.Context
import com.legion.api.http.ApiResult
import com.legion.api.utils.createFileMultipart
import com.legion.api.utils.createRequestBody
import com.legion.control.R
import com.legion.control.models.client.api.ClientApi
import com.legion.control.models.client.data.ClientInfo
import com.legion.control.models.client.repository.RegistrationRepository
import com.legion.core.state.LoadingState
import com.legion.utils.ResourceProvider
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RegistrationRepositoryImpl @Inject constructor(
    private val context: Context,
    private val clientApi: ClientApi,
    private val resourceProvider: ResourceProvider
) : RegistrationRepository {
    
    override suspend fun registrationClient(
        userId: Int?,
        clientInfo: ClientInfo
    ): Flow<LoadingState<String>?> = flow {
        emit(LoadingState.Processing)
    
        val firstName = createRequestBody(clientInfo.firstName)
        val secondName = createRequestBody(clientInfo.secondName)
        
        val email = createRequestBody(clientInfo.email)
        
        val birthDate = createRequestBody(clientInfo.birthDate)
        val passportData = createRequestBody(clientInfo.passportData)
    
        val avatarMultipart = createFileMultipart(context, AVATAR_MULTIPART_NAME, clientInfo.avatar)
        val documentsMultipart = clientInfo.documentPhoto
            .map {
                createFileMultipart(context, DOCUMENTS_MULTIPART_NAME, it)
            }
    
        val apiResult = clientApi.registrationClient(
            userId = userId,
            firstName = firstName,
            secondName = secondName,
            email = email,
            birthDate = birthDate,
            passportData = passportData,
            avatar = avatarMultipart,
            documents = documentsMultipart
        )
    
        val state = when(apiResult) {
            is ApiResult.Success -> {
                val successMessage = resourceProvider.getString(R.string.success_registration_client)
                LoadingState.Success(successMessage)
            }
            is ApiResult.Failure.HttpError -> {
                val internetErrorMessage = resourceProvider
                    .getString(R.string.internet_failure_registration_client)
                LoadingState.Error(internetErrorMessage)
            }
            is ApiResult.Failure.Error -> {
                val failureErrorMessage = resourceProvider.getString(R.string.failure_registration_client)
                LoadingState.Error(failureErrorMessage)
            }
        }
        
        emit(state)
    }
    
    private companion object {
        const val AVATAR_MULTIPART_NAME = "avatar"
        const val DOCUMENTS_MULTIPART_NAME = "documents"
    }
}