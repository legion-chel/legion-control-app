package com.legion.control.models.rounds.entities.api

import com.legion.control.models.qr.info.data.ActionType

class GetRoundsResponse(
    val id: Int,
    val scenarioTemplateId: Int,
    val name: String,
    val description: String,
    val type: ActionType
)