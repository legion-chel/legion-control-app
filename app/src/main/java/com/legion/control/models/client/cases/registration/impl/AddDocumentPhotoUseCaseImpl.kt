package com.legion.control.models.client.cases.registration.impl

import android.graphics.Bitmap
import android.net.Uri
import com.legion.control.models.client.cases.registration.AddDocumentPhotoUseCase
import com.legion.control.models.client.repository.ClientPhotoRepository
import javax.inject.Inject

class AddDocumentPhotoUseCaseImpl @Inject constructor(
    private val repository: ClientPhotoRepository
) : AddDocumentPhotoUseCase {
    
    override suspend fun add(documentPhotoUri: Uri, documentPhotoBitmap: Bitmap?) =
        repository.saveDocumentPhoto(documentPhotoUri, documentPhotoBitmap)
    
    override fun getPhotosUri() = repository.documentUriPhotos
}