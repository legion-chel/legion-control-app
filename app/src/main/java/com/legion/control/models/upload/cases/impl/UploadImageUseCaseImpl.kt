package com.legion.control.models.upload.cases.impl

import android.content.Context
import android.net.Uri
import com.legion.api.http.ApiResult
import com.legion.control.models.upload.api.UploadApi
import com.legion.control.models.upload.cases.UploadImageUseCase
import com.legion.control.models.upload.data.UploadImageState
import com.legion.control.models.upload.data.UploadImageType
import com.legion.utils.getImageFile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import javax.inject.Inject

class UploadImageUseCaseImpl @Inject constructor(
	private val context: Context,
	private val api: UploadApi
) : UploadImageUseCase {

	private val _uploadImageState = MutableStateFlow<UploadImageState?>(null)
	override val uploadImageState: StateFlow<UploadImageState?>
		get()= _uploadImageState

	override suspend fun uploadImage(uri: Uri?, imageType: UploadImageType) {
		if (uri == null) {
			val error = UploadImageState.Error("Путь до файла пустой")
			_uploadImageState.emit(error)
			return
		}

		val result = withContext(Dispatchers.IO) {
			val typePart = createTypePart(imageType.type)
			val filePart = createFileMultipart(uri)

			api.uploadImage(typePart, filePart)
		}

		val state = when(result) {
			is ApiResult.Success -> {
				UploadImageState.Success(result.value)
			}

			is ApiResult.Failure<*> -> UploadImageState.Error("Ошибка загрузки фотографии")
		}

		_uploadImageState.emit(state)
	}

	private fun createFileMultipart(uri: Uri): MultipartBody.Part {
		val imageName = uri.lastPathSegment
		checkNotNull(imageName) { "required image name" }
		val file = getImageFile(context, imageName)
		val requestFile = file.asRequestBody()
		return MultipartBody.Part.createFormData(FILE_PART_NAME, file.name, requestFile)
	}

	private fun createTypePart(type: String): RequestBody {
		val part = TYPE_MEDIA_TYPE.toMediaTypeOrNull()
		return type.toRequestBody(part)
	}

	companion object {
		private const val FILE_PART_NAME = "photo"
		private const val TYPE_MEDIA_TYPE = "multipart/form-data"
	}
}