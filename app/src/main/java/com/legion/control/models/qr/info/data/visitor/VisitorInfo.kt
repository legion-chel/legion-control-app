package com.legion.control.models.qr.info.data.visitor

import com.google.gson.annotations.SerializedName

data class VisitorInfo(
    @SerializedName("visitor_name")
    val name: String,

    @SerializedName("visitor_last_name")
    val secondName: String,

    @SerializedName("visitor_patronymic_name")
    val patronymic: String,

    @SerializedName("date_start")
    val dateStart: String,

    @SerializedName("date_end")
    val dateEnd: String
)