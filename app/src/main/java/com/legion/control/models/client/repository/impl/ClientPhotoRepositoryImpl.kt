package com.legion.control.models.client.repository.impl

import android.graphics.Bitmap
import android.net.Uri
import com.legion.control.models.client.repository.ClientPhotoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ClientPhotoRepositoryImpl @Inject constructor() : ClientPhotoRepository {
    private val _attachedPhotos = MutableStateFlow<List<Bitmap>>(emptyList())
    override val attachedPhotos: Flow<List<Bitmap>>
        get() = _attachedPhotos
    
    private var _avatarUri: Uri? = null
    override val avatarUri: Uri?
        get() = _avatarUri
    
    private val _documentUriPhotos = mutableListOf<Uri>()
    override val documentUriPhotos: List<Uri>
        get() = _documentUriPhotos
    
    override suspend fun saveAvatar(avatarUri: Uri, avatarBitmap: Bitmap?) {
        avatarBitmap ?: return
        
        withContext(Dispatchers.IO) {
            _avatarUri = avatarUri
        }
        
        emitPhoto(avatarBitmap)
    }
    
    override suspend fun saveDocumentPhoto(documentUri: Uri, documentBitmap: Bitmap?) {
        documentBitmap ?: return
        
        withContext(Dispatchers.IO) {
            _documentUriPhotos.add(documentUri)
        }
        
        emitPhoto(documentBitmap)
    }
    
    private suspend fun emitPhoto(bitmap: Bitmap?) {
        bitmap ?: return
        
        withContext(Dispatchers.Default) {
            val currentPhotos = _attachedPhotos.value.toMutableList()
            currentPhotos.add(bitmap)
    
            _attachedPhotos.emit(currentPhotos)
        }
    }
}