package com.legion.control.models.utils

interface GetDateStringUseCase {
    operator fun invoke(time: Long): String
}