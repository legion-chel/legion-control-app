package com.legion.control.models.client.cases.registration

import android.graphics.Bitmap
import android.net.Uri

interface AddClientAvatarUseCase {
    suspend fun add(avatarUri: Uri, avatarBitmap: Bitmap?)
    fun getAvatarUri(): Uri?
}