package com.legion.control.models.qr.info.data.goods

import com.google.gson.annotations.SerializedName

data class GoodsInfo(
    @SerializedName("description")
    val description: String,

    @SerializedName("count")
    val count: Int,

    @SerializedName("direction")
    val direction: GoodsDirection
)