package com.legion.control.models.qr.info.data

import androidx.fragment.app.FragmentManager

typealias ShowQrInfoDelegate = (FragmentManager) -> Unit