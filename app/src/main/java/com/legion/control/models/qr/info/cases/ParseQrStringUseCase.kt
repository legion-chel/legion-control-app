package com.legion.control.models.qr.info.cases

import com.legion.control.models.qr.info.data.QrContent

interface ParseQrStringUseCase {
    suspend fun parse(qrString: String): QrContent?
}