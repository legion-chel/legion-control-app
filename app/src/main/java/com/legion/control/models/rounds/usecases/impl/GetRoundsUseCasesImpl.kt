package com.legion.control.models.rounds.usecases.impl

import com.legion.control.models.qr.actions.repository.QrActionRepository
import com.legion.control.models.qr.actions.utils.ActionStateWrapperConverter
import com.legion.control.models.qr.info.data.ActionType
import com.legion.control.models.rounds.entities.RoundUIWrapper
import com.legion.control.models.rounds.repository.RoundsRepository
import com.legion.control.models.rounds.usecases.GetRoundsUseCases
import com.legion.control.models.schedules.repository.ScheduleRepository
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

class GetRoundsUseCasesImpl @Inject constructor(
    private val roundRepository: RoundsRepository,
    private val actionsRepository: QrActionRepository,
    private val scheduleRepository: ScheduleRepository,
    private val converter: ActionStateWrapperConverter
) : GetRoundsUseCases {
    
    override suspend fun invoke(): Flow<LoadingState<RoundUIWrapper>>  {
        val scheduleId = scheduleRepository.syncGetCurrentSchedule()?.id
        actionsRepository.loadActions(ActionType.ROUNDS, scheduleId)
    
        return roundRepository.getRounds(scheduleId)
            .combine(actionsRepository.actions) { roundsLoadingState, actionsLoadingState ->
                val state = if (roundsLoadingState is LoadingState.Success && actionsLoadingState is LoadingState.Success) {
                    val round = roundsLoadingState.data
                    val actions = actionsLoadingState
                        .data
                        .map(converter::convert)
                    
                    val wrapper = RoundUIWrapper(
                        roundId = round.id,
                        title = round.name,
                        description = round.description,
                        actionType = ActionType.ROUNDS,
                        actions = actions,
                        scenarioTemplateId = round.scenarioTemplateId
                    )
                    LoadingState.Success(wrapper)
                } else LoadingState.Error("Ошибка")
                
                return@combine state
            }
        
    }
}