package com.legion.control.models.auth.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.legion.utils.log.Logger
import javax.inject.Inject

class LogoutBroadcast @Inject constructor(context: Context) {

    private val broadcastManager = LocalBroadcastManager
        .getInstance(context)

    fun observe(observer: Observer) {
        try {
            broadcastManager.registerReceiver(observer, IntentFilter(ACTION))
        } catch (e: Exception) {
            Logger.e(e, "Error on register logout observer")
        }
    }

    fun removeObserving(observer: Observer) {
        try {
            broadcastManager.unregisterReceiver(observer)
        } catch (e: Exception) {
            Logger.e(e, "Error on unregister logout observer")
        }
    }

    class Sender @Inject constructor(context: Context) {
        private val broadcastManager = LocalBroadcastManager
            .getInstance(context)

        fun send() {
            val intent = Intent(ACTION)
            broadcastManager.sendBroadcast(intent)
        }
    }

    class Observer(
        private val logoutDelegate: () -> Unit
    ) : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action == ACTION) logoutDelegate()
        }
    }

    companion object {
        private const val ACTION = "logout"
    }
}