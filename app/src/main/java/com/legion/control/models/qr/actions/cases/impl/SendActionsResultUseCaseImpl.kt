package com.legion.control.models.qr.actions.cases.impl

import android.net.Uri
import com.legion.api.http.ApiResult
import com.legion.control.models.qr.actions.cases.SendQrActionsResultUseCase
import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.actions.factory.PhotoActionResultFactory
import com.legion.control.models.qr.actions.factory.TextActionResultFactory
import com.legion.control.models.qr.actions.repository.QrActionResultRepository
import com.legion.control.models.qr.info.data.ActionType
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class SendActionsResultUseCaseImpl @Inject constructor(
    private val photoResultFactory: PhotoActionResultFactory,
    private val textResultFactory: TextActionResultFactory,
    private val repository: QrActionResultRepository
) : SendQrActionsResultUseCase {

    private val _sendLoadingState = MutableStateFlow<LoadingState<String>?>(null)
    override val sendLoadingState: StateFlow<LoadingState<String>?>
        get() = _sendLoadingState.asStateFlow()

    override suspend fun send(
        qrId: Int,
        actionType: ActionType,
        photoActions: Map<Action, Uri>,
        textActions: Map<Action, String>
    ) {
        setState(LoadingState.Processing)

        val photoResults = photoResultFactory.create(qrId, actionType, photoActions)
        val textResults = textResultFactory.create(qrId, actionType, textActions)

        val totalResults = photoResults
            .union(textResults)
            .toList()

        val result = repository.send(totalResults)
        val state = if (result is ApiResult.Success) LoadingState.Success("Результат проверки сохранен")
        else LoadingState.Error("Ошибка отправки результатов")

        setState(state)
    }

    private suspend fun setState(state: LoadingState<String>?) {
        _sendLoadingState.emit(state)
    }
}