package com.legion.control.models.qr.actions.factory

import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.actions.data.result.QrActionResult
import com.legion.control.models.qr.info.data.ActionType

interface TextActionResultFactory {
    fun create(qrId: Int, actionType: ActionType, actions: Map<Action, String>): List<QrActionResult>
}