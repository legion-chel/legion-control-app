package com.legion.control.models.client.template.impl

import com.legion.control.models.client.scenario.ValidateInputClientNameScenario
import com.legion.control.models.client.scenario.ValidateInputDocumentInfoScenario
import com.legion.control.models.client.template.ValidateInputClientInfoTemplate
import com.legion.control.models.utils.ValidateEmailUseCase
import com.legion.utils.InputState
import com.legion.utils.isCorrect
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

class ValidateInputClientInfoTemplateImpl @Inject constructor(
    private val validateInputClientNameScenario: ValidateInputClientNameScenario,
    private val validateEmailUseCase: ValidateEmailUseCase,
    private val validateInputDocumentInfoScenario: ValidateInputDocumentInfoScenario
) : ValidateInputClientInfoTemplate {
    override val firstNameInputState: Flow<InputState?>
        get() = validateInputClientNameScenario.firstNameInputState
    
    override val secondNameInputState: Flow<InputState?>
        get() = validateInputClientNameScenario.secondNameInputState
    
    override val emailInputState: Flow<InputState?>
        get() = validateEmailUseCase.inputState
    
    override val documentSerialInputState: Flow<InputState?>
        get() = validateInputDocumentInfoScenario.serialInputState
    
    override val documentNumberInputState: Flow<InputState?>
        get() = validateInputDocumentInfoScenario.numberInputState
    
    override val buttonEnabled: Flow<Boolean>
        get() = validateInputClientNameScenario.firstNameInputState
            .combine(validateInputClientNameScenario.secondNameInputState) { first, second ->
                first.isCorrect && second.isCorrect
            }
            .combine(validateInputDocumentInfoScenario.serialInputState) { isCorrectNameInfo, serialState ->
                val isCorrectSerial = serialState.isCorrect
                isCorrectSerial && isCorrectNameInfo
            }
    
    override suspend fun validateFirstName(firstName: String) =
        validateInputClientNameScenario.validateFirstName(firstName)
    
    override suspend fun validateSecondName(secondName: String) =
        validateInputClientNameScenario.validateSecondName(secondName)
    
    override suspend fun validateEmail(email: String) =
        validateEmailUseCase.validate(email)
    
    override suspend fun validateDocumentSerial(serial: String) =
        validateInputDocumentInfoScenario.validateSerial(serial)
    
    override suspend fun validateDocumentNumber(number: String) =
        validateInputDocumentInfoScenario.validateNumber(number)
}