package com.legion.control.models.client.scenario.impl

import com.legion.control.models.client.cases.registration.RegistrationClientUseCase
import com.legion.control.models.client.data.ClientInfo
import com.legion.control.models.client.scenario.RegistrationClientScenario
import com.legion.control.models.utils.TransformLoadingStateToUIStateUseCase
import kotlinx.coroutines.flow.transform
import javax.inject.Inject

class RegistrationClientScenarioImpl @Inject constructor(
    private val registrationClientUseCase: RegistrationClientUseCase,
    private val transformLoadingStateToUIStateUseCase: TransformLoadingStateToUIStateUseCase
) : RegistrationClientScenario {
    override suspend fun registrationClient(clientInfo: ClientInfo) = registrationClientUseCase(clientInfo)
        .transform {
            val state = transformLoadingStateToUIStateUseCase(it) ?: return@transform
            emit(state)
        }
}