package com.legion.control.models.qr.info.repository

import com.legion.control.models.qr.info.data.QrData
import com.legion.control.models.qr.info.data.employee.EmployeeInfo
import com.legion.control.models.qr.info.data.goods.GoodsInfo
import com.legion.control.models.qr.info.data.visitor.VisitorInfo
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.StateFlow

interface QrInfoRepository {
    val goodsInfo: StateFlow<LoadingState<GoodsInfo>?>
    val employeeInfo: StateFlow<LoadingState<EmployeeInfo>?>
    val visitorInfo: StateFlow<LoadingState<VisitorInfo>?>

    suspend fun getGoodsQrInfo(qrData: QrData)
    suspend fun getEmployeeQrInfo(qrData: QrData)
    suspend fun getVisitorQrInfo(qrData: QrData)
}