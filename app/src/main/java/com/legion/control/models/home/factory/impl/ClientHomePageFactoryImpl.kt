package com.legion.control.models.home.factory.impl

import com.legion.control.components.client.created.CreatedPassesFragment
import com.legion.control.components.client.main.ClientMainFragment
import com.legion.control.components.profile.ProfileFragment
import com.legion.control.models.home.factory.ClientHomePageFactory
import javax.inject.Inject

class ClientHomePageFactoryImpl @Inject constructor(
    private val mainFragment: ClientMainFragment,
    private val createdPassesFragment: CreatedPassesFragment,
    private val profileFragment: ProfileFragment
) : ClientHomePageFactory {

    override fun create() = listOf(
        mainFragment,
        createdPassesFragment,
        profileFragment
    )
}