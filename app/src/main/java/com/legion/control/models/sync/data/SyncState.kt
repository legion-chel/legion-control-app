package com.legion.control.models.sync.data

enum class SyncState {
    PROCESSING,
    NEED_AUTH,
    DONE,
    NEED_ENTER_INFO
}