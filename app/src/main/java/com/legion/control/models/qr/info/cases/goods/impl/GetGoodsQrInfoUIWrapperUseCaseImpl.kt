package com.legion.control.models.qr.info.cases.goods.impl

import com.legion.control.components.guard.qr.goods.data.GoodsQrInfoUIWrapper
import com.legion.control.models.qr.actions.data.wrapper.ActionStateWrapper
import com.legion.control.models.qr.actions.repository.QrActionRepository
import com.legion.control.models.qr.actions.utils.ActionStateWrapperConverter
import com.legion.control.models.qr.info.cases.goods.GetGoodsQrInfoUIWrapperUseCase
import com.legion.control.models.qr.info.data.QrContent
import com.legion.control.models.qr.info.data.QrData
import com.legion.control.models.qr.info.data.goods.GoodsInfo
import com.legion.control.models.qr.info.repository.QrInfoRepository
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class GetGoodsQrInfoUIWrapperUseCaseImpl @Inject constructor(
    private val qrInfoRepository: QrInfoRepository,
    private val actionsRepository: QrActionRepository,
    private val converter: ActionStateWrapperConverter
) : GetGoodsQrInfoUIWrapperUseCase {

    override suspend fun invoke(qrContent: QrContent, scheduleId: Int?): Flow<LoadingState<GoodsQrInfoUIWrapper>?> {
        val qrData = QrData(qrContent.hash)
        qrInfoRepository.getGoodsQrInfo(qrData)
        actionsRepository.loadActions(qrContent.type, scheduleId)

        return qrInfoRepository.goodsInfo
            .combine(actionsRepository.actions) { goods, actions ->
                goods to actions
            }
            .transform {
                val goodsState = it.first
                val actionsState = it.second

                val state = when {
                    goodsState == null && actionsState == null -> null
                    goodsState is LoadingState.Success && actionsState is LoadingState.Success ->
                        createGoodsQrInfoUIWrapper(
                            qrContent,
                            goodsState.data,
                            actionsState.data
                        )

                    goodsState is LoadingState.Error -> LoadingState.Error(goodsState.error)
                    actionsState is LoadingState.Error -> LoadingState.Error(actionsState.error)
                    else -> return@transform
                }

                emit(state)
            }
    }

    private fun createGoodsQrInfoUIWrapper(
        qrContent: QrContent,
        goodsInfo: GoodsInfo,
        actions: List<ActionStateWrapper>
    ): LoadingState.Success<GoodsQrInfoUIWrapper> {
        val actionsUIWrappers = actions
            .map(converter::convert)

        val uiWrapper = GoodsQrInfoUIWrapper(
            qrId = qrContent.id,
            actionType = qrContent.type,
            description = goodsInfo.description,
            count = goodsInfo.count,
            direction = goodsInfo.direction.name,
            actions = actionsUIWrappers
        )

        return LoadingState.Success(uiWrapper)
    }
}