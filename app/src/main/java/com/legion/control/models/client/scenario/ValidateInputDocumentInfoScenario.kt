package com.legion.control.models.client.scenario

import com.legion.utils.InputState
import kotlinx.coroutines.flow.Flow

interface ValidateInputDocumentInfoScenario {
    val serialInputState: Flow<InputState?>
    val numberInputState: Flow<InputState?>
    
    suspend fun validateSerial(serial: String)
    suspend fun validateNumber(number: String)
}