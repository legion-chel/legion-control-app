package com.legion.control.models.qr.info.cases.visitor.impl

import com.legion.control.models.qr.actions.data.wrapper.ActionStateWrapper
import com.legion.control.models.qr.actions.repository.QrActionRepository
import com.legion.control.models.qr.actions.utils.ActionStateWrapperConverter
import com.legion.control.models.qr.info.cases.visitor.GetVisitorQrInfoUIWrapperUseCase
import com.legion.control.models.qr.info.data.QrContent
import com.legion.control.models.qr.info.data.QrData
import com.legion.control.models.qr.info.data.visitor.VisitorInfo
import com.legion.control.models.qr.info.data.visitor.VisitorQrInfoUIWrapper
import com.legion.control.models.qr.info.repository.QrInfoRepository
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class GetVisitorQrInfoUIWrapperUseCaseImpl @Inject constructor(
    private val qrInfoRepository: QrInfoRepository,
    private val actionsRepository: QrActionRepository,
    private val converter: ActionStateWrapperConverter,
) : GetVisitorQrInfoUIWrapperUseCase {

    override suspend fun invoke(qrContent: QrContent, scheduleId: Int?): Flow<LoadingState<VisitorQrInfoUIWrapper>?> {
        val qrData = QrData(qrContent.hash)
        qrInfoRepository.getVisitorQrInfo(qrData)
        actionsRepository.loadActions(qrContent.type, scheduleId)

        return qrInfoRepository.visitorInfo
            .combine(actionsRepository.actions) { visitor, actions ->
                visitor to actions
            }
            .transform {
                val visitorState = it.first
                val actionsState = it.second

                val state = when {
                    visitorState == null && actionsState == null -> null
                    visitorState is LoadingState.Success && actionsState is LoadingState.Success ->
                        createVisitorQrInfoUIWrapper(
                            qrContent,
                            visitorState.data,
                            actionsState.data
                        )
                    visitorState is LoadingState.Error -> LoadingState.Error(visitorState.error)
                    actionsState is LoadingState.Error -> LoadingState.Error(actionsState.error)
                    else -> return@transform
                }

                emit(state)
            }
    }

    private fun createVisitorQrInfoUIWrapper(
        qrContent: QrContent,
        employeeInfo: VisitorInfo,
        actions: List<ActionStateWrapper>
    ): LoadingState.Success<VisitorQrInfoUIWrapper> {
        val actionsUIWrappers = actions
            .map(converter::convert)

        val (name, secondName, patronymic) = employeeInfo

        val uiWrapper = VisitorQrInfoUIWrapper(
            qrId = qrContent.id,
            actionType = qrContent.type,
            fullName = "$secondName $name $patronymic",
            access = "Доступ есть",
            actions = actionsUIWrappers
        )

        return LoadingState.Success(uiWrapper)
    }

}