package com.legion.control.models.client.scenario

import com.legion.control.models.qr.info.data.goods.GoodsDirection

interface GetGoodsDirectionsScenario {
    fun getGoodsDirections(): List<String>
    suspend fun getDirectionByNumber(number: Int): GoodsDirection
}