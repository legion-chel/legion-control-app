package com.legion.control.models.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.legion.control.databinding.ItemMapBinding
import javax.inject.Inject

class MapAdapter @Inject constructor() : RecyclerView.Adapter<MapAdapter.UserInfoHolder>() {

    private val pairs = mutableListOf<Pair<String, String>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserInfoHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemMapBinding.inflate(inflater, parent, false)

        return UserInfoHolder(binding)
    }

    override fun onBindViewHolder(holder: UserInfoHolder, position: Int) {
        val pair = pairs[position]
        holder.bind(pair)
    }

    override fun getItemCount(): Int = pairs.size

    fun update(pairs: List<Pair<String, String>>) {
        this.pairs.apply {
            clear()
            addAll(pairs)
        }

        notifyDataSetChanged()
    }

    class UserInfoHolder(
        private val binding: ItemMapBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(info: Pair<String, String>) {
            val (label, text) = info

            with(binding) {
                this.label.text = label
                this.value.text = text
            }
        }
    }
}