package com.legion.control.models.home.cases.impl

import androidx.fragment.app.Fragment
import com.legion.control.models.home.cases.GetHomePagesUseCase
import com.legion.control.models.home.factory.ClientHomePageFactory
import com.legion.control.models.home.factory.GuardHomePageFactory
import com.legion.core.session.UserSessionFacade
import com.legion.core.session.user.UserRole
import javax.inject.Inject

class GetHomePagesUseCaseImpl @Inject constructor(
    private val userSession: UserSessionFacade,
    private val guardFactory: GuardHomePageFactory,
    private val clientFactory: ClientHomePageFactory
) : GetHomePagesUseCase {

    override fun getPages(): List<Fragment> {
        return when(userSession.user?.role) {
            UserRole.GUARD -> guardFactory.create()
            UserRole.CLIENT_USER -> clientFactory.create()
            else -> emptyList()
        }
    }
}