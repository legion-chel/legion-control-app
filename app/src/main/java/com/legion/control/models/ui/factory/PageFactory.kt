package com.legion.control.models.ui.factory

import androidx.fragment.app.Fragment

interface PageFactory<T : Fragment> {
    fun create(): List<T>
}