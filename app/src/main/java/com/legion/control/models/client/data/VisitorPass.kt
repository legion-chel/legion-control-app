package com.legion.control.models.client.data

import android.net.Uri

class VisitorPass(
    val firstName: String,
    val secondName: String,
    val patronymic: String,
    val photos: List<Uri>
)