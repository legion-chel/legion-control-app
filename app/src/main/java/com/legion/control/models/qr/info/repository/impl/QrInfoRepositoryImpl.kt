package com.legion.control.models.qr.info.repository.impl

import com.legion.api.http.ApiResult
import com.legion.control.models.qr.info.api.QrInfoApi
import com.legion.control.models.qr.info.data.QrData
import com.legion.control.models.qr.info.data.employee.EmployeeInfo
import com.legion.control.models.qr.info.data.goods.GoodsInfo
import com.legion.control.models.qr.info.data.visitor.VisitorInfo
import com.legion.control.models.qr.info.repository.QrInfoRepository
import com.legion.core.state.LoadingState
import com.legion.utils.ResourceProvider
import com.legion.utils.log.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class QrInfoRepositoryImpl @Inject constructor(
    private val api: QrInfoApi
) : QrInfoRepository {

    private val _goodsQrInfo = MutableStateFlow<LoadingState<GoodsInfo>?>(null)
    override val goodsInfo: StateFlow<LoadingState<GoodsInfo>?>
        get() = _goodsQrInfo.asStateFlow()

    private val _employeeInfo = MutableStateFlow<LoadingState<EmployeeInfo>?>(null)
    override val employeeInfo: StateFlow<LoadingState<EmployeeInfo>?>
        get() = _employeeInfo.asStateFlow()

    private val _visitorInfo = MutableStateFlow<LoadingState<VisitorInfo>?>(null)
    override val visitorInfo: StateFlow<LoadingState<VisitorInfo>?>
        get() = _visitorInfo.asStateFlow()

    private val notValidQrError
        get() = LoadingState.Error("QR код не подтвержден")

    private val errorLoadingData
        get() = LoadingState.Error("Ошибка загрузки данных")

    override suspend fun getGoodsQrInfo(qrData: QrData) {
        _goodsQrInfo.emit(LoadingState.Processing)

        withContext(Dispatchers.IO) {
            api.getGoodsInfo(qrData).handleResult(
                successDelegate = this@QrInfoRepositoryImpl::onSuccessLoadingGoodsInfo,
                httpErrorDelegate = this@QrInfoRepositoryImpl::onHttpErrorLoadingGoodsInfo,
                errorDelegate = this@QrInfoRepositoryImpl::onErrorLoadingGoodsInfo
            )
        }
    }

    override suspend fun getEmployeeQrInfo(qrData: QrData) {
        _employeeInfo.emit(LoadingState.Processing)

        withContext(Dispatchers.IO) {
            api.getEmployeeInfo(qrData).handleResult(
                successDelegate = this@QrInfoRepositoryImpl::onSuccessLoadingEmployeeInfo,
                httpErrorDelegate = this@QrInfoRepositoryImpl::onHttpErrorLoadingEmployeeInfo,
                errorDelegate = this@QrInfoRepositoryImpl::onErrorLoadingEmployeeInfo
            )
        }
    }

    override suspend fun getVisitorQrInfo(qrData: QrData) {
        _visitorInfo.emit(LoadingState.Processing)

        withContext(Dispatchers.IO) {
            api.getVisitorInfo(qrData).handleResult(
                successDelegate = this@QrInfoRepositoryImpl::onSuccessLoadingVisitorInfo,
                httpErrorDelegate = this@QrInfoRepositoryImpl::onHttpErrorLoadingVisitorInfo,
                errorDelegate = this@QrInfoRepositoryImpl::onErrorLoadingVisitorInfo
            )
        }
    }

    private suspend fun onSuccessLoadingGoodsInfo(qrInfo: GoodsInfo?) {
        qrInfo ?: return
        
        withContext(Dispatchers.Default) {
            val successState = LoadingState.Success(qrInfo)
            _goodsQrInfo.emit(successState)
        }
    }

    private suspend fun onHttpErrorLoadingGoodsInfo(result: ApiResult.Failure.HttpError) {
        withContext(Dispatchers.Default) {
            val errorState = if (result.statusCode == NOT_VALID_QR) {
                Logger.e(result.error, "Error on loading goods info before invalid qr code")
                notValidQrError
            }
            else {
                Logger.e(result.error, "Error on loading goods info")
                errorLoadingData
            }

            _goodsQrInfo.emit(errorState)
        }
    }

    private suspend fun onSuccessLoadingEmployeeInfo(qrInfo: EmployeeInfo?) {
        qrInfo ?: return
        
        withContext(Dispatchers.Default) {
            val successState = LoadingState.Success(qrInfo)
            _employeeInfo.emit(successState)
        }
    }

    private suspend fun onHttpErrorLoadingEmployeeInfo(result: ApiResult.Failure.HttpError) {
        withContext(Dispatchers.Default) {
            val errorState = if (result.statusCode == NOT_VALID_QR) {
                Logger.e(result.error, "Error on loading employee info before invalid qr code")
                notValidQrError
            }
            else {
                Logger.e(result.error, "Error on loading employee info")
                errorLoadingData
            }

            _employeeInfo.emit(errorState)
        }
    }

    private suspend fun onSuccessLoadingVisitorInfo(qrInfo: VisitorInfo?) {
        qrInfo ?: return
        
        withContext(Dispatchers.Default) {
            val successState = LoadingState.Success(qrInfo)
            _visitorInfo.emit(successState)
        }
    }

    private suspend fun onHttpErrorLoadingVisitorInfo(result: ApiResult.Failure.HttpError) {
        withContext(Dispatchers.Default) {
            val errorState = if (result.statusCode == NOT_VALID_QR) {
                Logger.e(result.error, "Error on loading visitor info before invalid qr code")
                notValidQrError
            }
            else {
                Logger.e(result.error, "Error on loading visitor info")
                errorLoadingData
            }

            _visitorInfo.emit(errorState)
        }
    }

    private suspend fun onErrorLoadingGoodsInfo(throwable: Throwable) {
        withContext(Dispatchers.Default) {
            Logger.e(throwable, "Error on loading goods info")
            _goodsQrInfo.emit(errorLoadingData)
        }
    }

    private suspend fun onErrorLoadingEmployeeInfo(throwable: Throwable) {
        withContext(Dispatchers.Default) {
            Logger.e(throwable, "Error on loading employee info")
            _employeeInfo.emit(errorLoadingData)
        }
    }

    private suspend fun onErrorLoadingVisitorInfo(throwable: Throwable) {
        withContext(Dispatchers.Default) {
            Logger.e(throwable, "Error on loading visitor info")
            _visitorInfo.emit(errorLoadingData)
        }
    }

    companion object {
        private const val NOT_VALID_QR = 422
    }
}