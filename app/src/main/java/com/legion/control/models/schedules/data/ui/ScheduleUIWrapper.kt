package com.legion.control.models.schedules.data.ui

import com.legion.control.models.schedules.data.ScheduleActionDelegate
import com.legion.utils.extensions.empty

class ScheduleUIWrapper private constructor(
    val title: String,
    val description: String,
    val timeStart: String,
    val timeEnd: String,
    val actionDelegate: ScheduleActionDelegate?
) {
    class Builder {
        private var title = String.empty
        private var description = String.empty
        private var timeStart = String.empty
        private var timeEnd = String.empty
        private var actionDelegate: ScheduleActionDelegate? = null

        fun setTitle(title: String) = apply {
            this.title = title
        }

        fun setDescription(description: String) = apply {
            this.description = description
        }

        fun setTimeStart(timeStart: String) = apply {
            this.timeStart = timeStart
        }

        fun setTimeEnd(timeEnd: String) = apply {
            this.timeEnd = timeEnd
        }

        fun setActionDelegate(actionDelegate: ScheduleActionDelegate?) = apply {
            this.actionDelegate = actionDelegate
        }

        fun build() = ScheduleUIWrapper(
            title = title,
            description = description,
            timeStart = timeStart,
            timeEnd = timeEnd,
            actionDelegate = actionDelegate
        )
    }
}