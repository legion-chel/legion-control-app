package com.legion.control.models.client.cases.registration.impl

import com.legion.control.models.client.cases.registration.RegistrationClientUseCase
import com.legion.control.models.client.data.ClientInfo
import com.legion.control.models.client.repository.RegistrationRepository
import com.legion.core.session.UserSessionFacade
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class RegistrationClientUseCaseImpl @Inject constructor(
    private val repository: RegistrationRepository,
    private val userSessionFacade: UserSessionFacade
) : RegistrationClientUseCase {
    
    override suspend fun invoke(clientInfo: ClientInfo): Flow<LoadingState<String>?> {
        val userId = userSessionFacade.user?.id
        return repository
            .registrationClient(userId, clientInfo)
    }
}