package com.legion.control.models.client.repository

import android.graphics.Bitmap
import android.net.Uri
import kotlinx.coroutines.flow.Flow

interface ClientPhotoRepository {
    val attachedPhotos: Flow<List<Bitmap>>
    
    val avatarUri: Uri?
    val documentUriPhotos: List<Uri>
    
    suspend fun saveAvatar(avatarUri: Uri, avatarBitmap: Bitmap?)
    suspend fun saveDocumentPhoto(documentUri: Uri, documentBitmap: Bitmap?)
}