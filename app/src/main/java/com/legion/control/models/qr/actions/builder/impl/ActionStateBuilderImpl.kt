package com.legion.control.models.qr.actions.builder.impl

import com.legion.api.http.ApiResult
import com.legion.control.models.qr.actions.builder.ActionLoadingStateBuilder
import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.actions.data.state.ActionLoadingState
import com.legion.control.models.qr.actions.data.state.ActionState
import com.legion.control.models.qr.actions.data.wrapper.ActionStateWrapper
import com.legion.utils.ResourceProvider
import javax.inject.Inject

class ActionStateBuilderImpl @Inject constructor(
    private val resourceProvider: ResourceProvider
) : ActionLoadingStateBuilder {

    override fun build(apiResult: ApiResult<List<Action>>): ActionLoadingState {
        return when(apiResult) {
            is ApiResult.Success -> buildSuccessState(apiResult.value)
            is ApiResult.Failure.HttpError -> ActionLoadingState.Error("")
            is ApiResult.Failure.Error -> ActionLoadingState.Error("")
        }
    }

    private fun buildSuccessState(actions: List<Action>): ActionLoadingState.Success {
        val wrappers = actions
            .map {
                ActionStateWrapper(it, ActionState.NOT_COMPLETE)
            }

        return ActionLoadingState.Success(wrappers)
    }
}