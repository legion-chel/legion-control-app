package com.legion.control.models.client.cases.goods

interface GetGoodsDirectionsUseCase {
    operator fun invoke(): List<String>
}