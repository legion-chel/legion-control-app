package com.legion.control.models.rounds.repository.impl

import com.legion.api.http.ApiResult
import com.legion.control.models.rounds.api.RoundApiService
import com.legion.control.models.rounds.entities.Round
import com.legion.control.models.rounds.repository.RoundsRepository
import com.legion.core.state.LoadingState
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RoundsRepositoryImpl @Inject constructor(
    private val roundApiService: RoundApiService
) : RoundsRepository {
    
    override suspend fun getRounds(scheduleId: Int?): Flow<LoadingState<Round>> = flow {
        val apiResult = withContext(IO) {
            roundApiService.getRounds(scheduleId)
        }
        
        val loadingState = if (apiResult is ApiResult.Success) {
            val roundResponse = apiResult.value.firstOrNull()
            if (roundResponse != null) {
                val round = Round(
                    id = roundResponse.id,
                    name = roundResponse.name,
                    description = roundResponse.description,
                    scenarioTemplateId = roundResponse.scenarioTemplateId,
                    scheduleId = scheduleId!!
                )
                
                LoadingState.Success(round)
            } else LoadingState.Error("Ошибка")
        } else LoadingState.Error("Ошибка")
        
        emit(loadingState)
    }
}