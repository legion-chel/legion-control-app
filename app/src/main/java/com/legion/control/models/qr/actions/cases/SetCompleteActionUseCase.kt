package com.legion.control.models.qr.actions.cases

import com.legion.control.models.qr.actions.data.Action

interface SetCompleteActionUseCase {
    suspend fun setComplete(action: Action)
}