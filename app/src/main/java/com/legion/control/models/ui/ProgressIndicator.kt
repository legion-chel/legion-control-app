package com.legion.control.models.ui

interface ProgressIndicator {
    fun showProgress()
    fun hideProgress()
}