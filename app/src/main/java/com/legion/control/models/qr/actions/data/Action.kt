package com.legion.control.models.qr.actions.data

import com.google.gson.annotations.SerializedName

data class Action(
    @SerializedName("index")
    val position: Int,

    @SerializedName("scenario_id")
    val scenarioId: Int,

    @SerializedName("template_action_id")
    val templateActionId: Int,

    @SerializedName("description")
    val description: String,

    @SerializedName("typeResult")
    val type: ActionType
)