package com.legion.control.models.utils.impl

import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.legion.control.models.utils.ConfigureCrashlyticsUseCase
import com.legion.core.session.UserSessionFacade
import javax.inject.Inject

class ConfigureCrashlyticsUseCaseImpl @Inject constructor(
    private val firebaseCrashlytics: FirebaseCrashlytics,
    private val userSessionFacade: UserSessionFacade
) : ConfigureCrashlyticsUseCase {

    override fun invoke() {
        userSessionFacade.user?.email?.let { email ->
            firebaseCrashlytics.setUserId(email)
        }
    }
}