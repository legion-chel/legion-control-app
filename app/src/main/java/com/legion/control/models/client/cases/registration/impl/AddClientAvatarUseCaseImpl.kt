package com.legion.control.models.client.cases.registration.impl

import android.graphics.Bitmap
import android.net.Uri
import com.legion.control.models.client.cases.registration.AddClientAvatarUseCase
import com.legion.control.models.client.repository.ClientPhotoRepository
import javax.inject.Inject

class AddClientAvatarUseCaseImpl @Inject constructor(
    private val repository: ClientPhotoRepository
) : AddClientAvatarUseCase {
    
    override suspend fun add(avatarUri: Uri, avatarBitmap: Bitmap?) {
        repository.saveAvatar(avatarUri, avatarBitmap)
    }
    
    override fun getAvatarUri(): Uri? = repository.avatarUri
}