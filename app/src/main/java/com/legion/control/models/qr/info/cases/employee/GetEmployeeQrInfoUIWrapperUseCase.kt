package com.legion.control.models.qr.info.cases.employee

import com.legion.control.models.qr.info.data.QrContent
import com.legion.control.models.qr.info.data.employee.EmployeeQrInfoUIWrapper
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.Flow

interface GetEmployeeQrInfoUIWrapperUseCase {
    suspend operator fun invoke(qrContent: QrContent, scheduleId: Int?): Flow<LoadingState<EmployeeQrInfoUIWrapper>?>
}