package com.legion.control.models.schedules.utils.impl

import androidx.fragment.app.FragmentManager
import com.legion.control.R
import com.legion.control.models.schedules.data.ScheduleActionDelegate
import com.legion.control.models.schedules.data.ScheduleStatus
import com.legion.control.models.schedules.utils.ScheduleStatusConverter
import com.legion.control.models.schedules.utils.ShitDialogBuilder
import com.legion.utils.ResourceProvider
import javax.inject.Inject

class ScheduleStatusConverterImpl @Inject constructor(
    private val resourceProvider: ResourceProvider,
    private val dialogBuilder: ShitDialogBuilder
) : ScheduleStatusConverter {

    override fun convert(status: ScheduleStatus): ScheduleActionDelegate? {
        val titleId = when (status) {
            ScheduleStatus.ACTIVE -> R.string.shift_end
            ScheduleStatus.PLANNED -> R.string.shift_start
            else -> return null
        }

        val title = resourceProvider.getString(titleId)
        val action = buildAction(status)

        return ScheduleActionDelegate(title, action)
    }

    private fun buildAction(status: ScheduleStatus): ((FragmentManager) -> Unit) {
        return { fragmentManager ->
            val fragment = when (status) {
                ScheduleStatus.PLANNED -> dialogBuilder.createStartShiftDialog()
                ScheduleStatus.ACTIVE -> dialogBuilder.createFinishShiftDialog()
                else -> null
            }

            fragment?.let {
                it.show(fragmentManager, it.tag)
            }
        }
    }
}