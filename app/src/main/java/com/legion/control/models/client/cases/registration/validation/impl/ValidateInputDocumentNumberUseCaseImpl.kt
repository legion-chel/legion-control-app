package com.legion.control.models.client.cases.registration.validation.impl

import com.legion.control.R
import com.legion.control.models.client.cases.registration.validation.ValidateInputDocumentNumberUseCase
import com.legion.utils.InputState
import com.legion.utils.ResourceProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ValidateInputDocumentNumberUseCaseImpl @Inject constructor(
    private val resourceProvider: ResourceProvider
) : ValidateInputDocumentNumberUseCase {
    
    private val _inputState = MutableStateFlow<InputState?>(null)
    override val inputState: Flow<InputState?>
        get() = _inputState
    
    override suspend fun validate(number: String) {
        withContext(Dispatchers.IO) {
            val state = when {
                number.isEmpty() -> {
                    val errMessage = resourceProvider.getString(R.string.error_empty_input)
                    InputState.Incorrect(errMessage)
                }
    
                number.length != 6 -> {
                    val errMessage = resourceProvider.getString(R.string.error_doc_number_incorrect_length)
                    InputState.Incorrect(errMessage)
                }
            
                else -> InputState.Correct
            }
        
            _inputState.emit(state)
        }
    }
}