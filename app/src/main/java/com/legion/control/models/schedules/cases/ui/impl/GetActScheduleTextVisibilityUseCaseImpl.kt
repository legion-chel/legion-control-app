package com.legion.control.models.schedules.cases.ui.impl

import android.graphics.Bitmap
import com.legion.control.models.schedules.cases.ui.GetActScheduleTextVisibilityUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class GetActScheduleTextVisibilityUseCaseImpl @Inject constructor(

) : GetActScheduleTextVisibilityUseCase {

    private val _textIsVisible = MutableStateFlow(true)
    override val textIsVisible: StateFlow<Boolean>
        get() = _textIsVisible

    override suspend fun getTextVisibility(bitmap: Bitmap?) {
        val isVisible = bitmap == null
        _textIsVisible.emit(isVisible)
    }

}