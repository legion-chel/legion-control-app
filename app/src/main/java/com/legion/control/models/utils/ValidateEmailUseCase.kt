package com.legion.control.models.utils

import com.legion.utils.InputState
import kotlinx.coroutines.flow.Flow

interface ValidateEmailUseCase {
    val inputState: Flow<InputState?>
    suspend fun validate(email: String)
}