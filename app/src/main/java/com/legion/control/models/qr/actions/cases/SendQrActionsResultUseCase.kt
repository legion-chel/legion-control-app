package com.legion.control.models.qr.actions.cases

import android.net.Uri
import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.info.data.ActionType
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.StateFlow

interface SendQrActionsResultUseCase {
    val sendLoadingState: StateFlow<LoadingState<String>?>
    suspend fun send(qrId: Int, actionType: ActionType, photoActions: Map<Action, Uri>, textActions: Map<Action, String>)
}