package com.legion.control.models.upload.data

enum class UploadImageType(val type: String) {
    SELFIE("selfie"),
    AVATAR("avatar"),
    INCIDENT("incident")
}
