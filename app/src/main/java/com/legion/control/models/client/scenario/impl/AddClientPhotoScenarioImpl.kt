package com.legion.control.models.client.scenario.impl

import android.graphics.Bitmap
import android.net.Uri
import com.legion.control.models.client.cases.registration.AddClientAvatarUseCase
import com.legion.control.models.client.cases.registration.AddDocumentPhotoUseCase
import com.legion.control.models.client.cases.registration.GetAttachedPhotosUseCase
import com.legion.control.models.client.scenario.AddClientPhotoScenario
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class AddClientPhotoScenarioImpl @Inject constructor(
    private val addClientAvatarUseCase: AddClientAvatarUseCase,
    private val addDocumentPhotoUseCase: AddDocumentPhotoUseCase,
    private val getAttachedPhotosUseCase: GetAttachedPhotosUseCase
) : AddClientPhotoScenario {
    
    override val attachedPhotos: Flow<List<Bitmap>>
        get() = getAttachedPhotosUseCase()
    
    override suspend fun addDocumentPhoto(uri: Uri, bitmap: Bitmap?) {
        addDocumentPhotoUseCase.add(uri, bitmap)
    }
    
    override suspend fun addAvatar(uri: Uri, bitmap: Bitmap?) {
        addClientAvatarUseCase.add(uri, bitmap)
    }
    
    override fun getDocumentsUrl() = addDocumentPhotoUseCase.getPhotosUri()
    
    override fun getAvatarUri() = addClientAvatarUseCase.getAvatarUri()
}