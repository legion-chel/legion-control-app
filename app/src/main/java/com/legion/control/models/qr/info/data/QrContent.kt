package com.legion.control.models.qr.info.data

import com.google.gson.annotations.SerializedName

data class QrContent(
    @SerializedName("qrId")
    val id: Int,
    
    @SerializedName("qrType")
    val type: ActionType,
    
    @SerializedName("qrData")
    val hash: String
)

data class QrData(
    @SerializedName("qrData")
    val hash: String
)