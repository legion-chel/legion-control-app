package com.legion.control.models.qr.actions.data.result

import com.google.gson.annotations.SerializedName

class QrActionResult(
    @SerializedName("scenario_id")
    val scenarioId: Int,
    
    @SerializedName("template_action_id")
    val templateActionId: Int,
    
    @SerializedName("object_type")
    val qrType: String,

    @SerializedName("object_id")
    val qrId: Int,

    @SerializedName("ins_date")
    val insertDate: Long,

    @SerializedName("result")
    val result: String
)