package com.legion.control.models.user

import com.legion.control.R
import com.legion.core.session.UserSessionFacade
import com.legion.utils.ResourceProvider
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UserInfoModel @Inject constructor(
    private val resourceProvider: ResourceProvider,
    private val userSessionFacade: UserSessionFacade
) {
    private val _userInfo = MutableStateFlow<List<Pair<String, String>>>(emptyList())
    val userInfo
        get() = _userInfo.asStateFlow()

    suspend fun loadInfo() = withContext(IO) {
        loadUser()
    }

    private suspend fun loadUser() {
        val userInfo = userSessionFacade.user ?: return

        val info = listOf(
            getString(R.string.user_info_licence_number) to userInfo.licenseNumber,
            getString(R.string.user_info_registration_date) to userInfo.registrationDate
        )

        _userInfo.emit(info)
    }

    private fun getString(resId: Int) = resourceProvider.getString(resId)
}