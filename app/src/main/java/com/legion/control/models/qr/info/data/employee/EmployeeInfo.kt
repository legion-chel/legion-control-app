package com.legion.control.models.qr.info.data.employee

import com.google.gson.annotations.SerializedName

data class EmployeeInfo(
    @SerializedName("employee_name")
    val firstName: String,

    @SerializedName("employee_last_name")
    val secondName: String,

    @SerializedName("employee_patronymic_name")
    val patronymic: String,

    @SerializedName("date_start")
    val dateStart: String,

    @SerializedName("date_end")
    val dateEnd: String
)