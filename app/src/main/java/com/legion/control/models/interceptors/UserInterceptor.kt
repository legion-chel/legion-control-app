package com.legion.control.models.interceptors

import com.legion.core.session.UserSessionFacade
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class UserInterceptor @Inject constructor(
    private val userSessionFacade: UserSessionFacade
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response = chain.proceed(chain.request())
}