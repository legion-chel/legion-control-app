package com.legion.control.models.qr.actions.repository

import com.legion.api.http.ApiResult
import com.legion.control.models.qr.actions.data.result.QrActionResult

interface QrActionResultRepository {
    suspend fun send(result: List<QrActionResult>): ApiResult<Nothing>
}