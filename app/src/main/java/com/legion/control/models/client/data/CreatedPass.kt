package com.legion.control.models.client.data

import com.google.gson.annotations.SerializedName

sealed class CreatedPass(
    @SerializedName("qr_url")
    val qrUrl: String
) {

    class Goods(
        @SerializedName("description")
        val description: String,

        @SerializedName("date_start")
        val startDate: String,

        @SerializedName("date_end")
        val endDate: String,

        qrUrl: String
    ) : CreatedPass(qrUrl)

    class Employee(
        @SerializedName("employee_name")
        val firstName: String,

        @SerializedName("employee_last_name")
        val secondName: String,

        @SerializedName("employee_patronymic_name")
        val patronymic: String,

        qrUrl: String
    ): CreatedPass(qrUrl) {
        operator fun component1() = firstName
        operator fun component2() = secondName
        operator fun component3() = patronymic
    }

    class Visitor(
        @SerializedName("visitor_name")
        val firstName: String,

        @SerializedName("visitor_last_name")
        val secondName: String,

        @SerializedName("visitor_patronymic_name")
        val patronymic: String,

        qrUrl: String
    ): CreatedPass(qrUrl) {
        operator fun component1() = firstName
        operator fun component2() = secondName
        operator fun component3() = patronymic
    }
}
