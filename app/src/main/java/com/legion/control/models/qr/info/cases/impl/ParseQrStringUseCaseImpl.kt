package com.legion.control.models.qr.info.cases.impl

import com.legion.control.models.qr.info.cases.ParseQrStringUseCase
import com.legion.control.models.qr.info.data.QrContent
import com.legion.utils.fromJson
import javax.inject.Inject

class ParseQrStringUseCaseImpl @Inject constructor() : ParseQrStringUseCase {
    override suspend fun parse(qrString: String): QrContent? = fromJson<QrContent>(qrString)
}