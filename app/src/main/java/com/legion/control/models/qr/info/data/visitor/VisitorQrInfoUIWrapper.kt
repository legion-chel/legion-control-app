package com.legion.control.models.qr.info.data.visitor

import com.legion.control.components.guard.qr.adapter.data.ActionUIWrapper
import com.legion.control.models.qr.info.data.ActionType

data class VisitorQrInfoUIWrapper(
    val qrId: Int,
    val actionType: ActionType,
    val fullName: String,
    val access: String,
    val actions: List<ActionUIWrapper>
)