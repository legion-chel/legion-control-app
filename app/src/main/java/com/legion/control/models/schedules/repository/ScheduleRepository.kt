package com.legion.control.models.schedules.repository

import com.legion.control.models.schedules.data.Schedule
import com.legion.control.models.schedules.data.ScheduleActState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

interface ScheduleRepository {
    val loadingState: StateFlow<ScheduleActState?>
    val startState: StateFlow<ScheduleActState?>
    val finishState: StateFlow<ScheduleActState?>

    fun getCurrentSchedule(): Flow<Schedule?>

    suspend fun syncGetCurrentSchedule(): Schedule?
    suspend fun updateSchedule()

    suspend fun startSchedule(scheduleId: Int?, photoId: Int?)
    suspend fun finishSchedule(scheduleId: Int?, photoId: Int?)
}