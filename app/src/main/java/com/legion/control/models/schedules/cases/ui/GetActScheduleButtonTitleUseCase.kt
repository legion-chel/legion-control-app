package com.legion.control.models.schedules.cases.ui

import android.graphics.Bitmap
import kotlinx.coroutines.flow.StateFlow

interface GetActScheduleButtonTitleUseCase {
    val title: StateFlow<String>
    suspend fun getButtonTitle(bitmap: Bitmap?)
}