package com.legion.control.models.schedules.cases.ui

import kotlinx.coroutines.flow.StateFlow

interface GetActScheduleButtonEnabledUseCase {
    val isEnabled: StateFlow<Boolean>
    suspend fun getEnabled(isLoading: Boolean)
}