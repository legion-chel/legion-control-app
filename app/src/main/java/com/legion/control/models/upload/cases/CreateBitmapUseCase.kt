package com.legion.control.models.upload.cases

import android.graphics.Bitmap
import android.net.Uri
import kotlinx.coroutines.flow.StateFlow

interface CreateBitmapUseCase {
    val bitmap: StateFlow<Bitmap?>
    suspend fun create(uri: Uri)
}