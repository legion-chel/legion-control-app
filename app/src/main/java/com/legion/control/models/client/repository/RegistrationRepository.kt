package com.legion.control.models.client.repository

import com.legion.control.models.client.data.ClientInfo
import com.legion.core.state.LoadingState
import kotlinx.coroutines.flow.Flow

interface RegistrationRepository {
    suspend fun registrationClient(userId: Int?, clientInfo: ClientInfo): Flow<LoadingState<String>?>
}