package com.legion.control.models.client.cases.impl

import androidx.navigation.NavDirections
import com.legion.control.components.client.main.ClientMainFragmentDirections
import com.legion.control.models.client.cases.GetDirectionByButtonTypeUseCase
import com.legion.control.models.ui.menu.ButtonType
import javax.inject.Inject

class GetDirectionByButtonTypeUseCaseImpl @Inject constructor() : GetDirectionByButtonTypeUseCase {

    override fun getDirection(buttonType: ButtonType): NavDirections? {
        return when(buttonType) {
            ButtonType.CREATE_GOODS_PASS -> ClientMainFragmentDirections.toCreateGoodsPass()
            ButtonType.CREATE_EMPLOYEE_PASS -> ClientMainFragmentDirections.toCreateEmployeePass()
            ButtonType.CREATE_VISITOR_PASS -> ClientMainFragmentDirections.toCreateVisitorPass()
            else -> null
        }
    }
}