package com.legion.control.models.sync.scenario.impl

import com.legion.control.models.sync.data.SyncState
import com.legion.control.models.sync.scenario.HandleSyncStateScenario
import com.legion.control.models.sync.usecases.GetLoadingStateBySyncStateUseCase
import com.legion.control.models.sync.usecases.NavigateBySyncStateUseCase
import javax.inject.Inject

class HandleSyncStateScenarioImpl @Inject constructor(
    private val getLoadingStateBySyncStateUseCase: GetLoadingStateBySyncStateUseCase,
    private val navigateBySyncStateUseCase: NavigateBySyncStateUseCase
) : HandleSyncStateScenario {
    
    override fun navigateBySyncState(syncState: SyncState) =
        navigateBySyncStateUseCase(syncState)
    
    override fun getLoadingBySyncState(syncState: SyncState) =
        getLoadingStateBySyncStateUseCase(syncState)
}