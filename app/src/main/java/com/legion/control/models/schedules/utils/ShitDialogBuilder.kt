package com.legion.control.models.schedules.utils

import com.legion.control.components.guard.schedules.finish.FinishScheduleFragment
import com.legion.control.components.guard.schedules.start.StartScheduleFragment
import com.legion.control.di.core.viewmodel.ViewModelFactory
import javax.inject.Inject

class ShitDialogBuilder @Inject constructor(
    private val factory: ViewModelFactory
){
    fun createStartShiftDialog(): StartScheduleFragment = StartScheduleFragment(factory)
    fun createFinishShiftDialog(): FinishScheduleFragment = FinishScheduleFragment(factory)
}