package com.legion.control.models.schedules.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "Schedule")
data class Schedule(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: Int,

    @SerializedName("description")
    val description: String,

    @SerializedName("timeStart")
    val timeStart: Long,

    @SerializedName("timeEnd")
    val timeEnd: Long,

    @SerializedName("status")
    var status: ScheduleStatus
)