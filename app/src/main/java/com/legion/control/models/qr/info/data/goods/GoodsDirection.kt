package com.legion.control.models.qr.info.data.goods

import com.google.gson.annotations.SerializedName

enum class GoodsDirection {
    @SerializedName("in")
    IN,

    @SerializedName("out")
    OUT
}