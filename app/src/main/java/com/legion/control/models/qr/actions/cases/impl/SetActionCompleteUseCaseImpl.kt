package com.legion.control.models.qr.actions.cases.impl

import com.legion.control.models.qr.actions.cases.SetCompleteActionUseCase
import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.actions.data.ActionType
import com.legion.control.models.qr.actions.data.state.ActionState
import com.legion.control.models.qr.actions.data.wrapper.ActionStateWrapper
import com.legion.control.models.qr.actions.repository.QrActionRepository
import javax.inject.Inject

class SetActionCompleteUseCaseImpl @Inject constructor(
    private val repository: QrActionRepository
) : SetCompleteActionUseCase {

    override suspend fun setComplete(action: Action) {
        if (action.type == ActionType.TEXT) return
        
        val wrapper = ActionStateWrapper(action, ActionState.COMPLETE)
        repository.updateActions(wrapper)
    }
}