package com.legion.control.models.client.cases.goods

import com.legion.control.models.qr.info.data.goods.GoodsDirection

interface GetGoodsDirectionsByNumberUseCase {
    suspend operator fun invoke(number: Int): GoodsDirection
}