package com.legion.control.models.qr.actions.api

import com.legion.api.http.ApiResult
import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.actions.data.result.QrActionResultData
import retrofit2.http.*

interface ActionApi {
    @GET("scenarios/{qrType}/actions")
    suspend fun getActions(
        @Path("qrType") qrType: String,
        @Query("currentScheduleId") shiftId: Int?
    ): ApiResult<List<Action>>

    @POST("/actions/results")
    suspend fun commitResult(@Body data: QrActionResultData): ApiResult<Nothing>
}