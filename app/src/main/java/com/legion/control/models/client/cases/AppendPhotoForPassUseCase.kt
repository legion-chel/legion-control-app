package com.legion.control.models.client.cases

import android.graphics.Bitmap
import android.net.Uri
import kotlinx.coroutines.flow.StateFlow

interface AppendPhotoForPassUseCase {
    val appendedPhotos: StateFlow<List<Bitmap>>
    val appendedPhotoUri: List<Uri>

    suspend fun append(uri: Uri, bitmap: Bitmap?)
}