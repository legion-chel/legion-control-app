package com.legion.control.models.sync.scenario

import com.legion.control.models.sync.data.SyncState

interface HandleSyncStateScenario {
    fun navigateBySyncState(syncState: SyncState)
    fun getLoadingBySyncState(syncState: SyncState): Boolean
}