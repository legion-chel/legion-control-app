package com.legion.control.models.client.scenario.impl

import com.legion.control.models.client.cases.employee.GetEmployeeCreatesPassesUseCase
import com.legion.control.models.client.cases.goods.GetGoodsCreatedPassesUseCase
import com.legion.control.models.client.cases.visitor.GetVisitorCreatedPassesUseCase
import com.legion.control.models.client.data.CreatedPass
import com.legion.control.models.client.scenario.GetCreatedPassesScenario
import com.legion.core.state.LoadingState
import com.legion.core.state.UIState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetCreatedPassesScenarioImpl @Inject constructor(
    private val getGoodsCreatedPassesUseCase: GetGoodsCreatedPassesUseCase,
    private val getEmployeeCreatesPassesUseCase: GetEmployeeCreatesPassesUseCase
) : GetCreatedPassesScenario {

    override val uiState: Flow<UIState<List<CreatedPass>>>
        get() = getEmployeeCreatesPassesUseCase.loadingState
            .combine(getGoodsCreatedPassesUseCase.loadingState) { employeeState, goodsState ->
                if (goodsState == null || employeeState == null)
                    return@combine UIState.Loading
    
                val isSuccess = goodsState.isSuccess || employeeState.isSuccess
                return@combine if (!isSuccess) UIState.Error("Ошибка загрузки данных")
                else {
                    val resultPasses = mutableListOf<CreatedPass>()
                    if (goodsState is LoadingState.Success) resultPasses.addAll(goodsState.data)
                    if (employeeState is LoadingState.Success) resultPasses.addAll(employeeState.data)
    
                   UIState.Success(resultPasses)
                }
            }

    override suspend fun getCreatedPasses() {
        withContext(Dispatchers.Default) {
            getGoodsCreatedPassesUseCase.getCreatedPasses()
            getEmployeeCreatesPassesUseCase.getCreatedPasses()
        }
    }
}