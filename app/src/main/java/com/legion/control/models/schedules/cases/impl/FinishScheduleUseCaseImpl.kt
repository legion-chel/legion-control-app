package com.legion.control.models.schedules.cases.impl

import com.legion.control.models.schedules.cases.FinishScheduleUseCase
import com.legion.control.models.schedules.data.ScheduleActState
import com.legion.control.models.schedules.repository.ScheduleRepository
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class FinishScheduleUseCaseImpl @Inject constructor(
	private val repository: ScheduleRepository
) : FinishScheduleUseCase {

	override val actState: StateFlow<ScheduleActState?>
		get() = repository.finishState

	override suspend fun executeAct(imageId: Int) {
		val currentScheduleId = repository
			.syncGetCurrentSchedule()
			?.id

		repository.finishSchedule(currentScheduleId, imageId)
		repository
	}
}