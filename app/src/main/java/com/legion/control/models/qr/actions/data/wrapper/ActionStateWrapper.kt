package com.legion.control.models.qr.actions.data.wrapper

import com.legion.control.models.qr.actions.data.Action
import com.legion.control.models.qr.actions.data.state.ActionState

data class ActionStateWrapper(
    val action: Action,
    val state: ActionState
)