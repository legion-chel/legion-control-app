package com.legion.control.models.home.cases.impl

import com.legion.control.R
import com.legion.control.models.home.cases.GetHomeMenuIdUseCase
import com.legion.core.session.UserSessionFacade
import com.legion.core.session.user.UserRole
import javax.inject.Inject

class GetHomeMenuIdUseCaseImpl @Inject constructor(
    private val userSessionFacade: UserSessionFacade
) : GetHomeMenuIdUseCase {
    override fun getMenuId(): Int? {
        return when(userSessionFacade.user?.role) {
            UserRole.GUARD -> R.menu.menu_guard_home
            UserRole.CLIENT_USER -> R.menu.menu_client_home
            else -> null
        }
    }

}