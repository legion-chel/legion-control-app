package com.legion.control.di.modules

import com.legion.control.models.upload.cases.CreateBitmapUseCase
import com.legion.control.models.upload.cases.SaveImageUriUseCase
import com.legion.control.models.upload.cases.UploadImageUseCase
import com.legion.control.models.upload.cases.impl.CreateBitmapUseCaseImpl
import com.legion.control.models.upload.cases.impl.SaveImageUriUseCaseImpl
import com.legion.control.models.upload.cases.impl.UploadImageUseCaseImpl
import com.legion.control.models.upload.scenario.UploadImageScenario
import com.legion.control.models.upload.scenario.impl.UploadImageScenarioImpl
import dagger.Binds
import dagger.Module

@Module
interface UploadModule {
	@Binds
	fun bindUploadImageUseCase(useCase: UploadImageUseCaseImpl): UploadImageUseCase

	@Binds
	fun bindCreateBitmapUseCase(useCase: CreateBitmapUseCaseImpl): CreateBitmapUseCase

	@Binds
	fun bindSaveImageUriUseCase(useCase: SaveImageUriUseCaseImpl): SaveImageUriUseCase

	@Binds
	fun bindUploadImageScenarioImpl(uploadImageScenarioImpl: UploadImageScenarioImpl): UploadImageScenario
}