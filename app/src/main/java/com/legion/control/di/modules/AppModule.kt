package com.legion.control.di.modules

import android.content.Context
import com.legion.control.MainApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    fun provideApplicationContext(application: MainApplication): Context = application
        .applicationContext
}