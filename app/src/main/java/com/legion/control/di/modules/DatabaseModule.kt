package com.legion.control.di.modules

import com.legion.control.database.AppDatabaseBuilder
import com.legion.control.database.impl.AppDatabaseBuilderImpl
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
interface DatabaseModule {
    @Binds
    fun bindAppDatabaseBuilder(builder: AppDatabaseBuilderImpl): AppDatabaseBuilder

    companion object {
        @Provides
        fun provideAppDatabase(builder: AppDatabaseBuilder) = builder.getDatabase()
    }
}