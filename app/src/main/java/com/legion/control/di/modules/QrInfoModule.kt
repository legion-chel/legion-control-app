package com.legion.control.di.modules

import com.legion.control.models.qr.info.cases.GetCanScanQrUseCase
import com.legion.control.models.qr.info.cases.ParseQrStringUseCase
import com.legion.control.models.qr.info.cases.employee.GetEmployeePassInfoUseCase
import com.legion.control.models.qr.info.cases.employee.GetEmployeeQrInfoUIWrapperUseCase
import com.legion.control.models.qr.info.cases.employee.impl.GetEmployeePassInfoUseCaseImpl
import com.legion.control.models.qr.info.cases.employee.impl.GetEmployeeQrInfoUIWrapperUseCaseImpl
import com.legion.control.models.qr.info.cases.goods.GetGoodsQrInfoUIWrapperUseCase
import com.legion.control.models.qr.info.cases.goods.impl.GetGoodsQrInfoUIWrapperUseCaseImpl
import com.legion.control.models.qr.info.cases.impl.GetCanScanQrUseCaseImpl
import com.legion.control.models.qr.info.cases.impl.ParseQrStringUseCaseImpl
import com.legion.control.models.qr.info.cases.visitor.GetVisitorPassInfoUseCase
import com.legion.control.models.qr.info.cases.visitor.GetVisitorQrInfoUIWrapperUseCase
import com.legion.control.models.qr.info.cases.visitor.impl.GetVisitorPassInfoUseCaseImpl
import com.legion.control.models.qr.info.cases.visitor.impl.GetVisitorQrInfoUIWrapperUseCaseImpl
import com.legion.control.models.qr.info.repository.QrInfoRepository
import com.legion.control.models.qr.info.repository.impl.QrInfoRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
interface QrInfoModule {

    @Binds
    fun bindParseQrStringUseCase(useCase: ParseQrStringUseCaseImpl): ParseQrStringUseCase

    @Binds
    fun bindGetGoodsQrInfoUseCase(useCase: GetGoodsQrInfoUIWrapperUseCaseImpl): GetGoodsQrInfoUIWrapperUseCase

    @Binds
    fun bindGetEmployeeQrInfoUseCase(
        useCase: GetEmployeeQrInfoUIWrapperUseCaseImpl
    ): GetEmployeeQrInfoUIWrapperUseCase

    @Binds
    fun bindGetVisitorQrInfoUseCase(
        useCase: GetVisitorQrInfoUIWrapperUseCaseImpl
    ): GetVisitorQrInfoUIWrapperUseCase

    @Binds
    fun bindGetEmployeePassInfoUseCase(
        useCase: GetEmployeePassInfoUseCaseImpl
    ): GetEmployeePassInfoUseCase

    @Binds
    fun bindGetVisitorPassInfoUseCase(
        useCase: GetVisitorPassInfoUseCaseImpl
    ): GetVisitorPassInfoUseCase

    @Binds
    fun bindQrInfoRepository(repository: QrInfoRepositoryImpl): QrInfoRepository
    
    @Binds
    fun bindCanScanQrUseCase(useCase: GetCanScanQrUseCaseImpl): GetCanScanQrUseCase
}