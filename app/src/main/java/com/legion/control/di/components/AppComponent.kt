package com.legion.control.di.components

import com.legion.control.MainApplication
import com.legion.control.components.guard.rounds.RoundsViewModel
import com.legion.control.di.modules.*
import com.legion.control.di.modules.client.ClientModule
import com.legion.control.di.modules.client.CreatedPassesModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        SubComponentsModule::class,
        AppModule::class,
        ActivityModule::class,
        FragmentModule::class,
        ViewModelModule::class,
        ApiModule::class,
        ScheduleModule::class,
        QrActionModule::class,
        DatabaseModule::class,
        UploadModule::class,
        QrInfoModule::class,
        HomeModule::class,
        ClientModule::class,
        CreatedPassesModule::class,
        UtilsModule::class,
        FirebaseModule::class,
        EnterUserInfoModule::class,
        NavigationModule::class,
        SyncModule::class,
        RoundModule::class
    ]
)
interface AppComponent : AndroidInjector<MainApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: MainApplication): Builder
        fun build(): AppComponent
    }
}