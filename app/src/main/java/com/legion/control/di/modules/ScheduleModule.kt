package com.legion.control.di.modules

import com.legion.api.ApiServiceBuilder
import com.legion.control.ApiConstants
import com.legion.control.database.AppDatabase
import com.legion.control.models.schedules.api.ScheduleApi
import com.legion.control.models.schedules.cases.FinishScheduleUseCase
import com.legion.control.models.schedules.cases.GetCurrentScheduleUseCase
import com.legion.control.models.schedules.cases.LoadScheduleUseCase
import com.legion.control.models.schedules.cases.StartScheduleUseCase
import com.legion.control.models.schedules.cases.impl.FinishScheduleUseCaseImpl
import com.legion.control.models.schedules.cases.impl.GetCurrentScheduleUseCaseImpl
import com.legion.control.models.schedules.cases.impl.LoadScheduleUseCaseImpl
import com.legion.control.models.schedules.cases.impl.StartScheduleUseCaseImpl
import com.legion.control.models.schedules.cases.ui.GetActScheduleButtonEnabledUseCase
import com.legion.control.models.schedules.cases.ui.GetActScheduleTextVisibilityUseCase
import com.legion.control.models.schedules.cases.ui.GetFinishScheduleButtonTitleUseCase
import com.legion.control.models.schedules.cases.ui.GetStartScheduleButtonTitleUseCase
import com.legion.control.models.schedules.cases.ui.impl.GetActScheduleButtonEnabledUseCaseImpl
import com.legion.control.models.schedules.cases.ui.impl.GetActScheduleTextVisibilityUseCaseImpl
import com.legion.control.models.schedules.cases.ui.impl.GetFinishScheduleButtonTitleUseCaseImpl
import com.legion.control.models.schedules.cases.ui.impl.GetStartScheduleButtonTitleUseCaseImpl
import com.legion.control.models.schedules.repository.ScheduleRepository
import com.legion.control.models.schedules.repository.impl.ScheduleRepositoryImpl
import com.legion.control.models.schedules.scenario.FinishScheduleUIScenario
import com.legion.control.models.schedules.scenario.StartScheduleUIScenario
import com.legion.control.models.schedules.scenario.impl.FinishScheduleUIScenarioImpl
import com.legion.control.models.schedules.scenario.impl.StartScheduleUIScenarioImpl
import com.legion.control.models.schedules.utils.ScheduleConverter
import com.legion.control.models.schedules.utils.ScheduleStatusConverter
import com.legion.control.models.schedules.utils.impl.ScheduleDataUIConverterImpl
import com.legion.control.models.schedules.utils.impl.ScheduleStatusConverterImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
interface ScheduleModule {

    @Binds
    fun bindGetCurrentScheduleUseCase(useCaseImpl: GetCurrentScheduleUseCaseImpl): GetCurrentScheduleUseCase

    @Binds
    fun bindLoadScheduleUseCase(useCase: LoadScheduleUseCaseImpl): LoadScheduleUseCase

    @Binds
    fun bindStartScheduleUseCase(useCase: StartScheduleUseCaseImpl): StartScheduleUseCase

    @Binds
    fun bindFinishScheduleUseCase(useCase: FinishScheduleUseCaseImpl): FinishScheduleUseCase

    @Binds
    fun bindGetActScheduleButtonEnabledUseCase(
        useCase: GetActScheduleButtonEnabledUseCaseImpl
    ): GetActScheduleButtonEnabledUseCase

    @Binds
    fun bindGeStartScheduleButtonTitleUseCase(
        useCase: GetStartScheduleButtonTitleUseCaseImpl
    ): GetStartScheduleButtonTitleUseCase

    @Binds
    fun bindGetFinishScheduleButtonTitleUseCase(
        useCase: GetFinishScheduleButtonTitleUseCaseImpl
    ): GetFinishScheduleButtonTitleUseCase

    @Binds
    fun bindGetActScheduleTextVisibilityUseCase(
        useCase: GetActScheduleTextVisibilityUseCaseImpl
    ): GetActScheduleTextVisibilityUseCase

    @Binds
    fun bindStartScheduleUIScenario(scenario: StartScheduleUIScenarioImpl): StartScheduleUIScenario

    @Binds
    fun bindFinishScheduleUIScenario(scenario: FinishScheduleUIScenarioImpl): FinishScheduleUIScenario

    @Binds
    fun bindScheduleRepository(repository: ScheduleRepositoryImpl): ScheduleRepository

    @Binds
    fun bindScheduleConverter(converter: ScheduleDataUIConverterImpl): ScheduleConverter

    @Binds
    fun bindScheduleStatusConverter(converter: ScheduleStatusConverterImpl): ScheduleStatusConverter

    companion object {
        @Provides
        @Singleton
        fun provideShiftService(serviceBuilder: ApiServiceBuilder): ScheduleApi = serviceBuilder
            .build(ApiConstants.API_URL, ScheduleApi::class.java)

        @Provides
        @Singleton
        fun provideScheduleDao(database: AppDatabase) = database.getScheduleDao()
    }
}