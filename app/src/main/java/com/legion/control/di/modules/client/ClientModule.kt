package com.legion.control.di.modules.client

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.legion.api.ApiServiceBuilder
import com.legion.control.ApiConstants
import com.legion.control.components.client.main.ClientMainFragment
import com.legion.control.components.client.main.ClientMainViewModel
import com.legion.control.components.client.pass.employee.CreateEmployeePassViewModel
import com.legion.control.components.client.pass.goods.CreateGoodsPassViewModel
import com.legion.control.components.client.pass.visitor.CreateVisitorPassViewModel
import com.legion.control.di.core.fragment.FragmentKey
import com.legion.control.di.core.viewmodel.ViewModelKey
import com.legion.control.models.client.api.ClientApi
import com.legion.control.models.client.cases.AppendPhotoForPassUseCase
import com.legion.control.models.client.cases.GetDirectionByButtonTypeUseCase
import com.legion.control.models.client.cases.employee.CreateEmployeePassUseCase
import com.legion.control.models.client.cases.employee.impl.CreateEmployeePassUseCaseImpl
import com.legion.control.models.client.cases.goods.CreateGoodsPassUseCase
import com.legion.control.models.client.cases.goods.GetGoodsDirectionsByNumberUseCase
import com.legion.control.models.client.cases.goods.GetGoodsDirectionsUseCase
import com.legion.control.models.client.cases.goods.impl.CreateGoodsPassUseCaseImpl
import com.legion.control.models.client.cases.goods.impl.GetGoodsDirectionsByNumberUseCaseImpl
import com.legion.control.models.client.cases.goods.impl.GetGoodsDirectionsUseCaseImpl
import com.legion.control.models.client.cases.impl.AppendPhotoForPassUseCaseImpl
import com.legion.control.models.client.cases.impl.GetDirectionByButtonTypeUseCaseImpl
import com.legion.control.models.client.cases.visitor.CreateVisitorPassUseCase
import com.legion.control.models.client.cases.visitor.impl.CreateVisitorPassUseCaseImpl
import com.legion.control.models.client.scenario.CreateEmployeePassScenario
import com.legion.control.models.client.scenario.CreateGoodsPassScenario
import com.legion.control.models.client.scenario.GetGoodsDirectionsScenario
import com.legion.control.models.client.scenario.impl.CreateEmployeePassScenarioImpl
import com.legion.control.models.client.scenario.impl.CreateGoodsPassScenarioImpl
import com.legion.control.models.client.scenario.impl.GetGoodsDirectionsScenarioImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
interface ClientModule {
    @Binds
    @IntoMap
    @FragmentKey(ClientMainFragment::class)
    fun bindClientMainFragment(fragment: ClientMainFragment): Fragment

    @Binds
    @IntoMap
    @ViewModelKey(CreateGoodsPassViewModel::class)
    fun bindCreateGoodsPassViewModel(viewModel: CreateGoodsPassViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ClientMainViewModel::class)
    fun bindClientMainViewModel(viewModel: ClientMainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CreateEmployeePassViewModel::class)
    fun bindCreateEmployeePassVIewModel(vm: CreateEmployeePassViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CreateVisitorPassViewModel::class)
    fun bindCreateVisitorsPassViewModel(vm: CreateVisitorPassViewModel): ViewModel

    @Binds
    fun bindCreateGoodsPassUseCase(useCase: CreateGoodsPassUseCaseImpl): CreateGoodsPassUseCase

    @Binds
    fun bindCreateGoodsPassScenario(scenario: CreateGoodsPassScenarioImpl): CreateGoodsPassScenario

    @Binds
    fun bindCreateEmployeePassUseCase(useCase: CreateEmployeePassUseCaseImpl): CreateEmployeePassUseCase

    @Binds
    fun bindCreateEmployeePassScenario(scenario: CreateEmployeePassScenarioImpl): CreateEmployeePassScenario

    @Binds
    fun bindCreateVisitorPassUseCase(useCase: CreateVisitorPassUseCaseImpl): CreateVisitorPassUseCase

    @Binds
    fun bindGetNavDirectionByButtonTypeUseCase(useCase: GetDirectionByButtonTypeUseCaseImpl): GetDirectionByButtonTypeUseCase

    @Binds
    fun bindGetGoodsDirectionsUseCase(useCase: GetGoodsDirectionsUseCaseImpl): GetGoodsDirectionsUseCase

    @Binds
    fun bindGetGoodsDirectionsByNumberUseCase(useCase: GetGoodsDirectionsByNumberUseCaseImpl): GetGoodsDirectionsByNumberUseCase

    @Binds
    fun bindGetGoodsDirectionsScenario(scenario: GetGoodsDirectionsScenarioImpl): GetGoodsDirectionsScenario

    @Binds
    fun bindCreatePhotoForPassUseCase(useCase: AppendPhotoForPassUseCaseImpl): AppendPhotoForPassUseCase

    companion object {
        @Provides
        @Singleton
        fun provideClientApi(apiServiceBuilder: ApiServiceBuilder) = apiServiceBuilder
            .build(ApiConstants.API_URL, ClientApi::class.java)
    }
}