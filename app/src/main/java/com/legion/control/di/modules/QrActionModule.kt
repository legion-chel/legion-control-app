package com.legion.control.di.modules

import com.legion.control.components.guard.qr.adapter.utils.viewholder.QrActionViewHolderBuilder
import com.legion.control.components.guard.qr.adapter.utils.viewholder.QrActionViewHolderBuilderImpl
import com.legion.control.components.guard.qr.adapter.utils.viewtype.QrActionViewTypeSelector
import com.legion.control.components.guard.qr.adapter.utils.viewtype.QrActionViewTypeSelectorImpl
import com.legion.control.models.qr.actions.builder.ActionLoadingStateBuilder
import com.legion.control.models.qr.actions.builder.impl.ActionStateBuilderImpl
import com.legion.control.models.qr.actions.cases.GetQrActionsUseCase
import com.legion.control.models.qr.actions.cases.SendQrActionsResultUseCase
import com.legion.control.models.qr.actions.cases.SetCompleteActionUseCase
import com.legion.control.models.qr.actions.cases.TransformActionLoadingStateUseCase
import com.legion.control.models.qr.actions.cases.impl.GetQrActionsUseCaseImpl
import com.legion.control.models.qr.actions.cases.impl.SendActionsResultUseCaseImpl
import com.legion.control.models.qr.actions.cases.impl.SetActionCompleteUseCaseImpl
import com.legion.control.models.qr.actions.cases.impl.TransformActionLoadingStateUseCaseImpl
import com.legion.control.models.qr.actions.factory.PhotoActionResultFactory
import com.legion.control.models.qr.actions.factory.TextActionResultFactory
import com.legion.control.models.qr.actions.factory.impl.PhotoActionResultFactoryImpl
import com.legion.control.models.qr.actions.factory.impl.TextActionResultFactoryImpl
import com.legion.control.models.qr.actions.repository.QrActionRepository
import com.legion.control.models.qr.actions.repository.QrActionResultRepository
import com.legion.control.models.qr.actions.repository.impl.QrActionRepositoryImpl
import com.legion.control.models.qr.actions.repository.impl.QrActionResultRepositoryImpl
import com.legion.control.models.qr.actions.source.RemoteActionSource
import com.legion.control.models.qr.actions.source.impl.RemoteActionSourceImpl
import com.legion.control.models.qr.actions.utils.ActionStateWrapperConverter
import com.legion.control.models.qr.actions.utils.impl.ActionStateWrapperConverterImpl
import dagger.Binds
import dagger.Module

@Module
interface QrActionModule {
    @Binds
    fun bindGetActionUseCase(useCaseImpl: GetQrActionsUseCaseImpl): GetQrActionsUseCase

    @Binds
    fun bindActionRepository(repositoryImpl: QrActionRepositoryImpl): QrActionRepository

    @Binds
    fun bindActionStateBuilder(builder: ActionStateBuilderImpl): ActionLoadingStateBuilder

    @Binds
    fun bindRemoteActionSource(source: RemoteActionSourceImpl): RemoteActionSource

    @Binds
    fun bindQrActionViewHolderBuilder(builder: QrActionViewHolderBuilderImpl): QrActionViewHolderBuilder

    @Binds
    fun bindQrActionViewTypeSelector(selector: QrActionViewTypeSelectorImpl): QrActionViewTypeSelector

    @Binds
    fun bindPhotoActionResultFactory(factory: PhotoActionResultFactoryImpl): PhotoActionResultFactory

    @Binds
    fun bindTextActionResultFactory(factory: TextActionResultFactoryImpl): TextActionResultFactory

    @Binds
    fun bindQrActionResultRepository(repository: QrActionResultRepositoryImpl): QrActionResultRepository

    @Binds
    fun bindSendQrActionResultUseCase(useCase: SendActionsResultUseCaseImpl): SendQrActionsResultUseCase

    @Binds
    fun bindTransformActionLoadingStateUseCase(useCase: TransformActionLoadingStateUseCaseImpl): TransformActionLoadingStateUseCase

    @Binds
    fun bindActionStateWrapperConverter(converter: ActionStateWrapperConverterImpl): ActionStateWrapperConverter

    @Binds
    fun bindSetActionCompleteUseCase(useCase: SetActionCompleteUseCaseImpl): SetCompleteActionUseCase
}