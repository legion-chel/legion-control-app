package com.legion.control.di.modules

import com.legion.control.models.utils.ConfigureCrashlyticsUseCase
import com.legion.control.models.utils.GetDateStringUseCase
import com.legion.control.models.utils.TransformLoadingStateToUIStateUseCase
import com.legion.control.models.utils.ValidateEmailUseCase
import com.legion.control.models.utils.impl.ConfigureCrashlyticsUseCaseImpl
import com.legion.control.models.utils.impl.GetDateStringUseCaseImpl
import com.legion.control.models.utils.impl.TransformLoadingStateToUIStateUseCaseImpl
import com.legion.control.models.utils.impl.ValidateEmailUseCaseImpl
import dagger.Binds
import dagger.Module

@Module
interface UtilsModule {
    @Binds
    fun bindTransformLoadingStateToUIStateUseCase(
        useCase: TransformLoadingStateToUIStateUseCaseImpl
    ): TransformLoadingStateToUIStateUseCase

    @Binds
    fun bindConfigureFirebaseCrashlyticsUseCase(
        useCase: ConfigureCrashlyticsUseCaseImpl
    ): ConfigureCrashlyticsUseCase
    
    @Binds
    fun bindGetDateStringUseCase(useCase: GetDateStringUseCaseImpl): GetDateStringUseCase
    
    @Binds
    fun bindValidateEmailUseCase(useCase: ValidateEmailUseCaseImpl): ValidateEmailUseCase
}