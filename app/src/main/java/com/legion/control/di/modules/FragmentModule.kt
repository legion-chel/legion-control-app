package com.legion.control.di.modules

import androidx.fragment.app.Fragment
import com.legion.control.components.guard.main.GuardMainFragment
import com.legion.control.components.guard.schedules.main.ShiftFragment
import com.legion.control.components.login.ui.LoginFragment
import com.legion.control.components.organizations.ui.OrganizationsFragment
import com.legion.control.components.profile.ProfileFragment
import com.legion.control.components.splash.SplashFragment
import com.legion.control.components.user.ui.UserInfoFragment
import com.legion.control.di.core.fragment.FragmentKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface FragmentModule {
    @Binds
    @IntoMap
    @FragmentKey(LoginFragment::class)
    fun bindLoginFragment(fragment: LoginFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SplashFragment::class)
    fun bindSplashFragment(fragment: SplashFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(GuardMainFragment::class)
    fun bindMainFragment(fragment: GuardMainFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ProfileFragment::class)
    fun bindProfileFragment(fragment: ProfileFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(UserInfoFragment::class)
    fun bindUserInfoFragment(fragment: UserInfoFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(OrganizationsFragment::class)
    fun bindOrganizationsFragment(fragment: OrganizationsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ShiftFragment::class)
    fun bindShitFragment(fragment: ShiftFragment): Fragment
}