package com.legion.control.di.modules

import androidx.lifecycle.ViewModel
import com.legion.control.components.guard.reports.CreateReportViewModel
import com.legion.control.components.guard.main.GuardMainViewModel
import com.legion.control.components.guard.qr.employee.EmployeePassViewModel
import com.legion.control.components.guard.qr.goods.GoodsQrInfoViewModel
import com.legion.control.components.guard.qr.visitor.VisitorPassViewModel
import com.legion.control.components.guard.rounds.RoundsViewModel
import com.legion.control.components.guard.schedules.finish.FinishShiftViewModel
import com.legion.control.components.guard.schedules.main.ShiftViewModel
import com.legion.control.components.guard.schedules.start.StartScheduleViewModel
import com.legion.control.components.login.ui.LoginViewModel
import com.legion.control.components.organizations.ui.OrganizationsViewModel
import com.legion.control.components.profile.ProfileViewModel
import com.legion.control.components.splash.SplashViewModel
import com.legion.control.components.user.ui.UserInfoViewModel
import com.legion.control.di.core.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GuardMainViewModel::class)
    fun bindMainViewModel(viewModel: GuardMainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    fun bindProfileViewModel(viewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserInfoViewModel::class)
    fun bindUserInfoViewModel(viewModel: UserInfoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrganizationsViewModel::class)
    fun bindOrganizationsModel(viewModel: OrganizationsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ShiftViewModel::class)
    fun bindShiftViewModel(viewModel: ShiftViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(StartScheduleViewModel::class)
    fun bindStartShiftViewModel(viewModel: StartScheduleViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FinishShiftViewModel::class)
    fun bindFinishShiftViewModel(viewModel: FinishShiftViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GoodsQrInfoViewModel::class)
    fun bindQrInfoViewModel(viewModel: GoodsQrInfoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EmployeePassViewModel::class)
    fun bindEmployeeQrInfoViewModel(viewModel: EmployeePassViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VisitorPassViewModel::class)
    fun bindVisitorQrInfoViewModel(viewModel: VisitorPassViewModel): ViewModel
    
    @Binds
    @IntoMap
    @ViewModelKey(RoundsViewModel::class)
    fun bindRoundsViewModel(viewModel: RoundsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CreateReportViewModel::class)
    fun bindCreateIncidentsViewModel(viewModel: CreateReportViewModel): ViewModel
}