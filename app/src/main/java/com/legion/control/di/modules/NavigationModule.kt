package com.legion.control.di.modules

import com.legion.control.MainApplication
import com.legion.control.navigation.Navigator
import com.legion.control.navigation.impl.NavigatorImpl
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
interface NavigationModule {
    @Binds
    fun bindNavigator(navigator: NavigatorImpl): Navigator
    
    companion object {
        @Provides
        fun provideNavController(application: MainApplication) = application.currentActivity.navController
    }
}