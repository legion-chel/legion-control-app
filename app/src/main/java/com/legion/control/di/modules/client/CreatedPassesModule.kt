package com.legion.control.di.modules.client

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.legion.control.components.client.created.CreatedPassesFragment
import com.legion.control.components.client.created.CreatedPassesViewModel
import com.legion.control.di.core.fragment.FragmentKey
import com.legion.control.di.core.viewmodel.ViewModelKey
import com.legion.control.models.client.cases.employee.GetEmployeeCreatesPassesUseCase
import com.legion.control.models.client.cases.employee.impl.GetEmployeeCreatedPassesUseCaseImpl
import com.legion.control.models.client.cases.goods.GetGoodsCreatedPassesUseCase
import com.legion.control.models.client.cases.goods.impl.GetGoodsCreatedPassesUseCaseImpl
import com.legion.control.models.client.cases.visitor.GetVisitorCreatedPassesUseCase
import com.legion.control.models.client.cases.visitor.impl.GetVisitorCreatedPassesUseCaseImpl
import com.legion.control.models.client.repository.PassRepository
import com.legion.control.models.client.repository.impl.PassRepositoryImpl
import com.legion.control.models.client.scenario.GetCreatedPassesScenario
import com.legion.control.models.client.scenario.impl.GetCreatedPassesScenarioImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface CreatedPassesModule {
    @Binds
    @IntoMap
    @FragmentKey(CreatedPassesFragment::class)
    fun bindCreatedPassesFragment(fragment: CreatedPassesFragment): Fragment

    @Binds
    @IntoMap
    @ViewModelKey(CreatedPassesViewModel::class)
    fun bindCreatedPassesViewModel(viewModel: CreatedPassesViewModel): ViewModel

    @Binds
    fun bindCreatedPassesRepository(
        repositoryImpl: PassRepositoryImpl
    ): PassRepository

    @Binds
    fun bindGetGoodsCreatedPassesUseCase(
        useCase: GetGoodsCreatedPassesUseCaseImpl
    ): GetGoodsCreatedPassesUseCase

    @Binds
    fun bindGetEmployeeCreatedPassesUseCase(
        useCase: GetEmployeeCreatedPassesUseCaseImpl
    ): GetEmployeeCreatesPassesUseCase

    @Binds
    fun bindGetVisitorCreatedPassesUseCase(
        useCase: GetVisitorCreatedPassesUseCaseImpl
    ): GetVisitorCreatedPassesUseCase

    @Binds
    fun bindGetCreatedPassesScenario(scenario: GetCreatedPassesScenarioImpl): GetCreatedPassesScenario
}