package com.legion.control.di.modules

import com.legion.control.models.sync.scenario.HandleSyncStateScenario
import com.legion.control.models.sync.scenario.impl.HandleSyncStateScenarioImpl
import com.legion.control.models.sync.usecases.GetLoadingStateBySyncStateUseCase
import com.legion.control.models.sync.usecases.NavigateBySyncStateUseCase
import com.legion.control.models.sync.usecases.impl.GetLoadingStateBySyncStateUseCaseImpl
import com.legion.control.models.sync.usecases.impl.NavigateBySyncStateUseCaseImpl
import dagger.Binds
import dagger.Module

@Module
interface SyncModule {
    @Binds
    fun bindNavigateBySyncStateUseCase(
        useCase: NavigateBySyncStateUseCaseImpl
    ): NavigateBySyncStateUseCase
    
    @Binds
    fun bindGetLoadingStateBySyncSateUseCase(
        useCase: GetLoadingStateBySyncStateUseCaseImpl
    ): GetLoadingStateBySyncStateUseCase
    
    @Binds
    fun bindHandleSyncStateScenario(
        scenario: HandleSyncStateScenarioImpl
    ): HandleSyncStateScenario
}