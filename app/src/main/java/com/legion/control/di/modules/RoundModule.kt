package com.legion.control.di.modules

import com.legion.api.ApiServiceBuilder
import com.legion.control.ApiConstants
import com.legion.control.models.rounds.api.RoundApiService
import com.legion.control.models.rounds.repository.RoundsRepository
import com.legion.control.models.rounds.repository.impl.RoundsRepositoryImpl
import com.legion.control.models.rounds.usecases.GetRoundsUseCases
import com.legion.control.models.rounds.usecases.impl.GetRoundsUseCasesImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
interface RoundModule {
    @Binds
    fun bindGetRoundUseCase(useCase: GetRoundsUseCasesImpl): GetRoundsUseCases
    
    @Binds
    fun bindRoundRepository(repository: RoundsRepositoryImpl): RoundsRepository
    
    companion object {
        @Provides
        @Singleton
        fun provideRoundApiService(apiServiceBuilder: ApiServiceBuilder) = apiServiceBuilder
            .build(ApiConstants.API_URL, RoundApiService::class.java)
    }
}