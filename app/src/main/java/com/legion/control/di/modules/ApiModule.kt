package com.legion.control.di.modules

import com.legion.api.ApiServiceBuilder
import com.legion.api.factory.InterceptorsFactory
import com.legion.control.ApiConstants
import com.legion.control.models.auth.api.AuthService
import com.legion.control.models.interceptors.AppInterceptorFactory
import com.legion.control.models.interceptors.AuthInterceptor
import com.legion.control.models.interceptors.UserInterceptor
import com.legion.control.models.qr.actions.api.ActionApi
import com.legion.control.models.qr.info.api.QrInfoApi
import com.legion.control.models.upload.api.UploadApi
import com.legion.control.models.user.UserService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApiModule {
    @Provides
    @Singleton
    fun provideInterceptorFactory(
        authInterceptor: AuthInterceptor,
        userInterceptor: UserInterceptor
    ): InterceptorsFactory = AppInterceptorFactory(authInterceptor, userInterceptor)

    @Provides
    @Singleton
    fun provideAuthService(serviceBuilder: ApiServiceBuilder): AuthService = serviceBuilder
        .build(ApiConstants.API_URL, AuthService::class.java)

    @Provides
    @Singleton
    fun provideUserService(serviceBuilder: ApiServiceBuilder): UserService = serviceBuilder
        .build(ApiConstants.API_URL, UserService::class.java)

    @Provides
    @Singleton
    fun provideQrApiService(serviceBuilder: ApiServiceBuilder): QrInfoApi = serviceBuilder
        .build(ApiConstants.API_URL, QrInfoApi::class.java)

    @Provides
    @Singleton
    fun provideUploadApi(serviceBuilder: ApiServiceBuilder): UploadApi = serviceBuilder
        .build(ApiConstants.API_URL, UploadApi::class.java)

    @Provides
    @Singleton
    fun provideActionApi(serviceBuilder: ApiServiceBuilder): ActionApi = serviceBuilder
        .build(ApiConstants.API_URL, ActionApi::class.java)
}