package com.legion.control.di.modules

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.legion.control.components.home.ui.HomeFragment
import com.legion.control.components.home.ui.HomeViewModel
import com.legion.control.di.core.fragment.FragmentKey
import com.legion.control.di.core.viewmodel.ViewModelKey
import com.legion.control.models.home.cases.GetHomeMenuIdUseCase
import com.legion.control.models.home.cases.GetHomePagesUseCase
import com.legion.control.models.home.cases.impl.GetHomeMenuIdUseCaseImpl
import com.legion.control.models.home.cases.impl.GetHomePagesUseCaseImpl
import com.legion.control.models.home.factory.ClientHomePageFactory
import com.legion.control.models.home.factory.GuardHomePageFactory
import com.legion.control.models.home.factory.impl.ClientHomePageFactoryImpl
import com.legion.control.models.home.factory.impl.GuardHomePageFactoryImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface HomeModule {
    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    fun bindHomeViewModel(vm: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @FragmentKey(HomeFragment::class)
    fun bindHomeFragment(fragment: HomeFragment): Fragment

    @Binds
    fun bindGuardPageFactory(factory: GuardHomePageFactoryImpl): GuardHomePageFactory

    @Binds
    fun bindClientPageFactory(factory: ClientHomePageFactoryImpl): ClientHomePageFactory

    @Binds
    fun bindGetHomePagesUseCase(useCase: GetHomePagesUseCaseImpl): GetHomePagesUseCase

    @Binds
    fun bindGetHomeMenuIdUseCase(useCase: GetHomeMenuIdUseCaseImpl): GetHomeMenuIdUseCase
}