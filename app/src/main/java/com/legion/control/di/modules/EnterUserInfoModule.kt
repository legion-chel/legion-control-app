package com.legion.control.di.modules

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.legion.control.components.client.registration.RegistrationClientFragment
import com.legion.control.components.client.registration.RegistrationClientViewModel
import com.legion.control.di.core.fragment.FragmentKey
import com.legion.control.di.core.viewmodel.ViewModelKey
import com.legion.control.models.client.cases.registration.AddClientAvatarUseCase
import com.legion.control.models.client.cases.registration.AddDocumentPhotoUseCase
import com.legion.control.models.client.cases.registration.GetAttachedPhotosUseCase
import com.legion.control.models.client.cases.registration.RegistrationClientUseCase
import com.legion.control.models.client.cases.registration.impl.AddClientAvatarUseCaseImpl
import com.legion.control.models.client.cases.registration.impl.AddDocumentPhotoUseCaseImpl
import com.legion.control.models.client.cases.registration.impl.RegistrationClientUseCaseImpl
import com.legion.control.models.client.cases.registration.validation.ValidateInputClientFirstNameUseCase
import com.legion.control.models.client.cases.registration.validation.ValidateInputClientSecondNameUseCase
import com.legion.control.models.client.cases.registration.validation.ValidateInputDocumentNumberUseCase
import com.legion.control.models.client.cases.registration.validation.ValidateInputDocumentSerialUseCase
import com.legion.control.models.client.cases.registration.validation.impl.*
import com.legion.control.models.client.repository.ClientPhotoRepository
import com.legion.control.models.client.repository.RegistrationRepository
import com.legion.control.models.client.repository.impl.ClientPhotoRepositoryImpl
import com.legion.control.models.client.repository.impl.RegistrationRepositoryImpl
import com.legion.control.models.client.scenario.AddClientPhotoScenario
import com.legion.control.models.client.scenario.RegistrationClientScenario
import com.legion.control.models.client.scenario.ValidateInputClientNameScenario
import com.legion.control.models.client.scenario.ValidateInputDocumentInfoScenario
import com.legion.control.models.client.scenario.impl.AddClientPhotoScenarioImpl
import com.legion.control.models.client.scenario.impl.RegistrationClientScenarioImpl
import com.legion.control.models.client.scenario.impl.ValidateInputClientNameScenarioImpl
import com.legion.control.models.client.scenario.impl.ValidateInputDocumentInfoScenarioImpl
import com.legion.control.models.client.template.ValidateInputClientInfoTemplate
import com.legion.control.models.client.template.impl.ValidateInputClientInfoTemplateImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface EnterUserInfoModule {
    @Binds
    @IntoMap
    @FragmentKey(RegistrationClientFragment::class)
    fun bindEnterClientInfoFragment(fragment: RegistrationClientFragment): Fragment
    
    @Binds
    @IntoMap
    @ViewModelKey(RegistrationClientViewModel::class)
    fun bindEnterClientInfoViewModel(viewModel: RegistrationClientViewModel): ViewModel
    
    @Binds
    fun bindValidateInputClientFirstNameUseCase(
        useCase: ValidateInputClientFirstNameUseCaseImpl
    ): ValidateInputClientFirstNameUseCase
    
    @Binds
    fun bindValidateInputClientSecondNameUseCase(
        useCase: ValidateInputClientSecondNameUseCaseImpl
    ): ValidateInputClientSecondNameUseCase
    
    @Binds
    fun bindValidateInputClientNameScenario(
        scenario: ValidateInputClientNameScenarioImpl
    ): ValidateInputClientNameScenario
    
    @Binds
    fun bindValidateInputClientInfoTemplate(
        templateImpl: ValidateInputClientInfoTemplateImpl
    ): ValidateInputClientInfoTemplate
    
    @Binds
    fun bindClientPhotoRepository(
        repository: ClientPhotoRepositoryImpl
    ): ClientPhotoRepository
    
    @Binds
    fun bindAddDocumentPhotoUseCase(
        useCase: AddDocumentPhotoUseCaseImpl
    ): AddDocumentPhotoUseCase
    
    @Binds
    fun bindAddClientAvatarUseCase(
        useCase: AddClientAvatarUseCaseImpl
    ): AddClientAvatarUseCase
    
    @Binds
    fun bindAddClientPhotoScenario(
        scenario: AddClientPhotoScenarioImpl
    ): AddClientPhotoScenario
    
    @Binds
    fun bindValidateInputDocumentSerialUseCase(
        useCase: ValidateInputDocumentSerialUseCaseImpl
    ): ValidateInputDocumentSerialUseCase
    
    @Binds
    fun bindValidateInputDocumentNumberUseCase(
        useCase: ValidateInputDocumentNumberUseCaseImpl
    ): ValidateInputDocumentNumberUseCase
    
    @Binds
    fun bindValidateInputDocumentInfoScenario(
        scenario: ValidateInputDocumentInfoScenarioImpl
    ): ValidateInputDocumentInfoScenario
    
    @Binds
    fun bindRegistrationRepository(
        repository: RegistrationRepositoryImpl
    ): RegistrationRepository
    
    @Binds
    fun bindRegistrationUseCase(
        useCase: RegistrationClientUseCaseImpl
    ): RegistrationClientUseCase
    
    @Binds
    fun bindRegistrationClientScenario(
        scenario: RegistrationClientScenarioImpl
    ): RegistrationClientScenario
    
    @Binds
    fun bindGetAttachedPhotosUseCase(
        useCase: GetAttachedPhotosUseCaseImpl
    ): GetAttachedPhotosUseCase
}