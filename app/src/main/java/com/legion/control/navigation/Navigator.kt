package com.legion.control.navigation

import androidx.navigation.NavDirections

interface Navigator {
    fun navigate(navDirections: NavDirections)
}