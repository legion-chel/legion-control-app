package com.legion.control.navigation.impl

import androidx.navigation.NavController
import androidx.navigation.NavDirections
import com.legion.control.navigation.Navigator
import com.legion.utils.extensions.navigateSafe
import javax.inject.Inject

class NavigatorImpl @Inject constructor(
    private val navController: NavController
) : Navigator {
    override fun navigate(navDirections: NavDirections) {
        navController.navigateSafe(navDirections)
    }
}