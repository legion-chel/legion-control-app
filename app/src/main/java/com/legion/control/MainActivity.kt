package com.legion.control

import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.legion.control.databinding.ActivityMainBinding
import com.legion.control.di.core.fragment.FragmentFactory
import com.legion.control.models.ui.ProgressIndicator
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), ProgressIndicator {

    private lateinit var binding: ActivityMainBinding

    val navController: NavController
        get() {
            val fragment = supportFragmentManager.findFragmentById(R.id.hostFragment) as NavHostFragment
            return fragment.findNavController()
        }

    private val destinationListener = NavController.OnDestinationChangedListener { _, destination, _ ->
        val destinationTitle = destination.label
        supportActionBar?.title = destinationTitle
    }

    @Inject
    fun injectFragmentFactory(fragmentFactory: FragmentFactory) {
        supportFragmentManager.fragmentFactory = fragmentFactory
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        navController.addOnDestinationChangedListener(destinationListener)
    }

    override fun showProgress() {
        binding.progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        binding.progressBar.visibility = View.GONE
    }

    override fun onBackPressed() {
        if (!navController.popBackStack()) super.onBackPressed()
    }
}