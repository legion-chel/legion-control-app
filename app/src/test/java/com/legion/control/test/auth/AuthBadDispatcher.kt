package com.legion.control.test.auth

import com.legion.api.BAD_REQUEST_CODE
import com.legion.api.INVALID_DATA_CODE
import com.legion.api.SERVER_ERROR_CODE
import com.legion.control.instruments.api.RequestDispatcher
import com.legion.control.models.auth.data.AuthData
import com.legion.utils.fromJson
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import okio.Buffer
import java.nio.charset.Charset

class AuthBadDispatcher : RequestDispatcher() {
    override fun dispatch(request: RecordedRequest): MockResponse {
        return when(request.path) {
            "/auth/login" -> {
                val authData = getAuthData(request.body)
                MockResponse().setResponseCode(getRequestCode(authData))
            }
            else -> MockResponse().setResponseCode(404)
        }
    }

    private fun getRequestCode(authData: AuthData?): Int {
        authData ?: return SERVER_ERROR_CODE

        return if (authData.login.isEmpty()) INVALID_DATA_CODE
        else BAD_REQUEST_CODE
    }

    private fun getAuthData(buffer: Buffer): AuthData? {
        val string = buffer.readString(buffer.size, Charset.defaultCharset())
        return fromJson(string)
    }
}