package com.legion.control.test.auth

import android.content.Context
import android.os.Build
import androidx.test.core.app.ApplicationProvider
import com.legion.control.instruments.ServiceBuilder
import com.legion.control.instruments.api.WebServer
import com.legion.control.models.auth.api.AuthService
import com.legion.control.models.auth.data.AuthState
import com.legion.control.models.auth.model.AuthModel
import com.legion.control.models.auth.model.AuthValidator
import com.legion.control.models.auth.utils.LogoutBroadcast
import com.legion.core.session.UserSessionFacade
import com.legion.utils.ResourceProvider
import com.legion.utils.extensions.empty
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

private const val DEFAULT_LOGIN = "login"
private const val DEFAULT_PASSWORD = "password"

@Config(sdk = [
    Build.VERSION_CODES.N_MR1
])
@RunWith(RobolectricTestRunner::class)
class AuthModelTest {
    private val webServer = WebServer()
    private val model: AuthModel

    init {
        val context = ApplicationProvider.getApplicationContext<Context>().applicationContext

        val userSessionFacade = UserSessionFacade(context)
        val apiService = ServiceBuilder()
            .build(webServer.url.toString(), AuthService::class.java)

        val logoutSender = LogoutBroadcast.Sender(context)
        val authValidator = AuthValidator(ResourceProvider(context))

        model = AuthModel(apiService, userSessionFacade, logoutSender, authValidator)
    }

    @Test
    fun `Auth success`() = runBlocking {
        webServer.setDispatcher(AuthSuccessDispatcher())

        val state = model.auth(DEFAULT_LOGIN, DEFAULT_PASSWORD)

        Assert.assertTrue("Auth state", state is AuthState.Success)
        Assert.assertTrue(model.isAuthorized)
    }

    @Test
    fun `Auth with bad request`() = runBlocking {
        webServer.setDispatcher(AuthBadDispatcher())

        val state = model.auth(DEFAULT_LOGIN, DEFAULT_PASSWORD)

        Assert.assertTrue("Auth state", state is AuthState.Failed)
        Assert.assertFalse(model.isAuthorized)
    }

    @Test
    fun `Auth with invalid request data`() = runBlocking {
        webServer.setDispatcher(AuthBadDispatcher())

        val state = model.auth(String.empty, String.empty)

        Assert.assertTrue("Auth state", state is AuthState.Failed)
        Assert.assertFalse(model.isAuthorized)
    }

    @After
    fun clear() {
        webServer.stop()
    }
}