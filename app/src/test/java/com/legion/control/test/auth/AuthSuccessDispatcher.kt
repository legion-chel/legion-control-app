package com.legion.control.test.auth

import com.legion.control.instruments.api.RequestDispatcher
import com.legion.core.session.auth.AuthInfo
import com.legion.utils.toJson
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest

class AuthSuccessDispatcher : RequestDispatcher() {
    override fun dispatch(request: RecordedRequest): MockResponse {
        val authInfo = AuthInfo(
            "user",
            "user",
            12312L
        )

        val authInfoJson = toJson(authInfo)

        return when(request.path) {
            "/auth/login" -> MockResponse().setResponseCode(200).setBody(authInfoJson)
            else -> MockResponse().setResponseCode(400)
        }
    }
}