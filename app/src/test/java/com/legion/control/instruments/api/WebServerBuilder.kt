package com.legion.control.instruments.api

import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.mockwebserver.MockWebServer

open class WebServer {
    private val webServer = MockWebServer().apply {
        url(HOST)
    }

    val url
        get() = webServer.url(HOST).toString().toHttpUrl()

    fun start() = webServer.start()

    fun setDispatcher(dispatcher: RequestDispatcher) {
        webServer.dispatcher = dispatcher
    }

    fun stop() = webServer.shutdown()

    companion object {
        const val HOST = "/"
    }
}
