package com.legion.control.instruments

import com.legion.api.ApiServiceBuilder
import com.legion.api.factory.HttpClientFactory
import com.legion.api.factory.InterceptorsFactory

class ServiceBuilder {
    private val interceptorsFactory = InterceptorsFactory {
        emptyList()
    }

    private val httpClientFactory = HttpClientFactory(interceptorsFactory)

    fun<T> build(url: String, service: Class<T>): T = ApiServiceBuilder(httpClientFactory)
        .build(url, service)
}